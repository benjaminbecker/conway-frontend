import React, { useState } from 'react'
import './GameOfLifePreviewList.css'
import Modal from 'react-bootstrap/Modal'
import ListGroup from 'react-bootstrap/ListGroup'
import GameOfLifePreview from './GameOfLifePreview'
import GameOfLifePreviewFilter from './GameOfLifePreviewFilter'
import { urlBackend } from '../settings'

const numberOfItemsInView = 200;

const getFilteredGames = (games, filterQuery) => {
    const filteredGames = games.filter(game => game.label.toLowerCase().includes(filterQuery.toLowerCase()));
    return filteredGames
}

const increaseIdListSubset = (id, setIdListSubset) => {
    setIdListSubset(id + 1);
}

const decreaseIdListSubset = (id, setIdListSubset) => {
    if (id>0) setIdListSubset(id - 1);
}

const GameOfLifePreviewList = (props) => {
    const [filterQuery, setFilterQuery] = useState('');
    const [idListSubset, setIdListSubset] = useState(0);
    const filteredGames = getFilteredGames(props.games, filterQuery);
    const nFilteredGames = filteredGames.length;
    const idFirst = idListSubset * numberOfItemsInView;
    const idLast = (idListSubset + 1) * numberOfItemsInView - 1
    const subsetFilteredGames = filteredGames.slice(idFirst, idLast);
    const items = subsetFilteredGames.map(game => 
        <ListGroup.Item className="column preview-item" id={game.id} onClick={() => props.callbackSetFilename(game.filename)}>
            <div className="d-flex w-100 justify-content-between">
                <span classname="preview-description">
                    <p className="preview-title">{game.label}</p>
                </span>
                <span className="preview-image">
                    <img src={urlBackend + game.filename.replace('.rle', '.svg')} width="50px" height="50px"/>
                </span>
            </div>
        </ListGroup.Item>)
    return (
        <div>
            <div className="filter-query-container">
                <GameOfLifePreviewFilter filterQuery={filterQuery} setFilterQuery={setFilterQuery} />
                <p className="filter-item-count">
                    {idFirst+1}-{Math.min(idLast+1, nFilteredGames)} of {nFilteredGames}:
                </p>
                <button onClick={(ev) => decreaseIdListSubset(idListSubset, setIdListSubset)}>&lt;</button>
                <button onClick={(ev) => increaseIdListSubset(idListSubset, setIdListSubset)}>&gt;</button>
                <button onClick={props.onHide}>close</button>
            </div>
            <div className="preview-list">
                <ListGroup>
                    {items}
                </ListGroup>
            </div>
        </div>
    );
};

export default GameOfLifePreviewList;