import React from 'react';

const GameOfLifeSelector = (props) => {
    const options = props.games.map((option) => 
        <option value={option.id}>{option.label}</option>
    )
    return (
        <div>
            <label htmlFor="game-select">Choose a start configuration:</label>
            <select id="game-select">
                { options }
            </select>
        </div>
    );
};

export default GameOfLifeSelector;