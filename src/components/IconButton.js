import React from 'react';
import './IconButton.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const IconButton = ({ icon, label, onClick }) => {
    return (
        <a className="btn-container" onClick={onClick}>
            <button className="btn">
                <FontAwesomeIcon icon={icon} />
            </button>
            <span className="btn-caption">{label}</span>
        </a>
    );
};

export default IconButton;