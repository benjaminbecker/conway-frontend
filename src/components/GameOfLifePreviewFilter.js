import React from 'react';


const GameOfLifePreviewFilter = ({ className, filterQuery, setFilterQuery }) => {
    const callbackChange = ev => {setFilterQuery(ev.target.value)}
    return (
        <div className={className}>
            <input placeholder="search..." className={className} value={filterQuery} onChange={callbackChange}></input>
        </div>
    );
};

export default GameOfLifePreviewFilter;