import React from 'react';

function GameOfLifeBoard(props) {
    const { n_x, n_y, aliveList } = props;
    const blocks = aliveList.map(([ x, y ]) => 
        <circle cx={x+0.5} cy={y+0.5} r={0.5} fill="#d6ce15" />
    );
    return (
        <div>
            <svg viewBox={`0 0 ${n_x} ${n_y}`}>
                <rect width={n_x} height={n_y} fill="black" />
                { blocks }
                Sorry, your browser does not support inline SVG.
            </svg>
        </div>
    )
}

export default GameOfLifeBoard