import React from 'react';
import { getXMax, getYMax } from '../logic/gameOfLife';
import GameOfLifeBoard from './GameOfLifeBoard';

const GameOfLifePreview = props => {
    const { aliveList, rectangular=true } = props;
    let n_x = getXMax(aliveList) + 1;
    let n_y = getYMax(aliveList) + 1;
    if (rectangular) {
        n_x = Math.max(n_x, n_y);
        n_y = n_x;
    }
    const propsGameOfLifeBoard = { n_x, n_y, aliveList};
    return (
        <div>
            <GameOfLifeBoard {...propsGameOfLifeBoard} />
        </div>
    );
};

export default GameOfLifePreview;