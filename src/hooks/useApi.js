import React, { useState } from 'react';
import useFetch from "react-fetch-hook";

import { urlBackend } from '../settings';


function useApi(defaultFilename) {
    const [filename, setFilename] = useState(defaultFilename);
    const { isLoading, data } = useFetch(urlBackend + filename);

    return [isLoading, data, setFilename]
}

export default useApi;