import { isAlive, getNeighbours, countNeighboursAlive, updateAliveList, getXMax, getYMax, shiftAliveList } from './gameOfLife';

describe('isAlive', () => {
    it('returns true if [x, y] in aliveList', () => {
        const aliveList = [[1,2], [3,4], [5,6]];
        expect(isAlive([3, 4], aliveList)).toBe(true);
    });

    it('returns false if [x, y] not in aliveList', () => {
        const aliveList = [[1,2], [3,4], [5,6]];
        expect(isAlive([2, 3], aliveList)).toBe(false);
    });
})

describe('getNeighbours', () => {
    it('returns neighbours', () => {
        const neighbours = getNeighbours([3, 6]);
        const expectedNeighbours = [[2,5], [3,5], [4,5], [2,6], [4,6], [2,7], [3,7], [4,7]];
        expect(neighbours).toEqual(
            expect.arrayContaining(expectedNeighbours)
        );
    })
})

describe('countNeighboursAlive', () => {
    it('returns 8 for complete aliveList', () => {
        const aliveList = [
            [1,1], [2,1], [3,1],
            [1,2], [2,2], [3,2],
            [1,3], [2,3], [3,3]
        ];
        expect(countNeighboursAlive([2, 2], aliveList)).toBe(8);
    })
})

describe('updateAliveList', () => {
    it('returns empty aliveList if given empty aliveList', () => {
        let aliveList = [];
        aliveList = updateAliveList(aliveList, 10, 10);
        expect(aliveList.length).toBe(0);
    });

    it('kills a single alive cell', () => {
        let aliveList = [[2, 2]];
        aliveList = updateAliveList(aliveList, 10, 10);
        expect(aliveList.length).toBe(0);
    });

    it('does not change number of items for static cross', () => {
        let aliveList = [
                   [3,1],
            [2,2],        [4,2],
                   [3,3]
        ];
        aliveList = updateAliveList(aliveList, 10, 10);
        expect(aliveList.length).toBe(4);
    })
});

describe('getXMax', () => {
    it('returns 10 :)', () => {
        const aliveList = [[1,1], [10,1]];
        const xMax = getXMax(aliveList);
        expect(xMax).toBe(10);
    });

    it('returns 0 for empty list', () => {
        const aliveList = [];
        const xMax = getXMax(aliveList);
        expect(xMax).toBe(0);        
    });
})

describe('getYMax', () => {
    it('returns 10 :)', () => {
        const aliveList = [[1,1], [1,10]];
        const yMax = getYMax(aliveList);
        expect(yMax).toBe(10);
    });
})

describe('shiftAliveList', () => {
    it('correctly shifts the aliveList', () => {
        const aliveList = [[1,1], [1,10]];
        const deltaX = 3;
        const deltaY = 4;
        const shiftedAliveList = shiftAliveList(aliveList, deltaX, deltaY);
        expect(shiftedAliveList).toEqual([[4,5], [4,14]])
    })
})