export function isAlive([x, y], aliveList) {
    const result = aliveList.some(cell => 
        cell[0]===x && cell[1]===y
    )
    return result
}

export function getNeighbours([x, y]) {
    const neighbours = [
        [x-1,y-1], [x,y-1], [x+1,y-1], 
        [x-1,y],            [x+1,y], 
        [x-1,y+1], [x,y+1], [x+1,y+1]
    ];
    return neighbours
}

export function countNeighboursAlive([x, y], aliveList) {
    const neighbours = getNeighbours([x, y]);
    const countAlive = neighbours
        .filter(cell => isAlive(cell, aliveList))
        .length;
    return countAlive
}

export function updateAliveList(aliveList, xMin, xMax, yMin, yMax) {
    const isInBoard = cell => cell[0]>=xMin && cell[0]<=xMax && cell[1]>=yMin && cell[1]<=yMax;
    let updatedAliveList = [];
    let deadButRelevantCells = [];
    for (const cell of aliveList) {
        const neighbours = getNeighbours(cell);
        const deadNeighbours = neighbours.filter(cell => !isAlive(cell, aliveList));
        Array.prototype.push.apply(deadButRelevantCells, deadNeighbours);
        const numberOfDeadNeighbours = deadNeighbours.length;
        if (numberOfDeadNeighbours===5 || numberOfDeadNeighbours===6) {
            // this is the same rule as two or three neighbours alive (8-3=5, 8-2=6)
            if (isInBoard(cell)) {
                updatedAliveList.push(cell);
            }
        }
    }
    for (const cell of deadButRelevantCells) {
        const cellDuplicates = deadButRelevantCells.filter(([x, y]) => 
            cell[0]===x && cell[1]===y
        );
        if (cellDuplicates.length === 3) {
            // this happens if this dead cell has 3 neighbours alive
            if (!isAlive(cell, updatedAliveList)) {
                // only push if not already in updatedAliveList
                if (isInBoard(cell)) {
                    updatedAliveList.push(cell);
                }
            }
        }
    }
    return updatedAliveList
}

export function getXMax(aliveList) {
    const reducer = (currentMax, currentCell) => Math.max(currentCell[0], currentMax);
    const xMax = aliveList.reduce(reducer, 0);
    return xMax
}

export function getYMax(aliveList) {
    const reducer = (currentMax, currentCell) => Math.max(currentCell[1], currentMax);
    const yMax = aliveList.reduce(reducer, 0);
    return yMax
}

export function shiftAliveList(aliveList, deltaX, deltaY) {
    const shiftedAliveList = aliveList.map(cell => [cell[0]+deltaX, cell[1]+deltaY]);
    return shiftedAliveList
}

export function centerAliveList(aliveList, boardWidth, boardHeight) {
    const xMax = getXMax(aliveList);
    const yMax = getYMax(aliveList);
    const halfOfRemainingWidth = Math.round((boardWidth - xMax) / 2);
    const halfOfRemainingHeight = Math.round((boardHeight - yMax) / 2);
    return shiftAliveList(aliveList, halfOfRemainingWidth, halfOfRemainingHeight)
}