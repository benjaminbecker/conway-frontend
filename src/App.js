import React, { useState, useEffect } from 'react';
import GameOfLifeBoard from './components/GameOfLifeBoard';
import { updateAliveList, centerAliveList } from './logic/gameOfLife';
import useInterval from './hooks/useInterval';
import GameOfLifePreviewList from './components/GameOfLifePreviewList';

import './App.css'

import Container from 'react-bootstrap/Container'

import { faPause, faPlay, faStepForward, faUndo, faHamburger } from '@fortawesome/free-solid-svg-icons'

import rle_index from './rle/rle_index.js'
import useApi from './hooks/useApi';
import IconButton from './components/IconButton';

const interval = 200;
let games = Object.keys(rle_index).map((key, index) => {
  return {
    id: `game_${index}`,
    label: key,
    filename: rle_index[key]['fn'],
    aliveList: [],
    width: rle_index[key]['w'],
    height: rle_index[key]['h'],
  }
});

games = games.filter(game => game.width < 50 && game.height < 50)
games = games.sort((game1, game2) => {
  if (game1.label < game2.label) {
    return -1;
  }
  if (game1.label > game2.label) {
    return 1;
  }
  return 0;
})


function App() {
  const [aliveList, setAliveList] = useState([]);
  const [boardWidth, setBoardWidth] = useState(100);
  const [boardHeight, setBoardHeight] = useState(100);
  const [menuVisible, setMenuVisible] = useState(false);
  
  const [isLoading, data, setFilename] = useApi('gosperglidergun.rle');
  const aliveListInit = (data===undefined) ? [] : data.alive_list;
  const gameTitle = (data===undefined) ? '' : data.name;
  
  const [isRunning, setIsRunning] = useState(true);
  
  const [nTicks, setNTicks] = useState(0);
  
  const [justLoadedNewGame, setJustLoadedNewGame] = useState(true);
  
  const callbackNextStep = () => {
    if (justLoadedNewGame) {
      const aliveListCentered = centerAliveList(aliveListInit, boardWidth, boardHeight);
      setAliveList(aliveListCentered);
      setJustLoadedNewGame(false);
    } else {
      setAliveList(updateAliveList(aliveList, -5, boardWidth+5, -5, boardHeight+5));
      setNTicks(nTicks +  1);
    }
  }
  
  useInterval(callbackNextStep, isRunning ? interval : null, nTicks);
  
  const callbackReset = () => {
    const aliveListCentered = centerAliveList(aliveListInit, boardWidth, boardHeight);
    setAliveList(aliveListCentered);
    setNTicks(0);
  }
  
  const callbackLoadGame = (aliveListNew) => {
    // stop current game and remember if it was running
    let wasRunning = isRunning;
    setIsRunning(false);
    callbackReset();
    setIsRunning(wasRunning);
    setJustLoadedNewGame(true);
    setAliveList(aliveListNew);
    callbackReset();
  }
  
  const callbackPause = () => {
    setIsRunning(!isRunning);
  };
  
  useEffect(
    () => {callbackLoadGame(aliveListInit)}, [aliveListInit]
  );

  const callbackMenuButton = () => {
    setMenuVisible(!menuVisible);
  }

  const callbackSetFilenameAndCloseModal = (filename) => {
    setMenuVisible(false);
    setFilename(filename);
  }
  
  return (
    <div>
      <Container>
        <div class="title">{gameTitle}</div>
        <div class="conway-container">
          {
            isLoading ? (<div>Loading...</div>) : (<></>)
          }
          <GameOfLifeBoard n_x={boardWidth} n_y={boardHeight} aliveList={aliveList}/>
        </div>
        <div class="menu-bar">
          <IconButton label="Reset" icon={faUndo} onClick={callbackReset}/>
          <IconButton 
            label={isRunning ? "Pause" : "Play"} 
            icon={isRunning ? faPause : faPlay} 
            onClick={callbackPause}
          />
          <IconButton label="Step" icon={faStepForward} onClick={callbackNextStep} disabled={isRunning} />
          <IconButton label="Menu" icon={faHamburger} onClick={callbackMenuButton} />
        </div>
      </Container>
      {menuVisible && 
      <GameOfLifePreviewList 
        games={games} 
        callbackSetFilename={callbackSetFilenameAndCloseModal}
        show={menuVisible} 
        onHide={() => setMenuVisible(false)} 
      />}
    </div>
  );
}

export default App;
