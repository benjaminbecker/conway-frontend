const rle_index = {
    "Blocked p4-4": {
        "fn": "blockedp44.rle",
        "w": 19,
        "h": 9
    },
    "Beehive fuse": {
        "fn": "beehivefuse.rle",
        "w": 30,
        "h": 32
    },
    "Claw with tub with tail": {
        "fn": "clawwithtubwithtail.rle",
        "w": 6,
        "h": 7
    },
    "Mathematician": {
        "fn": "mathematician.rle",
        "w": 9,
        "h": 11
    },
    "Trans-boat with nine synth": {
        "fn": "transboatwithnine_synth.rle",
        "w": 138,
        "h": 88
    },
    "Snorkel loop synth": {
        "fn": "snorkelloop_synth.rle",
        "w": 33,
        "h": 17
    },
    "Birthday puffer": {
        "fn": "birthdaypuffer.rle",
        "w": 354,
        "h": 403
    },
    "Sir Robin": {
        "fn": "sirrobin.rle",
        "w": 31,
        "h": 79
    },
    "Ortho-loaf and table": {
        "fn": "ortholoafandtable.rle",
        "w": 5,
        "h": 7
    },
    "p3bumperalt2": {
        "fn": "p3bumperalt2.rle",
        "w": 59,
        "h": 51
    },
    "2x2 line puffer": {
        "fn": "2x2linepuffer.rle",
        "w": 5,
        "h": 9
    },
    "Glider duplicator 1": {
        "fn": "gliderduplicator1.rle",
        "w": 50,
        "h": 47
    },
    "47P72": {
        "fn": "47p72.rle",
        "w": 30,
        "h": 15
    },
    "23334M": {
        "fn": "23334m.rle",
        "w": 5,
        "h": 8
    },
    "Caterer on 48P31": {
        "fn": "catereron48p31.rle",
        "w": 24,
        "h": 27
    },
    "prepulsarshuttle40": {
        "fn": "prepulsarshuttle40.rle",
        "w": 22,
        "h": 35
    },
    "kleinbottle": {
        "fn": "kleinbottle.rle",
        "w": 100,
        "h": 100
    },
    "20p3": {
        "fn": "20p3.rle",
        "w": 15,
        "h": 5
    },
    "Tablecloth agar stabilizations": {
        "fn": "tablecloth_stabilizations.rle",
        "w": 50,
        "h": 30
    },
    "Glider emulator": {
        "fn": "glideremulator.rle",
        "w": 30,
        "h": 28
    },
    "Sombreros": {
        "fn": "sombreros.rle",
        "w": 18,
        "h": 11
    },
    "Pre-pulsar shuttle 58": {
        "fn": "prepulsarshuttle58.rle",
        "w": 25,
        "h": 27
    },
    "Achim's p144 synth": {
        "fn": "achimsp144_synth.rle",
        "w": 106,
        "h": 109
    },
    "Long ship": {
        "fn": "longship.rle",
        "w": 4,
        "h": 4
    },
    "119P4H1V0 tagalong": {
        "fn": "119p4h1v0tagalong.rle",
        "w": 44,
        "h": 19
    },
    "p5bumper": {
        "fn": "p5bumper.rle",
        "w": 59,
        "h": 52
    },
    "integralwithlonghookandtub": {
        "fn": "integralwithlonghookandtub.rle",
        "w": 6,
        "h": 8
    },
    "Very long integral": {
        "fn": "verylongintegral.rle",
        "w": 7,
        "h": 3
    },
    "blockpull": {
        "fn": "blockpull.rle",
        "w": 13,
        "h": 19
    },
    "Pre-pulsar hassler 55": {
        "fn": "prepulsarhassler55.rle",
        "w": 43,
        "h": 27
    },
    "Cis-beacon and cap": {
        "fn": "cisbeaconandcap.rle",
        "w": 4,
        "h": 8
    },
    "Pulsar": {
        "fn": "pulsar.rle",
        "w": 13,
        "h": 13
    },
    "Wasp": {
        "fn": "wasp.rle",
        "w": 22,
        "h": 9
    },
    "Super mango": {
        "fn": "supermango.rle",
        "w": 6,
        "h": 7
    },
    "Copperhead": {
        "fn": "copperhead.rle",
        "w": 8,
        "h": 12
    },
    "lwssgun1": {
        "fn": "lwssgun1.rle",
        "w": 40,
        "h": 42
    },
    "Caterer on 36P22": {
        "fn": "catereron36p22.rle",
        "w": 33,
        "h": 13
    },
    "eatertailsiameseeatertail": {
        "fn": "eatertailsiameseeatertail.rle",
        "w": 6,
        "h": 7
    },
    "Cord puller": {
        "fn": "cordpuller.rle",
        "w": 98,
        "h": 93
    },
    "diuresis synth": {
        "fn": "diuresis_synth.rle",
        "w": 144,
        "h": 58
    },
    "Long long hook with tail synth": {
        "fn": "longlonghookwithtail_synth.rle",
        "w": 108,
        "h": 91
    },
    "Bomber predecessor": {
        "fn": "bomberpredecessor.rle",
        "w": 10,
        "h": 6
    },
    "weekenderdistaff": {
        "fn": "weekenderdistaff.rle",
        "w": 281,
        "h": 2206
    },
    "Penny lane": {
        "fn": "pennylane.rle",
        "w": 15,
        "h": 10
    },
    "Mazing": {
        "fn": "mazing.rle",
        "w": 7,
        "h": 7
    },
    "Kickback reaction": {
        "fn": "kickbackreaction.rle",
        "w": 8,
        "h": 7
    },
    "roteightor extension": {
        "fn": "roteightorextension.rle",
        "w": 32,
        "h": 32
    },
    "Trans-boat and dock": {
        "fn": "transboatanddock.rle",
        "w": 6,
        "h": 7
    },
    "O-pentomino": {
        "fn": "opentomino.rle",
        "w": 5,
        "h": 1
    },
    "Infinite glider hotel 2": {
        "fn": "infinitegliderhotel2.rle",
        "w": 274,
        "h": 206
    },
    "hookwithnine": {
        "fn": "hookwithnine.rle",
        "w": 6,
        "h": 7
    },
    "Gunstar 2": {
        "fn": "gunstar2.rle",
        "w": 119,
        "h": 119
    },
    "verylongmelusine": {
        "fn": "verylongmelusine.rle",
        "w": 8,
        "h": 4
    },
    "4 boats": {
        "fn": "4boats.rle",
        "w": 8,
        "h": 8
    },
    "18p2.471 synth": {
        "fn": "18p2.471_synth.rle",
        "w": 110,
        "h": 43
    },
    "Light bulb": {
        "fn": "lightbulb.rle",
        "w": 7,
        "h": 9
    },
    "Beehive bend tail synth": {
        "fn": "beehivebendtail_synth.rle",
        "w": 114,
        "h": 101
    },
    "Knightwave": {
        "fn": "knightwave.rle",
        "w": 319,
        "h": 192
    },
    "Pre-pulsar shuttle 28 variant": {
        "fn": "prepulsarshuttle28variant.rle",
        "w": 22,
        "h": 17
    },
    "Replicator": {
        "fn": "replicator.rle",
        "w": 5,
        "h": 5
    },
    "HighLife 8-cell still lifes": {
        "fn": "highlife8cellstilllifes.rle",
        "w": 65,
        "h": 5
    },
    "toggleablegun": {
        "fn": "toggleablegun.rle",
        "w": 180,
        "h": 149
    },
    "Heavyweight emulator": {
        "fn": "heavyweightemulator.rle",
        "w": 16,
        "h": 7
    },
    "Pufferfish synth": {
        "fn": "pufferfish_synth.rle",
        "w": 59,
        "h": 51
    },
    "En retard": {
        "fn": "enretard.rle",
        "w": 11,
        "h": 11
    },
    "68p16": {
        "fn": "68p16.rle",
        "w": 20,
        "h": 20
    },
    "Q-pentomino": {
        "fn": "qpentomino.rle",
        "w": 4,
        "h": 2
    },
    "Switch engine channel": {
        "fn": "switchenginechannel.rle",
        "w": 56,
        "h": 56
    },
    "Edna": {
        "fn": "edna.rle",
        "w": 20,
        "h": 20
    },
    "Grin": {
        "fn": "grin.rle",
        "w": 4,
        "h": 2
    },
    "Twin hat": {
        "fn": "twinhat.rle",
        "w": 9,
        "h": 5
    },
    "integralwithtub synth": {
        "fn": "integralwithtub_synth.rle",
        "w": 152,
        "h": 17
    },
    "twirlingttetsons2 synth": {
        "fn": "twirlingttetsons2_synth.rle",
        "w": 304,
        "h": 141
    },
    "Syringe": {
        "fn": "syringegtoh.rle",
        "w": 24,
        "h": 34
    },
    "Garden of Eden 3": {
        "fn": "gardenofeden3.rle",
        "w": 13,
        "h": 12
    },
    "carriersiameseeatertail": {
        "fn": "carriersiameseeatertail.rle",
        "w": 4,
        "h": 7
    },
    "Pufferfish": {
        "fn": "pufferfish.rle",
        "w": 15,
        "h": 12
    },
    "long5snake": {
        "fn": "long5snake.rle",
        "w": 7,
        "h": 9
    },
    "Aircraft carrier synth": {
        "fn": "aircraftcarrier_synth.rle",
        "w": 146,
        "h": 90
    },
    "28P7.2 synth": {
        "fn": "28p7.2_synth.rle",
        "w": 87,
        "h": 83
    },
    "31c/240 Herschel-pair climber": {
        "fn": "31c240herschelpairclimber.rle",
        "w": 61,
        "h": 58
    },
    "98P25 synth": {
        "fn": "98p25_synth.rle",
        "w": 200,
        "h": 360
    },
    "catchandthrow": {
        "fn": "catchandthrow.rle",
        "w": 63,
        "h": 18
    },
    "69P48": {
        "fn": "69p48.rle",
        "w": 19,
        "h": 15
    },
    "24P2": {
        "fn": "24p2.rle",
        "w": 8,
        "h": 7
    },
    "Integral with two tubs": {
        "fn": "integralwithtwotubs.rle",
        "w": 7,
        "h": 7
    },
    "silvergtohplusf166": {
        "fn": "silvergtohplusf166.rle",
        "w": 125,
        "h": 102
    },
    "42883m": {
        "fn": "42883m.rle",
        "w": 16,
        "h": 16
    },
    "linemendingreaction": {
        "fn": "linemendingreaction.rle",
        "w": 135,
        "h": 135
    },
    "Biting off more than they can chew extended": {
        "fn": "bitingoffmorethantheycanchewextended.rle",
        "w": 16,
        "h": 15
    },
    "onesidedspaceshipsynthesis": {
        "fn": "onesidedspaceshipsynthesis.rle",
        "w": 23,
        "h": 41
    },
    "Ship on quadpole": {
        "fn": "shiponquadpole.rle",
        "w": 10,
        "h": 10
    },
    "ciscarrierdownontable": {
        "fn": "ciscarrierdownontable.rle",
        "w": 6,
        "h": 6
    },
    "119P4H1V0": {
        "fn": "119p4h1v0.rle",
        "w": 35,
        "h": 19
    },
    "carriertieship": {
        "fn": "carriertieship.rle",
        "w": 6,
        "h": 7
    },
    "T-nosed p4 on 56P27": {
        "fn": "tnosedp4on56p27.rle",
        "w": 34,
        "h": 32
    },
    "P48 toad sucker": {
        "fn": "p48toadsucker.rle",
        "w": 37,
        "h": 54
    },
    "Griddle and boat": {
        "fn": "griddleandboat.rle",
        "w": 6,
        "h": 8
    },
    "mutteringmoat1 synth": {
        "fn": "mutteringmoat1_synth.rle",
        "w": 140,
        "h": 44
    },
    "p50 glider shuttle": {
        "fn": "p50glidershuttle.rle",
        "w": 33,
        "h": 33
    },
    "Pseudo-barberpole": {
        "fn": "pseudobarberpole.rle",
        "w": 12,
        "h": 12
    },
    "integralwithhookandtail": {
        "fn": "integralwithhookandtail.rle",
        "w": 7,
        "h": 5
    },
    "Frog II": {
        "fn": "frogii.rle",
        "w": 11,
        "h": 13
    },
    "beehiveondock synth": {
        "fn": "beehiveondock_synth.rle",
        "w": 32,
        "h": 14
    },
    "Griddle and beehive": {
        "fn": "griddleandbeehive.rle",
        "w": 6,
        "h": 8
    },
    "60P33": {
        "fn": "60p33.rle",
        "w": 30,
        "h": 24
    },
    "verylongsnake synth": {
        "fn": "verylongsnake_synth.rle",
        "w": 151,
        "h": 27
    },
    "Backrake 1": {
        "fn": "backrake1.rle",
        "w": 27,
        "h": 18
    },
    "extensiblecrown": {
        "fn": "extensiblecrown.rle",
        "w": 52,
        "h": 39
    },
    "Cis-beacon and table synth": {
        "fn": "cisbeaconandtable_synth.rle",
        "w": 107,
        "h": 49
    },
    "80p6": {
        "fn": "80p6.rle",
        "w": 16,
        "h": 21
    },
    "Block on boat": {
        "fn": "blockonboat.rle",
        "w": 6,
        "h": 3
    },
    "heisen2": {
        "fn": "heisen2.rle",
        "w": 91,
        "h": 33
    },
    "ruler": {
        "fn": "ruler.rle",
        "w": 169,
        "h": 116
    },
    "Bipole on boat": {
        "fn": "bipoleonboat.rle",
        "w": 8,
        "h": 8
    },
    "cisbeacononanvil synth": {
        "fn": "cisbeacononanvilsynth.rle",
        "w": 139,
        "h": 117
    },
    "Deep cell": {
        "fn": "deepcell.rle",
        "w": 499,
        "h": 516
    },
    "Twirling T-tetson II": {
        "fn": "twirlingttetsonsii.rle",
        "w": 24,
        "h": 24
    },
    "Nine": {
        "fn": "nine.rle",
        "w": 3,
        "h": 4
    },
    "Long long hook with tail": {
        "fn": "longlonghookwithtail.rle",
        "w": 7,
        "h": 4
    },
    "Jason's p11": {
        "fn": "jasonsp11.rle",
        "w": 25,
        "h": 25
    },
    "lwsslwssdeflection": {
        "fn": "lwsslwssdeflection.rle",
        "w": 53,
        "h": 9
    },
    "600P6H1V0": {
        "fn": "600p6h1v0.rle",
        "w": 19,
        "h": 146
    },
    "Life without death quadratic growth": {
        "fn": "lifewithoutdeathquadraticgrowth.rle",
        "w": 47,
        "h": 47
    },
    "period52gun2": {
        "fn": "period52gun2.rle",
        "w": 345,
        "h": 302
    },
    "eaterheadsiameseeaterhead": {
        "fn": "eaterheadsiameseeaterhead.rle",
        "w": 6,
        "h": 7
    },
    "Trans-R-bee and R-loaf": {
        "fn": "transrbeeandrloaf.rle",
        "w": 5,
        "h": 8
    },
    "Fountain": {
        "fn": "fountain.rle",
        "w": 19,
        "h": 15
    },
    "Spiral": {
        "fn": "spiral.rle",
        "w": 7,
        "h": 7
    },
    "Long boat": {
        "fn": "longboat.rle",
        "w": 4,
        "h": 4
    },
    "smiley synth": {
        "fn": "smiley_synth.rle",
        "w": 28,
        "h": 46
    },
    "Pentadecathlon on snacker": {
        "fn": "pentadecathlononsnacker.rle",
        "w": 20,
        "h": 17
    },
    "5-engine Cordership": {
        "fn": "5enginecordership.rle",
        "w": 104,
        "h": 75
    },
    "113p18": {
        "fn": "113p18.rle",
        "w": 21,
        "h": 28
    },
    "274P6H1V0": {
        "fn": "274p6h1v0.rle",
        "w": 58,
        "h": 19
    },
    "21P2": {
        "fn": "21p2.rle",
        "w": 7,
        "h": 7
    },
    "weekender synth": {
        "fn": "weekender_synth.rle",
        "w": 188,
        "h": 177
    },
    "Cis-fuse with two tails": {
        "fn": "cisfusewithtwotails.rle",
        "w": 6,
        "h": 6
    },
    "jasonsp36caterers synth": {
        "fn": "jasonsp36caterers_synth.rle",
        "w": 228,
        "h": 45
    },
    "Washerwoman": {
        "fn": "washerwoman.rle",
        "w": 56,
        "h": 5
    },
    "carriersiameseshillelagh": {
        "fn": "carriersiameseshillelagh.rle",
        "w": 3,
        "h": 8
    },
    "R-bee and snake": {
        "fn": "rbeeandsnake.rle",
        "w": 5,
        "h": 6
    },
    "Honey thieves with test tube baby": {
        "fn": "honeythieveswithtesttubebaby.rle",
        "w": 16,
        "h": 19
    },
    "P160 dart gun": {
        "fn": "p160dartgun.rle",
        "w": 657,
        "h": 620
    },
    "quadpole synth": {
        "fn": "quadpole_synth.rle",
        "w": 33,
        "h": 17
    },
    "Burloaferimeter": {
        "fn": "burloaferimeter.rle",
        "w": 10,
        "h": 11
    },
    "Moon": {
        "fn": "moon.rle",
        "w": 2,
        "h": 4
    },
    "period33gun": {
        "fn": "period33gun.rle",
        "w": 57,
        "h": 37
    },
    "48P31": {
        "fn": "48p31.rle",
        "w": 24,
        "h": 13
    },
    "Long hook with tail": {
        "fn": "longhookwithtail.rle",
        "w": 6,
        "h": 4
    },
    "p59 Herschel loop 2": {
        "fn": "p59herschelloop2.rle",
        "w": 1074,
        "h": 74
    },
    "hatsiamesevase synth": {
        "fn": "hatsiamesevase_synth.rle",
        "w": 111,
        "h": 32
    },
    "Pre-beehive": {
        "fn": "prebeehive.rle",
        "w": 3,
        "h": 2
    },
    "Period 4 oscillators": {
        "fn": "period4oscillators.rle",
        "w": 294,
        "h": 198
    },
    "Surprise": {
        "fn": "surprise.rle",
        "w": 10,
        "h": 9
    },
    "parasite": {
        "fn": "parasite.rle",
        "w": 30,
        "h": 45
    },
    "twinhat synth": {
        "fn": "twinhat_synth.rle",
        "w": 110,
        "h": 170
    },
    "brokensnake": {
        "fn": "brokensnake.rle",
        "w": 3,
        "h": 7
    },
    "Blinker puffer 2": {
        "fn": "blinkerpuffer2.rle",
        "w": 17,
        "h": 25
    },
    "Cis-barge with tail": {
        "fn": "cisbargewithtail.rle",
        "w": 6,
        "h": 5
    },
    "transbeaconuponlonghook": {
        "fn": "transbeaconuponlonghook.rle",
        "w": 6,
        "h": 8
    },
    "92p51": {
        "fn": "92p51.rle",
        "w": 24,
        "h": 23
    },
    "Pushalong 1": {
        "fn": "pushalong1.rle",
        "w": 11,
        "h": 12
    },
    "Infinite glider hotel 3": {
        "fn": "infinitegliderhotel3.rle",
        "w": 256,
        "h": 157
    },
    "2c3wire": {
        "fn": "2c3wire.rle",
        "w": 69,
        "h": 66
    },
    "Boat with hooked tail": {
        "fn": "boatwithhookedtail.rle",
        "w": 6,
        "h": 4
    },
    "Double caterer": {
        "fn": "doublecaterer.rle",
        "w": 19,
        "h": 12
    },
    "long3snake": {
        "fn": "long3snake.rle",
        "w": 7,
        "h": 5
    },
    "g4receiver": {
        "fn": "g4receiver.rle",
        "w": 60,
        "h": 50
    },
    "Bi-loaf 4": {
        "fn": "biloaf4.rle",
        "w": 6,
        "h": 6
    },
    "Bookends": {
        "fn": "bookends.rle",
        "w": 7,
        "h": 4
    },
    "knightwavesupported": {
        "fn": "knightwavesupported.rle",
        "w": 123,
        "h": 195
    },
    "Hivenudger synth": {
        "fn": "hivenudger_synth.rle",
        "w": 154,
        "h": 109
    },
    "10-engine Cordership": {
        "fn": "10enginecordership.rle",
        "w": 88,
        "h": 88
    },
    "piclimber": {
        "fn": "piclimber.rle",
        "w": 23,
        "h": 58
    },
    "Blocked p4 t-nose hybrid": {
        "fn": "blockedp4tnosehybrid.rle",
        "w": 21,
        "h": 13
    },
    "p9bumper": {
        "fn": "p9bumper.rle",
        "w": 64,
        "h": 55
    },
    "duelingbanjosp48glidergun": {
        "fn": "duelingbanjosp48glidergun.rle",
        "w": 51,
        "h": 35
    },
    "Maze 8-cell still lifes": {
        "fn": "maze8cellstilllifes.rle",
        "w": 311,
        "h": 42
    },
    "Mosquito 1": {
        "fn": "mosquito1.rle",
        "w": 1794,
        "h": 412
    },
    "loaflipflop synth": {
        "fn": "loaflipflop_synth.rle",
        "w": 174,
        "h": 131
    },
    "verylongcishookwithtail": {
        "fn": "verylongcishookwithtail.rle",
        "w": 7,
        "h": 8
    },
    "Crab tubstretcher": {
        "fn": "crabtubstretcher.rle",
        "w": 13,
        "h": 15
    },
    "Waveguide 1": {
        "fn": "waveguide1.rle",
        "w": 53,
        "h": 21
    },
    "Carrier siamese carrier": {
        "fn": "carriersiamesecarrier.rle",
        "w": 7,
        "h": 4
    },
    "koksgalaxy synth": {
        "fn": "koksgalaxy_synth.rle",
        "w": 74,
        "h": 43
    },
    "Total periodic": {
        "fn": "totalperiodic.rle",
        "w": 60,
        "h": 59
    },
    "28p7.3bumperbouncer": {
        "fn": "28p7.3bumperbouncer.rle",
        "w": 79,
        "h": 64
    },
    "Tethered rake": {
        "fn": "tetheredrake.rle",
        "w": 134,
        "h": 92
    },
    "Vacuum (gun)": {
        "fn": "vacuumgun.rle",
        "w": 49,
        "h": 43
    },
    "Griddle and beehive synth": {
        "fn": "griddleandbeehive_synth.rle",
        "w": 159,
        "h": 71
    },
    "Pufferfish spaceship": {
        "fn": "pufferfishspaceship.rle",
        "w": 53,
        "h": 37
    },
    "HighLife 9-cell still lifes": {
        "fn": "highlife9cellstilllifes.rle",
        "w": 84,
        "h": 6
    },
    "Pre-pulsar shuttle 26": {
        "fn": "prepulsarshuttle26.rle",
        "w": 37,
        "h": 37
    },
    "HighLife 10-cell still lifes": {
        "fn": "highlife10cellstilllifes.rle",
        "w": 124,
        "h": 16
    },
    "Puffer 2": {
        "fn": "puffer2.rle",
        "w": 18,
        "h": 5
    },
    "Light speed oscillator 2": {
        "fn": "lightspeedoscillator2.rle",
        "w": 85,
        "h": 25
    },
    "84P87": {
        "fn": "84p87.rle",
        "w": 37,
        "h": 41
    },
    "30P5H2V0": {
        "fn": "30p5h2v0.rle",
        "w": 13,
        "h": 11
    },
    "Dinner table extension": {
        "fn": "dinnertableextension.rle",
        "w": 34,
        "h": 34
    },
    "blonkeronrichsp16": {
        "fn": "blonkeronrichsp16.rle",
        "w": 19,
        "h": 22
    },
    "Twirling T-tetsons 2": {
        "fn": "twirlingttetsons2.rle",
        "w": 24,
        "h": 24
    },
    "cismangowithtail": {
        "fn": "cismangowithtail.rle",
        "w": 7,
        "h": 5
    },
    "Sawtooth 1846": {
        "fn": "sawtooth1846.rle",
        "w": 370,
        "h": 118
    },
    "Honey thieves": {
        "fn": "honeythieves.rle",
        "w": 15,
        "h": 15
    },
    "factory": {
        "fn": "factory.rle",
        "w": 51,
        "h": 24
    },
    "cisblockonlonghook synth": {
        "fn": "cisblockonlonghook_synth.rle",
        "w": 157,
        "h": 139
    },
    "p448dartgun": {
        "fn": "p448dartgun.rle",
        "w": 2219,
        "h": 2173
    },
    "Clips": {
        "fn": "clips.rle",
        "w": 9,
        "h": 7
    },
    "hungryhat synth": {
        "fn": "hungryhat_synth.rle",
        "w": 34,
        "h": 12
    },
    "p32 blinker hassler": {
        "fn": "p32blinkerhassler.rle",
        "w": 21,
        "h": 23
    },
    "Cheshire cat": {
        "fn": "cheshirecat.rle",
        "w": 6,
        "h": 6
    },
    "pole4rotor": {
        "fn": "pole4rotor.rle",
        "w": 3,
        "h": 1
    },
    "zquadloaf": {
        "fn": "zquadloaf.rle",
        "w": 7,
        "h": 13
    },
    "rattlesnake synth": {
        "fn": "rattlesnake_synth.rle",
        "w": 834,
        "h": 80
    },
    "38P7.2 synth": {
        "fn": "38p7.2_synth.rle",
        "w": 175,
        "h": 37
    },
    "p52G3to4": {
        "fn": "p52g3to4.rle",
        "w": 211,
        "h": 161
    },
    "naturalheisenburp": {
        "fn": "naturalheisenburp.rle",
        "w": 141,
        "h": 141
    },
    "132p37 synth": {
        "fn": "132p37_synth.rle",
        "w": 317,
        "h": 160
    },
    "3-engine Cordership": {
        "fn": "3enginecordership.rle",
        "w": 58,
        "h": 65
    },
    "46P22": {
        "fn": "46p22.rle",
        "w": 17,
        "h": 16
    },
    "C-heptomino": {
        "fn": "cheptomino.rle",
        "w": 4,
        "h": 3
    },
    "R-pentomino synth": {
        "fn": "rpentomino_synth.rle",
        "w": 113,
        "h": 108
    },
    "Period 8 eater 3": {
        "fn": "period8eater3.rle",
        "w": 20,
        "h": 20
    },
    "Long Canada goose": {
        "fn": "longcanadagoose.rle",
        "w": 35,
        "h": 34
    },
    "44p72 synth": {
        "fn": "44p72_synth.rle",
        "w": 580,
        "h": 122
    },
    "Elevener": {
        "fn": "elevener.rle",
        "w": 6,
        "h": 6
    },
    "Snark-assisted period-59 glider gun": {
        "fn": "p59glidergungreene.rle",
        "w": 405,
        "h": 355
    },
    "transboatondock": {
        "fn": "transboatondock.rle",
        "w": 6,
        "h": 7
    },
    "piston synth": {
        "fn": "piston_synth.rle",
        "w": 32,
        "h": 12
    },
    "2x2 2-cell still lifes": {
        "fn": "2x22cellstilllifes.rle",
        "w": 5,
        "h": 2
    },
    "karelsp177 synth": {
        "fn": "karelsp177_synth.rle",
        "w": 167,
        "h": 88
    },
    "Mold on fumarole": {
        "fn": "moldonfumarole.rle",
        "w": 9,
        "h": 14
    },
    "Claw with tail synth": {
        "fn": "clawwithtail_synth.rle",
        "w": 106,
        "h": 98
    },
    "Interchange synth": {
        "fn": "interchange_synth.rle",
        "w": 76,
        "h": 106
    },
    "Long long shillelagh synth": {
        "fn": "longlongshillelagh_synth.rle",
        "w": 107,
        "h": 56
    },
    "Boat": {
        "fn": "boat.rle",
        "w": 3,
        "h": 3
    },
    "rf28bvariants": {
        "fn": "rf28bvariants.rle",
        "w": 18,
        "h": 77
    },
    "Seal": {
        "fn": "seal.rle",
        "w": 34,
        "h": 35
    },
    "period48glidergun": {
        "fn": "period48glidergun.rle",
        "w": 45,
        "h": 41
    },
    "tannersp46gun": {
        "fn": "tannersp46gun.rle",
        "w": 31,
        "h": 44
    },
    "Aries betwixt two blocks": {
        "fn": "ariesbetwixttwoblocks.rle",
        "w": 9,
        "h": 8
    },
    "verylongcanoe synth": {
        "fn": "verylongcanoe_synth.rle",
        "w": 110,
        "h": 78
    },
    "Cis-beacon and anvil": {
        "fn": "cisbeaconandanvil.rle",
        "w": 7,
        "h": 9
    },
    "corderheisenburpmodeld": {
        "fn": "corderheisenburpmodeld.rle",
        "w": 689,
        "h": 715
    },
    "Bi-loaf 3": {
        "fn": "biloaf3.rle",
        "w": 4,
        "h": 7
    },
    "signalelbow": {
        "fn": "signalelbow.rle",
        "w": 34,
        "h": 44
    },
    "p7bouncer": {
        "fn": "p7bouncer.rle",
        "w": 66,
        "h": 65
    },
    "Mangled 1 beacon": {
        "fn": "mangled1beacon.rle",
        "w": 7,
        "h": 7
    },
    "p66": {
        "fn": "p66.rle",
        "w": 35,
        "h": 35
    },
    "3c7wavestabilization": {
        "fn": "3c7wavestabilization.rle",
        "w": 223,
        "h": 139
    },
    "fumaroleonbeluchenkosp13": {
        "fn": "fumaroleonbeluchenkosp13.rle",
        "w": 23,
        "h": 19
    },
    "transblockonlonghook synth": {
        "fn": "transblockonlonghook_synth.rle",
        "w": 147,
        "h": 139
    },
    "alternativep7bumper": {
        "fn": "alternativep7bumper.rle",
        "w": 65,
        "h": 54
    },
    "4enginecorderships": {
        "fn": "4enginecorderships.rle",
        "w": 302,
        "h": 137
    },
    "Long long boat": {
        "fn": "longlongboat.rle",
        "w": 5,
        "h": 5
    },
    "finiteplane": {
        "fn": "finiteplane.rle",
        "w": 100,
        "h": 100
    },
    "Jam on 34P13.1": {
        "fn": "jamon34p13.1.rle",
        "w": 21,
        "h": 16
    },
    "Ellison p4 HW emulator": {
        "fn": "ellisonp4hwemulator.rle",
        "w": 24,
        "h": 9
    },
    "dendrite": {
        "fn": "dendrite.rle",
        "w": 269,
        "h": 490
    },
    "eater3 synth": {
        "fn": "eater3_synth.rle",
        "w": 158,
        "h": 50
    },
    "Very long clock": {
        "fn": "verylongclock.rle",
        "w": 12,
        "h": 8
    },
    "switchablelwssgun": {
        "fn": "switchablelwssgun.rle",
        "w": 122,
        "h": 100
    },
    "Broken lines": {
        "fn": "brokenlines.rle",
        "w": 227,
        "h": 118
    },
    "Bi-loaf 2 synth": {
        "fn": "biloaf2_synth.rle",
        "w": 152,
        "h": 46
    },
    "carriersiameselongsnake": {
        "fn": "carriersiameselongsnake.rle",
        "w": 4,
        "h": 8
    },
    "bakersdozenreactions": {
        "fn": "bakersdozenreactions.rle",
        "w": 35,
        "h": 28
    },
    "110P62": {
        "fn": "110p62.rle",
        "w": 41,
        "h": 26
    },
    "swminus2": {
        "fn": "swminus2.rle",
        "w": 42,
        "h": 44
    },
    "Pre-pulsar spaceship": {
        "fn": "prepulsarspaceship.rle",
        "w": 69,
        "h": 13
    },
    "78p70 synth": {
        "fn": "78p70_synth.rle",
        "w": 329,
        "h": 41
    },
    "Table and dock": {
        "fn": "tableanddock.rle",
        "w": 6,
        "h": 6
    },
    "Integral sign": {
        "fn": "integralsign.rle",
        "w": 5,
        "h": 5
    },
    "Almost knightship": {
        "fn": "almostknightship.rle",
        "w": 13,
        "h": 19
    },
    "gliderduplicator": {
        "fn": "gliderduplicator.rle",
        "w": 76,
        "h": 53
    },
    "p4reflector": {
        "fn": "p4reflector.rle",
        "w": 67,
        "h": 65
    },
    "Washing machine synth": {
        "fn": "washingmachine_synth.rle",
        "w": 37,
        "h": 27
    },
    "long3ship": {
        "fn": "long3ship.rle",
        "w": 6,
        "h": 6
    },
    "HighLife 5-cell still lifes": {
        "fn": "highlife5cellstilllifes.rle",
        "w": 3,
        "h": 3
    },
    "boatwithlong3tail": {
        "fn": "boatwithlong3tail.rle",
        "w": 4,
        "h": 8
    },
    "Boat with long tail synth": {
        "fn": "boatwithlongtail_synth.rle",
        "w": 68,
        "h": 23
    },
    "cisbeacondownonlonghook": {
        "fn": "cisbeacondownonlonghook.rle",
        "w": 5,
        "h": 8
    },
    "snakesiameseverylongsnake": {
        "fn": "snakesiameseverylongsnake.rle",
        "w": 4,
        "h": 9
    },
    "blocker synth": {
        "fn": "blocker_synth.rle",
        "w": 64,
        "h": 21
    },
    "Two pre-L hasslers": {
        "fn": "twoprelhasslers.rle",
        "w": 24,
        "h": 16
    },
    "snarkcatalystvariants": {
        "fn": "snarkcatalystvariants.rle",
        "w": 51,
        "h": 52
    },
    "Pond and dock": {
        "fn": "pondanddock.rle",
        "w": 8,
        "h": 6
    },
    "Ship synth": {
        "fn": "ship_synth.rle",
        "w": 148,
        "h": 104
    },
    "Beehive and long hook eating tub synth": {
        "fn": "beehiveandlonghookeatingtub_synth.rle",
        "w": 117,
        "h": 32
    },
    "statorlessp3": {
        "fn": "statorlessp3.rle",
        "w": 31,
        "h": 28
    },
    "blx19r": {
        "fn": "blx19r.rle",
        "w": 10,
        "h": 13
    },
    "permanentswitch": {
        "fn": "permanentswitch.rle",
        "w": 59,
        "h": 34
    },
    "2-engine Cordership": {
        "fn": "2enginecordership.rle",
        "w": 41,
        "h": 49
    },
    "tannersp46 synth": {
        "fn": "tannersp46_synth.rle",
        "w": 146,
        "h": 88
    },
    "104P25": {
        "fn": "104p25.rle",
        "w": 38,
        "h": 23
    },
    "Tumbler": {
        "fn": "tumbler.rle",
        "w": 9,
        "h": 5
    },
    "Gotts dots": {
        "fn": "gottsdots.rle",
        "w": 187,
        "h": 39
    },
    "Long integral": {
        "fn": "longintegral.rle",
        "w": 4,
        "h": 6
    },
    "beehiveoncap synth": {
        "fn": "beehiveoncap_synth.rle",
        "w": 26,
        "h": 14
    },
    "eaterblockfrob synth": {
        "fn": "eaterblockfrob_synth.rle",
        "w": 78,
        "h": 20
    },
    "gullwithtub": {
        "fn": "gullwithtub.rle",
        "w": 7,
        "h": 7
    },
    "bipond synth": {
        "fn": "bipond_synth.rle",
        "w": 41,
        "h": 16
    },
    "Star gate": {
        "fn": "stargate.rle",
        "w": 155,
        "h": 126
    },
    "56p27 synth": {
        "fn": "56p27_synth.rle",
        "w": 75,
        "h": 40
    },
    "herschel synth": {
        "fn": "herschel_synth.rle",
        "w": 12,
        "h": 14
    },
    "Baker": {
        "fn": "baker.rle",
        "w": 16,
        "h": 14
    },
    "34p14shuttle synth": {
        "fn": "34p14shuttle_synth.rle",
        "w": 161,
        "h": 121
    },
    "bicap synth": {
        "fn": "bicap_synth.rle",
        "w": 32,
        "h": 17
    },
    "Pi eater 2": {
        "fn": "pieater2.rle",
        "w": 23,
        "h": 4
    },
    "Switchwave": {
        "fn": "switchwave.rle",
        "w": 78,
        "h": 78
    },
    "24p10 synth": {
        "fn": "24p10_synth.rle",
        "w": 112,
        "h": 41
    },
    "Omnibus": {
        "fn": "omnibus.rle",
        "w": 9,
        "h": 10
    },
    "Sidewalk": {
        "fn": "sidewalk.rle",
        "w": 6,
        "h": 5
    },
    "boatwithcistail": {
        "fn": "boatwithcistail.rle",
        "w": 7,
        "h": 5
    },
    "R-mango and house": {
        "fn": "rmangoandhouse.rle",
        "w": 8,
        "h": 6
    },
    "Long prodigal": {
        "fn": "longprodigal.rle",
        "w": 7,
        "h": 5
    },
    "W-pentomino": {
        "fn": "wpentomino.rle",
        "w": 3,
        "h": 3
    },
    "124P37": {
        "fn": "124p37.rle",
        "w": 37,
        "h": 37
    },
    "Hungry hat": {
        "fn": "hungryhat.rle",
        "w": 6,
        "h": 5
    },
    "fox synth": {
        "fn": "fox_synth.rle",
        "w": 79,
        "h": 39
    },
    "Enterprise": {
        "fn": "enterprise.rle",
        "w": 21,
        "h": 21
    },
    "sokweshuttle": {
        "fn": "sokweshuttle.rle",
        "w": 307,
        "h": 125
    },
    "Blockade synth": {
        "fn": "blockade_synth.rle",
        "w": 134,
        "h": 96
    },
    "long9ship": {
        "fn": "long9ship.rle",
        "w": 12,
        "h": 12
    },
    "long3canoe": {
        "fn": "long3canoe.rle",
        "w": 8,
        "h": 8
    },
    "teardropwithclaw": {
        "fn": "teardropwithclaw.rle",
        "w": 6,
        "h": 5
    },
    "Decapole synth": {
        "fn": "decapole_synth.rle",
        "w": 73,
        "h": 33
    },
    "25-cell quadratic growth": {
        "fn": "25cellquadraticgrowth.rle",
        "w": 21372,
        "h": 172
    },
    "29P9": {
        "fn": "29p9.rle",
        "w": 10,
        "h": 10
    },
    "Long long boat synth": {
        "fn": "longlongboat_synth.rle",
        "w": 71,
        "h": 13
    },
    "tubwithtwoupcistails": {
        "fn": "tubwithtwoupcistails.rle",
        "w": 7,
        "h": 5
    },
    "lwssgliderbounce": {
        "fn": "lwssgliderbounce.rle",
        "w": 43,
        "h": 15
    },
    "Four boats synth": {
        "fn": "4boats_synth.rle",
        "w": 36,
        "h": 36
    },
    "Skewed traffic light": {
        "fn": "skewedtrafficlight.rle",
        "w": 24,
        "h": 22
    },
    "quad synth": {
        "fn": "quad_synth.rle",
        "w": 101,
        "h": 18
    },
    "Hammerhead": {
        "fn": "hammerhead.rle",
        "w": 18,
        "h": 16
    },
    "Thumb 1": {
        "fn": "thumb1.rle",
        "w": 13,
        "h": 9
    },
    "Quadri-Snark": {
        "fn": "quadrisnark.rle",
        "w": 25,
        "h": 22
    },
    "Long claw with tail": {
        "fn": "longclawwithtail.rle",
        "w": 7,
        "h": 4
    },
    "Twin bees shuttle interactions": {
        "fn": "twinbeesshuttleinteractions.rle",
        "w": 192,
        "h": 109
    },
    "P94S2": {
        "fn": "p94s2.rle",
        "w": 174,
        "h": 149
    },
    "lifehistoryexample": {
        "fn": "lifehistoryexample.rle",
        "w": 37,
        "h": 36
    },
    "French kiss": {
        "fn": "frenchkiss.rle",
        "w": 10,
        "h": 9
    },
    "Cap": {
        "fn": "cap.rle",
        "w": 4,
        "h": 3
    },
    "Toad": {
        "fn": "toad.rle",
        "w": 4,
        "h": 2
    },
    "Rattlesnake": {
        "fn": "rattlesnake.rle",
        "w": 13,
        "h": 15
    },
    "Candlefrobra synth": {
        "fn": "candlefrobra_synth.rle",
        "w": 117,
        "h": 56
    },
    "Boat on aircraft": {
        "fn": "boatonaircraft.rle",
        "w": 6,
        "h": 7
    },
    "snakedance synth": {
        "fn": "snakedance_synth.rle",
        "w": 69,
        "h": 47
    },
    "Trans-mirrored R-bee synth": {
        "fn": "transmirroredrbee_synth.rle",
        "w": 107,
        "h": 23
    },
    "230p8": {
        "fn": "230p8.rle",
        "w": 49,
        "h": 20
    },
    "Pi portraitor": {
        "fn": "piportraitor.rle",
        "w": 24,
        "h": 24
    },
    "Glider-producing switch engine predecessor": {
        "fn": "gliderproducingswitchenginepredecessor.rle",
        "w": 28,
        "h": 6
    },
    "hexapole synth,rle": {
        "fn": "hexapole_synth.rle",
        "w": 65,
        "h": 25
    },
    "tricetongs synth": {
        "fn": "tricetongs_synth.rle",
        "w": 129,
        "h": 139
    },
    "P56 B-heptomino shuttle": {
        "fn": "p56bheptominoshuttle.rle",
        "w": 26,
        "h": 21
    },
    "Griddle and table": {
        "fn": "griddleandtable.rle",
        "w": 6,
        "h": 7
    },
    "Sawtooth 562": {
        "fn": "sawtooth562.rle",
        "w": 114,
        "h": 74
    },
    "Dock siamese carrier": {
        "fn": "docksiamesecarrier.rle",
        "w": 6,
        "h": 6
    },
    "Cis-mirrored R-bee synth": {
        "fn": "cismirroredrbee_synth.rle",
        "w": 95,
        "h": 46
    },
    "Original diuresis": {
        "fn": "originaldiuresis.rle",
        "w": 44,
        "h": 21
    },
    "Bistable switch": {
        "fn": "bistableswitch.rle",
        "w": 75,
        "h": 61
    },
    "Period 2 oscillators": {
        "fn": "period2oscillators.rle",
        "w": 231,
        "h": 180
    },
    "cislongbargewithtail": {
        "fn": "cislongbargewithtail.rle",
        "w": 7,
        "h": 5
    },
    "fourskewedblocks": {
        "fn": "fourskewedblocks.rle",
        "w": 10,
        "h": 13
    },
    "Figure eight on 34P13": {
        "fn": "figureeighton34p13.rle",
        "w": 16,
        "h": 22
    },
    "Snake siamese snake synth": {
        "fn": "snakesiamesesnake_synth.rle",
        "w": 141,
        "h": 117
    },
    "$rats": {
        "fn": "rats.rle",
        "w": 12,
        "h": 11
    },
    "Swan boatstretcher": {
        "fn": "swanboatstretcher.rle",
        "w": 24,
        "h": 15
    },
    "Switch engine": {
        "fn": "switchengine.rle",
        "w": 6,
        "h": 4
    },
    "Amphisbaena": {
        "fn": "amphisbaena.rle",
        "w": 7,
        "h": 5
    },
    "4-engine Cordership": {
        "fn": "4enginecordership.rle",
        "w": 76,
        "h": 76
    },
    "112P51 synth": {
        "fn": "112p51_synth.rle",
        "w": 187,
        "h": 187
    },
    "transboatontable synth": {
        "fn": "transboatontable_synth.rle",
        "w": 40,
        "h": 10
    },
    "60P5H2V0 synth": {
        "fn": "60p5h2v0_synth.rle",
        "w": 179,
        "h": 173
    },
    "Cis-boat and dock": {
        "fn": "cisboatanddock.rle",
        "w": 6,
        "h": 7
    },
    "rbeewithfeather": {
        "fn": "rbeewithfeather.rle",
        "w": 6,
        "h": 5
    },
    "13-engine Cordership": {
        "fn": "13enginecordership.rle",
        "w": 97,
        "h": 98
    },
    "long6boat": {
        "fn": "long6boat.rle",
        "w": 9,
        "h": 9
    },
    "Spark coil synth": {
        "fn": "sparkcoil_synth.rle",
        "w": 75,
        "h": 8
    },
    "40enginecordership": {
        "fn": "40enginecordership.rle",
        "w": 436,
        "h": 436
    },
    "Dragon flotillae": {
        "fn": "dragonflotillae.rle",
        "w": 62,
        "h": 30
    },
    "spider synth": {
        "fn": "spider_synth.rle",
        "w": 25298,
        "h": 4913
    },
    "byflops synth": {
        "fn": "byflops_synth.rle",
        "w": 70,
        "h": 53
    },
    "98P25": {
        "fn": "98p25.rle",
        "w": 38,
        "h": 23
    },
    "Honeythieves synth": {
        "fn": "honeythieves_synth.rle",
        "w": 135,
        "h": 84
    },
    "Phoenix 1 extended": {
        "fn": "phoenix1extended.rle",
        "w": 16,
        "h": 16
    },
    "Beacon synth": {
        "fn": "beacon_synth.rle",
        "w": 151,
        "h": 106
    },
    "LWSS tagalong": {
        "fn": "lwsstagalong.rle",
        "w": 25,
        "h": 19
    },
    "3c/7 wave": {
        "fn": "3c7wave.rle",
        "w": 63,
        "h": 9
    },
    "Bi-loaf 2": {
        "fn": "biloaf2.rle",
        "w": 7,
        "h": 7
    },
    "128P13.1": {
        "fn": "128p13.1.rle",
        "w": 36,
        "h": 36
    },
    "Herschel receiver": {
        "fn": "herschelreceiver.rle",
        "w": 50,
        "h": 33
    },
    "72P6H2V0": {
        "fn": "72p6h2v0.rle",
        "w": 25,
        "h": 14
    },
    "transcarriertiesnake": {
        "fn": "transcarriertiesnake.rle",
        "w": 6,
        "h": 7
    },
    "p43 glider loop": {
        "fn": "p43gliderloop.rle",
        "w": 65,
        "h": 65
    },
    "135-degree MWSS-to-G": {
        "fn": "135degreemwsstog.rle",
        "w": 14,
        "h": 11
    },
    "3-engine Cordership gun": {
        "fn": "3enginecordershipgun.rle",
        "w": 279,
        "h": 258
    },
    "Hivenudger 2": {
        "fn": "hivenudger2.rle",
        "w": 30,
        "h": 27
    },
    "dayandnightfireball": {
        "fn": "dayandnightfireball.rle",
        "w": 11,
        "h": 51
    },
    "Ship-tie synth": {
        "fn": "shiptie_synth.rle",
        "w": 151,
        "h": 116
    },
    "Negentropy": {
        "fn": "negentropy.rle",
        "w": 11,
        "h": 12
    },
    "p46 gun": {
        "fn": "p46gun.rle",
        "w": 49,
        "h": 14
    },
    "p52gun3": {
        "fn": "p52gun3.rle",
        "w": 126,
        "h": 97
    },
    "Dead spark coil synth": {
        "fn": "deadsparkcoil_synth.rle",
        "w": 74,
        "h": 8
    },
    "p130shuttle": {
        "fn": "p130shuttle.rle",
        "w": 46,
        "h": 27
    },
    "Long cis-hook with tail": {
        "fn": "longcishookwithtail.rle",
        "w": 6,
        "h": 7
    },
    "22p2 synth": {
        "fn": "22p2_synth.rle",
        "w": 61,
        "h": 19
    },
    "105P25": {
        "fn": "105p25.rle",
        "w": 34,
        "h": 23
    },
    "Backrake 2": {
        "fn": "backrake2.rle",
        "w": 19,
        "h": 26
    },
    "2x2 8-cell still lifes": {
        "fn": "2x28cellstilllifes.rle",
        "w": 111,
        "h": 17
    },
    "Scot's p5": {
        "fn": "scotsp5.rle",
        "w": 13,
        "h": 7
    },
    "Bi-pond": {
        "fn": "bipond.rle",
        "w": 7,
        "h": 7
    },
    "transshipontable": {
        "fn": "transshipontable.rle",
        "w": 6,
        "h": 5
    },
    "Centinal synth": {
        "fn": "centinal_synth.rle",
        "w": 112,
        "h": 104
    },
    "Small lake": {
        "fn": "smalllake.rle",
        "w": 9,
        "h": 9
    },
    "P18 honey farm hassler": {
        "fn": "p18honeyfarmhassler.rle",
        "w": 30,
        "h": 18
    },
    "Flying wing": {
        "fn": "flyingwing.rle",
        "w": 159,
        "h": 90
    },
    "Mold on rattlesnake": {
        "fn": "moldonrattlesnake.rle",
        "w": 14,
        "h": 15
    },
    "nw31": {
        "fn": "nw31.rle",
        "w": 25,
        "h": 25
    },
    "Garden of Eden 6": {
        "fn": "gardenofeden6.rle",
        "w": 11,
        "h": 10
    },
    "blonker synth": {
        "fn": "blonker_synth.rle",
        "w": 42,
        "h": 32
    },
    "ciscarrierupontable": {
        "fn": "ciscarrierupontable.rle",
        "w": 6,
        "h": 4
    },
    "p44 guns": {
        "fn": "p44guns.rle",
        "w": 212,
        "h": 60
    },
    "Sailboat": {
        "fn": "sailboat.rle",
        "w": 29,
        "h": 25
    },
    "21P2 synth": {
        "fn": "21p2_synth.rle",
        "w": 112,
        "h": 17
    },
    "verylongbarge": {
        "fn": "verylongbarge.rle",
        "w": 6,
        "h": 6
    },
    "Crab": {
        "fn": "crab.rle",
        "w": 13,
        "h": 12
    },
    "Longhook and dock": {
        "fn": "longhookanddock.rle",
        "w": 7,
        "h": 6
    },
    "Crab synthesis": {
        "fn": "crab_synth.rle",
        "w": 106,
        "h": 35
    },
    "34P64": {
        "fn": "34p64.rle",
        "w": 21,
        "h": 21
    },
    "Maze still lifes": {
        "fn": "mazestilllifes.rle",
        "w": 122,
        "h": 22
    },
    "bunnies10aoriginal": {
        "fn": "bunnies10aoriginal.rle",
        "w": 8,
        "h": 5
    },
    "Cis-hook and R-bee": {
        "fn": "cishookandrbee.rle",
        "w": 4,
        "h": 7
    },
    "Bipole bridge pseudo-barberpole": {
        "fn": "bipolebridgepseudobarberpole.rle",
        "w": 17,
        "h": 17
    },
    "67P5H1V1": {
        "fn": "67p5h1v1.rle",
        "w": 22,
        "h": 20
    },
    "twinbeesshuttlespark": {
        "fn": "twinbeesshuttlespark.rle",
        "w": 5,
        "h": 5
    },
    "transboatontable": {
        "fn": "transboatontable.rle",
        "w": 6,
        "h": 5
    },
    "gliderto2ec": {
        "fn": "gliderto2ec.rle",
        "w": 561,
        "h": 544
    },
    "Superfountain": {
        "fn": "superfountain.rle",
        "w": 46,
        "h": 25
    },
    "Cis-shillelagh": {
        "fn": "cisshillelagh.rle",
        "w": 6,
        "h": 5
    },
    "Clock": {
        "fn": "clock.rle",
        "w": 4,
        "h": 4
    },
    "Blonker": {
        "fn": "blonker.rle",
        "w": 12,
        "h": 8
    },
    "Toad synth": {
        "fn": "toad_synth.rle",
        "w": 153,
        "h": 115
    },
    "Claw test tube baby": {
        "fn": "clawtesttubebaby.rle",
        "w": 12,
        "h": 6
    },
    "Washing machine": {
        "fn": "washingmachine.rle",
        "w": 7,
        "h": 7
    },
    "Snake with feather": {
        "fn": "snakewithfeather.rle",
        "w": 7,
        "h": 4
    },
    "2x2 9-cell still lifes": {
        "fn": "2x29cellstilllifes.rle",
        "w": 229,
        "h": 17
    },
    "HighLife 11-cell still lifes": {
        "fn": "highlife11cellstilllifes.rle",
        "w": 115,
        "h": 42
    },
    "Cis-boat with tail synth": {
        "fn": "cisboatwithtail_synth.rle",
        "w": 65,
        "h": 36
    },
    "Gunstar": {
        "fn": "gunstar.rle",
        "w": 149,
        "h": 149
    },
    "boundingdiamond": {
        "fn": "boundingdiamond.rle",
        "w": 73,
        "h": 10
    },
    "Noah's ark synth": {
        "fn": "noahsark_synth.rle",
        "w": 120,
        "h": 52
    },
    "P59 Herschel loop 1": {
        "fn": "p59herschelloop1.rle",
        "w": 574,
        "h": 574
    },
    "Turtle": {
        "fn": "turtle.rle",
        "w": 12,
        "h": 10
    },
    "Simkin glider gun": {
        "fn": "simkinglidergun.rle",
        "w": 33,
        "h": 21
    },
    "28P7.1": {
        "fn": "28p71.rle",
        "w": 13,
        "h": 9
    },
    "catereron28p7.3": {
        "fn": "catereron28p7.3.rle",
        "w": 17,
        "h": 13
    },
    "fourreagents": {
        "fn": "fourreagents.rle",
        "w": 32,
        "h": 32
    },
    "Inflected 30-great sym": {
        "fn": "inflected30greatsym.rle",
        "w": 9,
        "h": 8
    },
    "126p78.1": {
        "fn": "126p781.rle",
        "w": 23,
        "h": 34
    },
    "jam synth": {
        "fn": "jam_synth.rle",
        "w": 107,
        "h": 50
    },
    "Biting off more than they can chew": {
        "fn": "bitingoffmorethantheycanchew.rle",
        "w": 12,
        "h": 12
    },
    "Achim's p8": {
        "fn": "achimsp8.rle",
        "w": 9,
        "h": 9
    },
    "Snake": {
        "fn": "snake.rle",
        "w": 4,
        "h": 2
    },
    "Super loaf": {
        "fn": "superloaf.rle",
        "w": 7,
        "h": 7
    },
    "Barge (extended)": {
        "fn": "bargeextended.rle",
        "w": 101,
        "h": 67
    },
    "Frothing puffer": {
        "fn": "frothingpuffer.rle",
        "w": 33,
        "h": 23
    },
    "Ship maker": {
        "fn": "shipmaker.rle",
        "w": 41,
        "h": 40
    },
    "Pentadecathlon": {
        "fn": "pentadecathlon.rle",
        "w": 10,
        "h": 3
    },
    "65P48": {
        "fn": "65p48.rle",
        "w": 21,
        "h": 16
    },
    "Trans-beacon and cap synth": {
        "fn": "transbeaconandcap_synth.rle",
        "w": 108,
        "h": 12
    },
    "Queen bee synth": {
        "fn": "queenbee_synth.rle",
        "w": 110,
        "h": 84
    },
    "Four eaters hassling four bookends": {
        "fn": "foureatershasslingfourbookends.rle",
        "w": 29,
        "h": 29
    },
    "Uninteresting p24": {
        "fn": "uninterestingp24.rle",
        "w": 26,
        "h": 15
    },
    "Candlefrobra": {
        "fn": "candlefrobra.rle",
        "w": 10,
        "h": 5
    },
    "Cloverleaf interchange": {
        "fn": "cloverleafinterchange.rle",
        "w": 13,
        "h": 13
    },
    "omnibuspredecessor": {
        "fn": "omnibuspredecessor.rle",
        "w": 14,
        "h": 3
    },
    "Middleweight emulator": {
        "fn": "middleweightemulator.rle",
        "w": 15,
        "h": 7
    },
    "29-1": {
        "fn": "29bitstilllifeno1_synth.rle",
        "w": 68,
        "h": 49
    },
    "transbargewithnine": {
        "fn": "transbargewithnine.rle",
        "w": 7,
        "h": 7
    },
    "33p3.1bumper": {
        "fn": "33p3.1bumper.rle",
        "w": 99,
        "h": 99
    },
    "p24 shuttle": {
        "fn": "p24shuttle.rle",
        "w": 20,
        "h": 20
    },
    "Richsp16 synth": {
        "fn": "richsp16_synth.rle",
        "w": 108,
        "h": 45
    },
    "reanimationbeehive": {
        "fn": "reanimationbeehive.rle",
        "w": 34,
        "h": 25
    },
    "beehiveontable": {
        "fn": "beehiveontable.rle",
        "w": 6,
        "h": 5
    },
    "Star": {
        "fn": "star.rle",
        "w": 11,
        "h": 11
    },
    "diehard1638": {
        "fn": "diehard1638.rle",
        "w": 31,
        "h": 31
    },
    "hheptomino": {
        "fn": "hheptomino.rle",
        "w": 4,
        "h": 4
    },
    "Queen bee turner": {
        "fn": "queenbeeturner.rle",
        "w": 155,
        "h": 155
    },
    "sirrobineater": {
        "fn": "sirrobineater.rle",
        "w": 176,
        "h": 354
    },
    "Quasar": {
        "fn": "quasar.rle",
        "w": 29,
        "h": 29
    },
    "48P22.1": {
        "fn": "48p221.rle",
        "w": 16,
        "h": 16
    },
    "beaconontableisomers": {
        "fn": "beaconontableisomers.rle",
        "w": 15,
        "h": 7
    },
    "38P24": {
        "fn": "38p24.rle",
        "w": 20,
        "h": 20
    },
    "prepulsarshuttle47v2 synth": {
        "fn": "prepulsarshuttle47v2_synth.rle",
        "w": 1516,
        "h": 88
    },
    "semicenark42tickrecovery": {
        "fn": "semicenark42tickrecovery.rle",
        "w": 37,
        "h": 37
    },
    "cauldron synth": {
        "fn": "cauldron_synth.rle",
        "w": 926,
        "h": 210
    },
    "boatonlongsnake": {
        "fn": "boatonlongsnake.rle",
        "w": 6,
        "h": 8
    },
    "Beehive test tube baby": {
        "fn": "beehivetesttubebaby.rle",
        "w": 10,
        "h": 6
    },
    "True period 22 gun": {
        "fn": "trueperiod22gun.rle",
        "w": 45,
        "h": 21
    },
    "biloafpgg": {
        "fn": "biloafpgg.rle",
        "w": 4,
        "h": 7
    },
    "Cis-R-bee and R-loaf": {
        "fn": "cisrbeeandrloaf.rle",
        "w": 4,
        "h": 8
    },
    "6bits synth": {
        "fn": "6bits_synth.rle",
        "w": 226,
        "h": 30
    },
    "Confused eaters": {
        "fn": "confusedeaters.rle",
        "w": 11,
        "h": 11
    },
    "p45 engine": {
        "fn": "p45engine.rle",
        "w": 42,
        "h": 42
    },
    "38P7.2": {
        "fn": "38p7.2.rle",
        "w": 13,
        "h": 11
    },
    "Pseudo-barberpole on rattlesnake": {
        "fn": "pseudobarberpoleonrattlesnake.rle",
        "w": 13,
        "h": 22
    },
    "originalsbmpull": {
        "fn": "originalsbmpull.rle",
        "w": 12,
        "h": 9
    },
    "whynot synth": {
        "fn": "whynot_synth.rle",
        "w": 40,
        "h": 22
    },
    "carriersiamesehookwithtailhook": {
        "fn": "carriersiamesehookwithtailhook.rle",
        "w": 8,
        "h": 4
    },
    "Mold": {
        "fn": "mold.rle",
        "w": 6,
        "h": 6
    },
    "Snake dance": {
        "fn": "snakedance.rle",
        "w": 9,
        "h": 9
    },
    "inflexion": {
        "fn": "inflexion.rle",
        "w": 7,
        "h": 4
    },
    "34p14shuttle": {
        "fn": "34p14shuttle.rle",
        "w": 15,
        "h": 11
    },
    "syringeGtoH synth": {
        "fn": "syringegtoh_synth.rle",
        "w": 301,
        "h": 274
    },
    "77P6H1V1": {
        "fn": "77p6h1v1.rle",
        "w": 36,
        "h": 36
    },
    "Pre-pulsar shuttle 47": {
        "fn": "prepulsarshuttle47.rle",
        "w": 31,
        "h": 30
    },
    "long4snake": {
        "fn": "long4snake.rle",
        "w": 8,
        "h": 6
    },
    "Fly extension": {
        "fn": "flyextension.rle",
        "w": 60,
        "h": 28
    },
    "Jam on 44P7.2": {
        "fn": "jamon44p7.2.rle",
        "w": 17,
        "h": 16
    },
    "tubwithtailwithcape": {
        "fn": "tubwithtailwithcape.rle",
        "w": 7,
        "h": 5
    },
    "caposcillator": {
        "fn": "caposcillator.rle",
        "w": 14,
        "h": 6
    },
    "Conduit 1": {
        "fn": "conduit1.rle",
        "w": 11,
        "h": 5
    },
    "Fox": {
        "fn": "fox.rle",
        "w": 7,
        "h": 7
    },
    "Gray counter": {
        "fn": "graycounter.rle",
        "w": 13,
        "h": 9
    },
    "Pipsquirter 2 reflector": {
        "fn": "pipsquirter2reflector.rle",
        "w": 21,
        "h": 25
    },
    "U-pentomino": {
        "fn": "upentomino.rle",
        "w": 3,
        "h": 2
    },
    "295P5H1V1": {
        "fn": "295p5h1v1.rle",
        "w": 52,
        "h": 52
    },
    "cissnaketie": {
        "fn": "cissnaketie.rle",
        "w": 4,
        "h": 8
    },
    "Sidesnagger": {
        "fn": "sidesnagger.rle",
        "w": 13,
        "h": 9
    },
    "caaba synth": {
        "fn": "caaba_synth.rle",
        "w": 110,
        "h": 43
    },
    "Swan weld goose": {
        "fn": "swanweldgoose.rle",
        "w": 34,
        "h": 21
    },
    "newfx176": {
        "fn": "newfx176.rle",
        "w": 50,
        "h": 31
    },
    "quasar3": {
        "fn": "quasar3.rle",
        "w": 61,
        "h": 61
    },
    "Fly": {
        "fn": "fly.rle",
        "w": 34,
        "h": 20
    },
    "verylongshillelagh": {
        "fn": "verylongshillelagh.rle",
        "w": 4,
        "h": 7
    },
    "Figure eight on 36P22": {
        "fn": "figureeighton36p22.rle",
        "w": 27,
        "h": 21
    },
    "Caterer on 68P32": {
        "fn": "catereron68p32.rle",
        "w": 29,
        "h": 23
    },
    "Bottle": {
        "fn": "bottle.rle",
        "w": 18,
        "h": 18
    },
    "period246glidergun": {
        "fn": "period246glidergun.rle",
        "w": 43,
        "h": 38
    },
    "ruler2": {
        "fn": "ruler2.rle",
        "w": 161,
        "h": 118
    },
    "Hook with tail synth": {
        "fn": "hookwithtail_synth.rle",
        "w": 68,
        "h": 76
    },
    "Herschel transmitter (stable)": {
        "fn": "herscheltransmitterstable.rle",
        "w": 19,
        "h": 18
    },
    "Star with blocks": {
        "fn": "starwithblocks.rle",
        "w": 11,
        "h": 11
    },
    "Fly on 60P3H1V0.3": {
        "fn": "flyon60p3h1v0.3.rle",
        "w": 34,
        "h": 33
    },
    "Period-61 glider gun": {
        "fn": "smallerp61gun.rle",
        "w": 204,
        "h": 185
    },
    "2x2 block oscillators": {
        "fn": "2x2blockoscillators.rle",
        "w": 26,
        "h": 2
    },
    "eaterwithheadfeather": {
        "fn": "eaterwithheadfeather.rle",
        "w": 7,
        "h": 5
    },
    "Mold synth": {
        "fn": "mold_synth.rle",
        "w": 146,
        "h": 106
    },
    "phoenix1 synth": {
        "fn": "phoenix1_synth.rle",
        "w": 33,
        "h": 14
    },
    "speedbooster": {
        "fn": "speedbooster.rle",
        "w": 64,
        "h": 23
    },
    "queenbeeshuttlepair": {
        "fn": "queenbeeshuttlepair.rle",
        "w": 25,
        "h": 27
    },
    "p36 toadsucker": {
        "fn": "p36toadsucker.rle",
        "w": 21,
        "h": 26
    },
    "Block and dock synth": {
        "fn": "blockanddock_synth.rle",
        "w": 120,
        "h": 106
    },
    "bouncerr2": {
        "fn": "bouncerr2.rle",
        "w": 29,
        "h": 50
    },
    "Cha cha": {
        "fn": "chacha.rle",
        "w": 8,
        "h": 6
    },
    "Killer toads": {
        "fn": "killertoads.rle",
        "w": 4,
        "h": 7
    },
    "Achim's p144": {
        "fn": "achimsp144.rle",
        "w": 28,
        "h": 19
    },
    "Scrubber with blocks": {
        "fn": "scrubber_with_blocks.rle",
        "w": 11,
        "h": 11
    },
    "Carrier siamese snake": {
        "fn": "carriersiamesesnake.rle",
        "w": 7,
        "h": 3
    },
    "Cis-mirrored professor": {
        "fn": "cismirroredprofessor.rle",
        "w": 9,
        "h": 11
    },
    "Statorless p5": {
        "fn": "statorlessp5.rle",
        "w": 15,
        "h": 14
    },
    "Period-59 glider gun (8k-by-8k)": {
        "fn": "p59glidergun8kx8k.rle",
        "w": 8870,
        "h": 7352
    },
    "Backrake 3": {
        "fn": "backrake3.rle",
        "w": 33,
        "h": 103
    },
    "highwayrobber": {
        "fn": "highwayrobber.rle",
        "w": 283,
        "h": 348
    },
    "p31 reflection": {
        "fn": "p31reflection.rle",
        "w": 35,
        "h": 32
    },
    "Hexapole": {
        "fn": "hexapole.rle",
        "w": 9,
        "h": 9
    },
    "copperhead synth": {
        "fn": "copperhead_synth.rle",
        "w": 48,
        "h": 46
    },
    "p67 glider loop": {
        "fn": "p67gliderloop.rle",
        "w": 89,
        "h": 89
    },
    "Babbling brook 1": {
        "fn": "babblingbrook1.rle",
        "w": 16,
        "h": 10
    },
    "Star gate with lightweight spaceships": {
        "fn": "stargatewithlightweightspaceships.rle",
        "w": 155,
        "h": 133
    },
    "7-in-a-row Cordership eater": {
        "fn": "7inarowcordershipeater.rle",
        "w": 420,
        "h": 428
    },
    "Rich's p16": {
        "fn": "richsp16.rle",
        "w": 13,
        "h": 10
    },
    "Justyna": {
        "fn": "justyna.rle",
        "w": 22,
        "h": 17
    },
    "B3578/S238 replicator": {
        "fn": "b3578s238replicator.rle",
        "w": 7,
        "h": 7
    },
    "4-engine Cordership synth": {
        "fn": "4enginecordership_synth.rle",
        "w": 131,
        "h": 131
    },
    "eater4 synth": {
        "fn": "eater4_synth.rle",
        "w": 125,
        "h": 125
    },
    "Claw": {
        "fn": "claw.rle",
        "w": 4,
        "h": 3
    },
    "Weekender tagalong": {
        "fn": "weekendertagalong.rle",
        "w": 37,
        "h": 24
    },
    "Loaf test tube baby": {
        "fn": "loaftesttubebaby.rle",
        "w": 12,
        "h": 7
    },
    "Blinker (rotor)": {
        "fn": "blinkerrotor.rle",
        "w": 3,
        "h": 1
    },
    "4c/13 ladders": {
        "fn": "4c13ladders.rle",
        "w": 52,
        "h": 507
    },
    "ttetrominotlife": {
        "fn": "ttetrominotlife.rle",
        "w": 3,
        "h": 2
    },
    "davegreenesminstrelcollection": {
        "fn": "davegreenesminstrelcollection.rle",
        "w": 506,
        "h": 149
    },
    "Unix on 41P7.2": {
        "fn": "unixon41p7.2.rle",
        "w": 18,
        "h": 18
    },
    "34p13 synth": {
        "fn": "34p13_synth.rle",
        "w": 64,
        "h": 33
    },
    "2x2 oscillators": {
        "fn": "2x2oscillators.rle",
        "w": 277,
        "h": 175
    },
    "Snacker 2": {
        "fn": "snacker2.rle",
        "w": 34,
        "h": 11
    },
    "diehard1340": {
        "fn": "diehard1340.rle",
        "w": 31,
        "h": 31
    },
    "Hive maker": {
        "fn": "hivemaker.rle",
        "w": 17,
        "h": 14
    },
    "jasonsp33": {
        "fn": "jasonsp33.rle",
        "w": 30,
        "h": 24
    },
    "p246 glider shuttle": {
        "fn": "p246glidershuttle.rle",
        "w": 60,
        "h": 50
    },
    "Schick engine": {
        "fn": "schickengine.rle",
        "w": 20,
        "h": 11
    },
    "longshillelagh synth": {
        "fn": "longshillelagh_synth.rle",
        "w": 26,
        "h": 16
    },
    "Trans-barge with tail synth": {
        "fn": "transbargewithtail_synth.rle",
        "w": 106,
        "h": 133
    },
    "Mosquito 2": {
        "fn": "mosquito2.rle",
        "w": 1644,
        "h": 315
    },
    "wswminus3": {
        "fn": "wswminus3.rle",
        "w": 20,
        "h": 21
    },
    "p104gun": {
        "fn": "p104gun.rle",
        "w": 39,
        "h": 21
    },
    "integralwithverylonghook": {
        "fn": "integralwithverylonghook.rle",
        "w": 8,
        "h": 5
    },
    "124p37 synth": {
        "fn": "124p37_synth.rle",
        "w": 353,
        "h": 73
    },
    "134P39.1": {
        "fn": "134p39.1.rle",
        "w": 25,
        "h": 29
    },
    "cisfusewithtwotails synth": {
        "fn": "cisfusewithtwotails_synth.rle",
        "w": 36,
        "h": 16
    },
    "Century": {
        "fn": "century.rle",
        "w": 4,
        "h": 3
    },
    "Python siamese carrier": {
        "fn": "pythonsiamesecarrier.rle",
        "w": 4,
        "h": 8
    },
    "2eceater": {
        "fn": "2eceater.rle",
        "w": 83,
        "h": 86
    },
    "23p2 synth": {
        "fn": "23p2_synth.rle",
        "w": 104,
        "h": 34
    },
    "Trans-boat with nine": {
        "fn": "transboatwithnine.rle",
        "w": 6,
        "h": 6
    },
    "smallstablesplitter": {
        "fn": "smallstablesplitter.rle",
        "w": 56,
        "h": 44
    },
    "honeybithwsseater": {
        "fn": "honeybithwsseater.rle",
        "w": 25,
        "h": 9
    },
    "Cis-boat and dock synth": {
        "fn": "cisboatanddock_synth.rle",
        "w": 115,
        "h": 101
    },
    "long5ship": {
        "fn": "long5ship.rle",
        "w": 8,
        "h": 8
    },
    "Beehive at loaf": {
        "fn": "beehiveatloaf.rle",
        "w": 6,
        "h": 7
    },
    "Test tube baby": {
        "fn": "testtubebaby.rle",
        "w": 8,
        "h": 5
    },
    "Long ship synth": {
        "fn": "longship_synth.rle",
        "w": 144,
        "h": 60
    },
    "p42 glider shuttle": {
        "fn": "p42glidershuttle.rle",
        "w": 32,
        "h": 32
    },
    "Bipole bridge pseudo-barberpole synth": {
        "fn": "bipolebridgepseudobarberpole_synth.rle",
        "w": 152,
        "h": 196
    },
    "Cis-barge with tail synth": {
        "fn": "cisbargewithtail_synth.rle",
        "w": 77,
        "h": 52
    },
    "R190": {
        "fn": "r190.rle",
        "w": 37,
        "h": 38
    },
    "blockondock synth": {
        "fn": "blockondock_synth.rle",
        "w": 209,
        "h": 179
    },
    "Period-59 glider gun": {
        "fn": "period59gun.rle",
        "w": 4091,
        "h": 3082
    },
    "117P18": {
        "fn": "117p18.rle",
        "w": 39,
        "h": 30
    },
    "2x2 still lifes": {
        "fn": "2x2stills.rle",
        "w": 38,
        "h": 12
    },
    "38p111 synth": {
        "fn": "38p111_synth.rle",
        "w": 663,
        "h": 204
    },
    "turners": {
        "fn": "turners.rle",
        "w": 56,
        "h": 39
    },
    "transboatlinetub": {
        "fn": "transboatlinetub.rle",
        "w": 7,
        "h": 7
    },
    "Chicken wire": {
        "fn": "chickenwire.rle",
        "w": 36,
        "h": 40
    },
    "1 beacon": {
        "fn": "1beacon.rle",
        "w": 7,
        "h": 7
    },
    "By flops": {
        "fn": "byflops.rle",
        "w": 6,
        "h": 7
    },
    "7-engine Cordership": {
        "fn": "7enginecordership.rle",
        "w": 71,
        "h": 65
    },
    "Thumb 2": {
        "fn": "thumb2.rle",
        "w": 11,
        "h": 11
    },
    "Gabriel's p138": {
        "fn": "gabrielsp138.rle",
        "w": 21,
        "h": 21
    },
    "prepreblockmakelwss": {
        "fn": "prepreblockmakelwss.rle",
        "w": 23,
        "h": 20
    },
    "7-in-a-row Cordership": {
        "fn": "7inarowcordership.rle",
        "w": 114,
        "h": 113
    },
    "Feather": {
        "fn": "feather.rle",
        "w": 3,
        "h": 4
    },
    "Tumbling T-tetson": {
        "fn": "tumblingttetson.rle",
        "w": 21,
        "h": 10
    },
    "Cuphook synth": {
        "fn": "cuphook_synth.rle",
        "w": 120,
        "h": 77
    },
    "extrovert": {
        "fn": "extrovert.rle",
        "w": 17,
        "h": 20
    },
    "T-nosed p5": {
        "fn": "tnosedp5.rle",
        "w": 41,
        "h": 17
    },
    "htomwss": {
        "fn": "htomwss.rle",
        "w": 61,
        "h": 54
    },
    "Octapole": {
        "fn": "octapole.rle",
        "w": 11,
        "h": 11
    },
    "symmetricalsynapsepredecessor": {
        "fn": "symmetricalsynapsepredecessor.rle",
        "w": 29,
        "h": 7
    },
    "bx106": {
        "fn": "bx106.rle",
        "w": 90,
        "h": 58
    },
    "twoblockershasslingrpentomino synth": {
        "fn": "twoblockershasslingrpentomino_synth.rle",
        "w": 180,
        "h": 150
    },
    "Blinker puffer 1 synth": {
        "fn": "blinkerpuffer1_synth.rle",
        "w": 79,
        "h": 53
    },
    "Achim's p4": {
        "fn": "achimsp4.rle",
        "w": 11,
        "h": 9
    },
    "p4bumper": {
        "fn": "p4bumper.rle",
        "w": 60,
        "h": 53
    },
    "F116": {
        "fn": "f116.rle",
        "w": 35,
        "h": 28
    },
    "P124 lumps of muck hassler": {
        "fn": "p124lumpsofmuckhassler.rle",
        "w": 39,
        "h": 38
    },
    "prepulsarshuttle44": {
        "fn": "prepulsarshuttle44.rle",
        "w": 36,
        "h": 29
    },
    "frenchkiss synth": {
        "fn": "frenchkiss_synth.rle",
        "w": 57,
        "h": 46
    },
    "long4hookwithtail": {
        "fn": "long4hookwithtail.rle",
        "w": 6,
        "h": 9
    },
    "fx70periodquadrupler": {
        "fn": "fx70periodquadrupler.rle",
        "w": 60,
        "h": 35
    },
    "53p13 synth": {
        "fn": "53p13_synth.rle",
        "w": 1411,
        "h": 59
    },
    "carrierbridgecarrier synth": {
        "fn": "carrierbridgecarrier_synth.rle",
        "w": 132,
        "h": 117
    },
    "Mosquito 1b": {
        "fn": "mosquito1b.rle",
        "w": 1794,
        "h": 411
    },
    "P22 lumps of muck hassler": {
        "fn": "p22lumpsofmuckhassler.rle",
        "w": 49,
        "h": 31
    },
    "Z-hexomino": {
        "fn": "zhexomino.rle",
        "w": 3,
        "h": 4
    },
    "p40bheptominoshuttle synth": {
        "fn": "p40bheptominoshuttle_synth.rle",
        "w": 169,
        "h": 47
    },
    "tubwithtwodowncistails": {
        "fn": "tubwithtwodowncistails.rle",
        "w": 7,
        "h": 5
    },
    "Pedestrian Life period 106 glider gun": {
        "fn": "pedestrianlife_p106gun.rle",
        "w": 8,
        "h": 6
    },
    "blockkeeper1": {
        "fn": "blockkeeper1.rle",
        "w": 47,
        "h": 36
    },
    "Crane": {
        "fn": "crane.rle",
        "w": 27,
        "h": 19
    },
    "hookwithtailwithcape": {
        "fn": "hookwithtailwithcape.rle",
        "w": 7,
        "h": 4
    },
    "eaterheadsiameselongsnake": {
        "fn": "eaterheadsiameselongsnake.rle",
        "w": 5,
        "h": 8
    },
    "p230 glider shuttle": {
        "fn": "p230glidershuttle.rle",
        "w": 62,
        "h": 52
    },
    "Rotated C": {
        "fn": "rotatedc.rle",
        "w": 7,
        "h": 7
    },
    "p54 shuttle": {
        "fn": "p54shuttle.rle",
        "w": 29,
        "h": 17
    },
    "Small still lifes": {
        "fn": "smallstilllifes.rle",
        "w": 273,
        "h": 93
    },
    "Beehive and table": {
        "fn": "beehiveandtable.rle",
        "w": 5,
        "h": 6
    },
    "Figure eight as reflector": {
        "fn": "figureeightasreflector.rle",
        "w": 14,
        "h": 17
    },
    "Boat on quadpole": {
        "fn": "boatonquadpole.rle",
        "w": 10,
        "h": 10
    },
    "p57bumperloop": {
        "fn": "p57bumperloop.rle",
        "w": 44,
        "h": 44
    },
    "4-8-12 diamond": {
        "fn": "4812diamond.rle",
        "w": 12,
        "h": 9
    },
    "Odd keys": {
        "fn": "oddkeys.rle",
        "w": 12,
        "h": 5
    },
    "twelveloop synth": {
        "fn": "twelveloop_synth.rle",
        "w": 36,
        "h": 20
    },
    "Iwona": {
        "fn": "iwona.rle",
        "w": 20,
        "h": 21
    },
    "Eater 1 with blinker": {
        "fn": "eater1withblinker.rle",
        "w": 6,
        "h": 7
    },
    "period44glidergun": {
        "fn": "period44glidergun.rle",
        "w": 44,
        "h": 41
    },
    "Replicator-based period 96 oscillator in HighLife (B36/S23)": {
        "fn": "highlifereplicatorxp96.rle",
        "w": 35,
        "h": 19
    },
    "Eater 3": {
        "fn": "eater3.rle",
        "w": 12,
        "h": 12
    },
    "hookwithtwotails": {
        "fn": "hookwithtwotails.rle",
        "w": 6,
        "h": 6
    },
    "quasar2": {
        "fn": "quasar2.rle",
        "w": 45,
        "h": 45
    },
    "Period 3 oscillators": {
        "fn": "period3oscillators.rle",
        "w": 349,
        "h": 180
    },
    "sqrtgun": {
        "fn": "sqrtgun.rle",
        "w": 226,
        "h": 187
    },
    "ghostherschel": {
        "fn": "ghostherschel.rle",
        "w": 3,
        "h": 4
    },
    "p15bouncer": {
        "fn": "p15bouncer.rle",
        "w": 55,
        "h": 52
    },
    "Gosper glider gun (glider destruction)": {
        "fn": "gosperglidergungliderdestruction.rle",
        "w": 47,
        "h": 26
    },
    "Beehive and dock": {
        "fn": "beehiveanddock.rle",
        "w": 6,
        "h": 7
    },
    "Pedestrian Life period 190 oblique spaceship": {
        "fn": "pedestrianlife_obliquespaceship.rle",
        "w": 40,
        "h": 28
    },
    "biloaf4 synth": {
        "fn": "biloaf4_synth.rle",
        "w": 10,
        "h": 10
    },
    "p5heavyweightvolcano2": {
        "fn": "p5heavyweightvolcano2.rle",
        "w": 22,
        "h": 15
    },
    "4gto5gr2": {
        "fn": "4gto5gr2.rle",
        "w": 65,
        "h": 49
    },
    "Two-glider mess synth": {
        "fn": "twoglidermess_synth.rle",
        "w": 27,
        "h": 8
    },
    "106p135 synth": {
        "fn": "106p135_synth.rle",
        "w": 308,
        "h": 72
    },
    "282P6H2V1": {
        "fn": "282p6h2v1.rle",
        "w": 31,
        "h": 79
    },
    "p35.1": {
        "fn": "p35beehivehassler.rle",
        "w": 79,
        "h": 24
    },
    "p11bumper": {
        "fn": "p11bumper.rle",
        "w": 83,
        "h": 77
    },
    "Mirrored dock synth": {
        "fn": "mirroreddock_synth.rle",
        "w": 67,
        "h": 19
    },
    "Pre-pulsar shuttle 26 variant": {
        "fn": "prepulsarshuttle26variant.rle",
        "w": 39,
        "h": 39
    },
    "Hustler II": {
        "fn": "hustlerii.rle",
        "w": 16,
        "h": 13
    },
    "longintegralwithboat": {
        "fn": "longintegralwithboat.rle",
        "w": 7,
        "h": 5
    },
    "long4canoe": {
        "fn": "long4canoe.rle",
        "w": 9,
        "h": 9
    },
    "Carnival shuttle": {
        "fn": "carnivalshuttle.rle",
        "w": 38,
        "h": 7
    },
    "goucherp487reflector": {
        "fn": "goucherp487reflector.rle",
        "w": 259,
        "h": 161
    },
    "Ship on quadpole synth": {
        "fn": "shiponquadpole_synth.rle",
        "w": 148,
        "h": 88
    },
    "Block synth": {
        "fn": "block_synth.rle",
        "w": 148,
        "h": 74
    },
    "30p6.1": {
        "fn": "30p6.1.rle",
        "w": 13,
        "h": 13
    },
    "Cow": {
        "fn": "cow.rle",
        "w": 40,
        "h": 7
    },
    "160P10H2V0": {
        "fn": "160p10h2v0.rle",
        "w": 19,
        "h": 37
    },
    "p52emu2": {
        "fn": "p52emu2.rle",
        "w": 365,
        "h": 365
    },
    "P50 traffic jam synth": {
        "fn": "p50trafficjam_synth.rle",
        "w": 293,
        "h": 129
    },
    "Snake pit 2 synth": {
        "fn": "snakepit2_synth.rle",
        "w": 36,
        "h": 21
    },
    "crosssurface": {
        "fn": "crosssurface.rle",
        "w": 100,
        "h": 100
    },
    "Diamond ring synth": {
        "fn": "diamondring_synth.rle",
        "w": 92,
        "h": 73
    },
    "Quad pseudo still life": {
        "fn": "quadpseudostilllife.rle",
        "w": 11,
        "h": 9
    },
    "2enginecordership synth": {
        "fn": "2enginecordership_synth.rle",
        "w": 175,
        "h": 167
    },
    "Beacon and two tails synth": {
        "fn": "beaconandtwotails_synth.rle",
        "w": 106,
        "h": 18
    },
    "Octagon 4": {
        "fn": "octagon4.rle",
        "w": 16,
        "h": 16
    },
    "Pedestle": {
        "fn": "pedestle.rle",
        "w": 11,
        "h": 17
    },
    "Glide symmetric PPS": {
        "fn": "glidesymmetricpps.rle",
        "w": 69,
        "h": 13
    },
    "thirteenloop synth": {
        "fn": "thirteenloop_synth.rle",
        "w": 31,
        "h": 18
    },
    "50P35": {
        "fn": "50p35.rle",
        "w": 33,
        "h": 21
    },
    "prequasar": {
        "fn": "prequasar.rle",
        "w": 121,
        "h": 108
    },
    "fusewithtailandverylongtail": {
        "fn": "fusewithtailandverylongtail.rle",
        "w": 8,
        "h": 5
    },
    "House": {
        "fn": "house.rle",
        "w": 5,
        "h": 3
    },
    "Eater 5 eating gliders": {
        "fn": "eater5eatinggliders.rle",
        "w": 13,
        "h": 14
    },
    "14p2.4": {
        "fn": "14p2.4.rle",
        "w": 8,
        "h": 7
    },
    "lightspeedbubble2": {
        "fn": "lightspeedbubble2.rle",
        "w": 61,
        "h": 49
    },
    "Ring of fire": {
        "fn": "ringoffire.rle",
        "w": 34,
        "h": 30
    },
    "Block and dock": {
        "fn": "blockanddock.rle",
        "w": 6,
        "h": 6
    },
    "pseudo50": {
        "fn": "pseudo50.rle",
        "w": 80,
        "h": 39
    },
    "28p7.3": {
        "fn": "28p7.3.rle",
        "w": 10,
        "h": 13
    },
    "fumaroleonrichsp16": {
        "fn": "fumaroleonrichsp16.rle",
        "w": 13,
        "h": 20
    },
    "50P35 synth": {
        "fn": "50p35_synth.rle",
        "w": 218,
        "h": 69
    },
    "114P6H1V0 pushalongs": {
        "fn": "114p6h1v0pushalong2.rle",
        "w": 63,
        "h": 60
    },
    "Protein": {
        "fn": "protein.rle",
        "w": 13,
        "h": 13
    },
    "X-pentomino": {
        "fn": "xpentomino.rle",
        "w": 3,
        "h": 3
    },
    "Tub test tube baby synth": {
        "fn": "tubtesttubebaby_synth.rle",
        "w": 138,
        "h": 40
    },
    "71P17.1": {
        "fn": "71p171.rle",
        "w": 18,
        "h": 14
    },
    "Long long canoe synth": {
        "fn": "longlongcanoe_synth.rle",
        "w": 110,
        "h": 78
    },
    "period92glidergun": {
        "fn": "period92glidergun.rle",
        "w": 46,
        "h": 25
    },
    "highclearanceeater1variant": {
        "fn": "highclearanceeater1variant.rle",
        "w": 16,
        "h": 13
    },
    "p130shuttle2": {
        "fn": "p130shuttle2.rle",
        "w": 50,
        "h": 40
    },
    "Fourteener": {
        "fn": "fourteener.rle",
        "w": 7,
        "h": 5
    },
    "31P8H4V0": {
        "fn": "31p8h4v0.rle",
        "w": 11,
        "h": 11
    },
    "Pentapole": {
        "fn": "pentapole.rle",
        "w": 8,
        "h": 8
    },
    "Pre-pulsar shuttle 47 v2": {
        "fn": "prepulsarshuttle47v2.rle",
        "w": 79,
        "h": 30
    },
    "pt9b": {
        "fn": "pt9b.rle",
        "w": 11,
        "h": 10
    },
    "fusewithtailandintegral": {
        "fn": "fusewithtailandintegral.rle",
        "w": 7,
        "h": 7
    },
    "long3integral": {
        "fn": "long3integral.rle",
        "w": 4,
        "h": 8
    },
    "Caterer on 44P7.2": {
        "fn": "catereron44p7.2.rle",
        "w": 19,
        "h": 16
    },
    "Teeth": {
        "fn": "teeth.rle",
        "w": 472,
        "h": 127
    },
    "Honey farm synth": {
        "fn": "honeyfarm_synth.rle",
        "w": 152,
        "h": 73
    },
    "Block and two tails": {
        "fn": "blockandtwotails.rle",
        "w": 5,
        "h": 5
    },
    "blinkersbitpole synth": {
        "fn": "blinkersbitpole_synth.rle",
        "w": 257,
        "h": 30
    },
    "6 bits": {
        "fn": "6bits.rle",
        "w": 40,
        "h": 23
    },
    "Blinker fuse 2": {
        "fn": "blinkerfuse2.rle",
        "w": 62,
        "h": 5
    },
    "filterstream": {
        "fn": "filterstream.rle",
        "w": 62,
        "h": 34
    },
    "6-engine Cordership": {
        "fn": "6enginecordership.rle",
        "w": 66,
        "h": 61
    },
    "Butterfly": {
        "fn": "butterfly.rle",
        "w": 4,
        "h": 4
    },
    "Edge repair Herschel": {
        "fn": "edgerepairherschel.rle",
        "w": 24,
        "h": 7
    },
    "Silver's p5 synth": {
        "fn": "silversp5_synth.rle",
        "w": 74,
        "h": 111
    },
    "Ortho-loaf and table synth": {
        "fn": "ortholoafandtable_synth.rle",
        "w": 108,
        "h": 73
    },
    "Beehive with tail": {
        "fn": "beehivewithtail.rle",
        "w": 6,
        "h": 5
    },
    "Puffer 2 synth": {
        "fn": "puffer2_synth.rle",
        "w": 105,
        "h": 117
    },
    "inverteddoubleclaw": {
        "fn": "inverteddoubleclaw.rle",
        "w": 6,
        "h": 6
    },
    "tumbler synth": {
        "fn": "tumbler_synth.rle",
        "w": 75,
        "h": 23
    },
    "carriersiamesetubwithtail": {
        "fn": "carriersiamesetubwithtail.rle",
        "w": 8,
        "h": 5
    },
    "Cis-hook with tail synth": {
        "fn": "cishookwithtail_synth.rle",
        "w": 146,
        "h": 110
    },
    "B-heptomino synth": {
        "fn": "bheptomino_synth.rle",
        "w": 147,
        "h": 79
    },
    "Edge-repair spaceship 1 deleting various objects and surviving.": {
        "fn": "edgerepairspaceshipsandobjects.rle",
        "w": 113,
        "h": 11
    },
    "Mold on 48P31": {
        "fn": "moldon48p31.rle",
        "w": 24,
        "h": 24
    },
    "Loading dock": {
        "fn": "loadingdock.rle",
        "w": 9,
        "h": 8
    },
    "Move puffer": {
        "fn": "movepuffer.rle",
        "w": 2,
        "h": 6
    },
    "37P4H1V0": {
        "fn": "37p4h1v0.rle",
        "w": 19,
        "h": 12
    },
    "tubwithtwouptranstails": {
        "fn": "tubwithtwouptranstails.rle",
        "w": 7,
        "h": 7
    },
    "Why not": {
        "fn": "whynot.rle",
        "w": 7,
        "h": 7
    },
    "Max": {
        "fn": "max.rle",
        "w": 27,
        "h": 27
    },
    "professorisomers": {
        "fn": "professorisomers.rle",
        "w": 72,
        "h": 14
    },
    "34P13": {
        "fn": "34p13.rle",
        "w": 16,
        "h": 16
    },
    "Pre-beehive synth": {
        "fn": "prebeehive_synth.rle",
        "w": 144,
        "h": 13
    },
    "p960 2c5gun": {
        "fn": "p960_2c5gun.rle",
        "w": 1510,
        "h": 1441
    },
    "Boss": {
        "fn": "boss.rle",
        "w": 11,
        "h": 14
    },
    "2x2 4-cell still lifes": {
        "fn": "2x24cellstilllifes.rle",
        "w": 14,
        "h": 4
    },
    "hf95p": {
        "fn": "hf95p.rle",
        "w": 22,
        "h": 26
    },
    "Jason's bow": {
        "fn": "jasonsbow.rle",
        "w": 92,
        "h": 42
    },
    "3c/14 pi wave": {
        "fn": "3c14piwave.rle",
        "w": 39,
        "h": 39
    },
    "Move still lifes": {
        "fn": "movestilllifes.rle",
        "w": 23,
        "h": 39
    },
    "Traffic light synth": {
        "fn": "trafficlight_synth.rle",
        "w": 109,
        "h": 29
    },
    "Elevener synth": {
        "fn": "elevener_synth.rle",
        "w": 64,
        "h": 40
    },
    "16chacha": {
        "fn": "chacha_synth.rle",
        "w": 129,
        "h": 46
    },
    "p6thumb": {
        "fn": "p6thumb.rle",
        "w": 9,
        "h": 9
    },
    "loafsiameseloaf synth": {
        "fn": "loafsiameseloaf_synth.rle",
        "w": 44,
        "h": 19
    },
    "AK-94 synthesis": {
        "fn": "ak94_synth.rle",
        "w": 712,
        "h": 68
    },
    "carriersiameseeaterhead": {
        "fn": "carriersiameseeaterhead.rle",
        "w": 7,
        "h": 5
    },
    "24827m": {
        "fn": "24827m.rle",
        "w": 8,
        "h": 7
    },
    "Period-59 glider gun (original)": {
        "fn": "p59glidergunoriginal.rle",
        "w": 14348,
        "h": 12790
    },
    "tableandtable": {
        "fn": "tableandtable.rle",
        "w": 4,
        "h": 5
    },
    "Cis-rotated hook": {
        "fn": "cisrotatedhook.rle",
        "w": 7,
        "h": 5
    },
    "glidertrain": {
        "fn": "glidertrain.rle",
        "w": 68,
        "h": 33
    },
    "ortholoafontable synth": {
        "fn": "ortholoafontable_synth.rle",
        "w": 100,
        "h": 15
    },
    "long9boat": {
        "fn": "long9boat.rle",
        "w": 12,
        "h": 12
    },
    "2enginecordershipslowsalvo": {
        "fn": "2enginecordershipslowsalvo.rle",
        "w": 761,
        "h": 777
    },
    "Mosquito 5": {
        "fn": "mosquito5.rle",
        "w": 2754,
        "h": 650
    },
    "Beehive at beehive": {
        "fn": "beehiveatbeehive.rle",
        "w": 6,
        "h": 6
    },
    "fourskewedblocks synth": {
        "fn": "fourskewedblocks_synth.rle",
        "w": 29,
        "h": 10
    },
    "p29trafficfarmhassler": {
        "fn": "p29trafficfarmhassler.rle",
        "w": 43,
        "h": 21
    },
    "HighLife 6-cell still lifes": {
        "fn": "highlife6cellstilllifes.rle",
        "w": 25,
        "h": 4
    },
    "eater2 synth": {
        "fn": "eater2_synth.rle",
        "w": 72,
        "h": 18
    },
    "linecrosser": {
        "fn": "linecrosser.rle",
        "w": 1102,
        "h": 1393
    },
    "Pre-pulsar shuttle 28": {
        "fn": "prepulsarshuttle28.rle",
        "w": 22,
        "h": 16
    },
    "Infinite LWSS hotel": {
        "fn": "infinitelwsshotel.rle",
        "w": 515,
        "h": 298
    },
    "shiponsnake": {
        "fn": "shiponsnake.rle",
        "w": 7,
        "h": 5
    },
    "Octapole synth": {
        "fn": "octapole_synth.rle",
        "w": 55,
        "h": 33
    },
    "long6ship": {
        "fn": "long6ship.rle",
        "w": 9,
        "h": 9
    },
    "T-pentomino": {
        "fn": "tpentomino.rle",
        "w": 3,
        "h": 3
    },
    "Ship": {
        "fn": "ship.rle",
        "w": 3,
        "h": 3
    },
    "linecuttingreaction": {
        "fn": "linecuttingreaction.rle",
        "w": 53,
        "h": 53
    },
    "smallp120hwssgun": {
        "fn": "smallp120hwssgun.rle",
        "w": 72,
        "h": 56
    },
    "nw31periodquadrupler": {
        "fn": "nw31periodquadrupler.rle",
        "w": 44,
        "h": 29
    },
    "Eater tail siamese snake": {
        "fn": "eatertailsiamesesnake.rle",
        "w": 4,
        "h": 7
    },
    "prepreblock": {
        "fn": "prepreblock.rle",
        "w": 2,
        "h": 3
    },
    "Pincers synth": {
        "fn": "pincers_synth.rle",
        "w": 80,
        "h": 98
    },
    "trafficlightextrudereaction": {
        "fn": "trafficlightextrudereaction.rle",
        "w": 51,
        "h": 9
    },
    "Cis-beacon up and long hook": {
        "fn": "cisbeaconupandlonghook.rle",
        "w": 5,
        "h": 8
    },
    "Halfmax": {
        "fn": "halfmax.rle",
        "w": 65,
        "h": 80
    },
    "Scrubber synth": {
        "fn": "scrubber_synth.rle",
        "w": 50,
        "h": 35
    },
    "verylongcisshillelagh": {
        "fn": "verylongcisshillelagh.rle",
        "w": 7,
        "h": 8
    },
    "gun132": {
        "fn": "gun132.rle",
        "w": 303,
        "h": 185
    },
    "Middleweight spaceship": {
        "fn": "mwss.rle",
        "w": 6,
        "h": 5
    },
    "Boring p24": {
        "fn": "boringp24.rle",
        "w": 26,
        "h": 18
    },
    "P57 Herschel loop 1": {
        "fn": "p57herschelloop1.rle",
        "w": 334,
        "h": 334
    },
    "symmetricscorpion synth": {
        "fn": "symmetricscorpion_synth.rle",
        "w": 30,
        "h": 11
    },
    "Hat synth": {
        "fn": "hat_synth.rle",
        "w": 109,
        "h": 18
    },
    "Nonapole": {
        "fn": "nonapole.rle",
        "w": 12,
        "h": 12
    },
    "Middleweight spaceship synth": {
        "fn": "mwss_synth.rle",
        "w": 156,
        "h": 105
    },
    "mold on 36P22": {
        "fn": "moldon36p22.rle",
        "w": 27,
        "h": 16
    },
    "Trans-mirrored R-bee": {
        "fn": "transmirroredrbee.rle",
        "w": 7,
        "h": 5
    },
    "Smiley": {
        "fn": "smiley.rle",
        "w": 7,
        "h": 7
    },
    "gtolwss": {
        "fn": "gtolwss.rle",
        "w": 168,
        "h": 75
    },
    "P58 toadsucker": {
        "fn": "p58toadsucker.rle",
        "w": 67,
        "h": 56
    },
    "Semi-snark": {
        "fn": "semisnark.rle",
        "w": 19,
        "h": 19
    },
    "Tub synth": {
        "fn": "tub_synth.rle",
        "w": 147,
        "h": 74
    },
    "Pufferfish with a companion": {
        "fn": "pufferfishcompanion.rle",
        "w": 18,
        "h": 27
    },
    "60p312": {
        "fn": "60p312.rle",
        "w": 42,
        "h": 42
    },
    "60P3H1V0.3": {
        "fn": "60p3h1v0.3.rle",
        "w": 33,
        "h": 8
    },
    "44p12.3": {
        "fn": "44p12.3.rle",
        "w": 14,
        "h": 14
    },
    "4c/9 ladders": {
        "fn": "4c9ladders.rle",
        "w": 20,
        "h": 258
    },
    "Hustler": {
        "fn": "hustler.rle",
        "w": 11,
        "h": 12
    },
    "p88 pi-heptomino hassler": {
        "fn": "p88piheptominohassler.rle",
        "w": 43,
        "h": 44
    },
    "Achimsp11 synth": {
        "fn": "achimsp11_synth.rle",
        "w": 195,
        "h": 218
    },
    "Beehive and long hook eating tub": {
        "fn": "beehiveandlonghookeatingtub.rle",
        "w": 11,
        "h": 5
    },
    "Grandfather problem": {
        "fn": "grandfatherproblemsolved.rle",
        "w": 24,
        "h": 23
    },
    "Heptapole synth": {
        "fn": "heptapole_synth.rle",
        "w": 119,
        "h": 53
    },
    "Racetrack": {
        "fn": "racetrack.rle",
        "w": 6,
        "h": 4
    },
    "Loaf synth": {
        "fn": "loaf_synth.rle",
        "w": 148,
        "h": 111
    },
    "Ship on bipole": {
        "fn": "shiponbipole.rle",
        "w": 8,
        "h": 8
    },
    "P416 60P5H2V0 gun": {
        "fn": "p41660p5h2v0gun.rle",
        "w": 990,
        "h": 979
    },
    "Toad sucker": {
        "fn": "toadsucker.rle",
        "w": 18,
        "h": 11
    },
    "Quindecapole": {
        "fn": "quindecapole.rle",
        "w": 18,
        "h": 18
    },
    "prepulsarshuttle22": {
        "fn": "prepulsarshuttle22.rle",
        "w": 24,
        "h": 19
    },
    "Beehive and cap": {
        "fn": "beehiveandcap.rle",
        "w": 5,
        "h": 7
    },
    "Bunnies": {
        "fn": "bunnies.rle",
        "w": 8,
        "h": 4
    },
    "krake synth": {
        "fn": "krake_synth.rle",
        "w": 32,
        "h": 14
    },
    "carriertieboat": {
        "fn": "carriertieboat.rle",
        "w": 6,
        "h": 7
    },
    "hbkreaction": {
        "fn": "hbkreaction.rle",
        "w": 59,
        "h": 53
    },
    "45 hivenudgers": {
        "fn": "45hivenudgers.rle",
        "w": 155,
        "h": 116
    },
    "New gun 1": {
        "fn": "newgun1.rle",
        "w": 34,
        "h": 34
    },
    "pole2rotor": {
        "fn": "pole2rotor.rle",
        "w": 3,
        "h": 1
    },
    "1xN quadratic growth": {
        "fn": "1xnquadraticgrowth.rle",
        "w": 1013783,
        "h": 1
    },
    "B-52 bomber synthesis": {
        "fn": "b52bomber_synth.rle",
        "w": 525,
        "h": 40
    },
    "Jack synth": {
        "fn": "jack_synth.rle",
        "w": 114,
        "h": 106
    },
    "P144 Hans Leo hassler": {
        "fn": "p144hansleohassler.rle",
        "w": 33,
        "h": 40
    },
    "Bx222": {
        "fn": "bx222.rle",
        "w": 42,
        "h": 36
    },
    "A for all synth": {
        "fn": "aforall_synth.rle",
        "w": 98,
        "h": 97
    },
    "60P3H1V0.3 interactions": {
        "fn": "60p3h1v03reactions.rle",
        "w": 185,
        "h": 43
    },
    "c/9 reaction": {
        "fn": "c9reaction.rle",
        "w": 9,
        "h": 4
    },
    "Snake bridge snake": {
        "fn": "snakebridgesnake.rle",
        "w": 6,
        "h": 6
    },
    "33p3.1": {
        "fn": "33p3.1.rle",
        "w": 11,
        "h": 12
    },
    "Sidecar gun": {
        "fn": "sidecargun.rle",
        "w": 213,
        "h": 142
    },
    "Unidimensional infinite growth": {
        "fn": "unidimensionalinfinitegrowth.rle",
        "w": 39,
        "h": 1
    },
    "56et4be4": {
        "fn": "foureatershasslingfourbookends_synth.rle",
        "w": 297,
        "h": 283
    },
    "Pre-block": {
        "fn": "preblock.rle",
        "w": 2,
        "h": 2
    },
    "Boat maker": {
        "fn": "boatmaker.rle",
        "w": 18,
        "h": 17
    },
    "Vase siamese hat": {
        "fn": "vasesiamesehat.rle",
        "w": 9,
        "h": 9
    },
    "p35honeyfarmhassler synth": {
        "fn": "p35honeyfarmhassler_synth.rle",
        "w": 128,
        "h": 43
    },
    "p4colourpreservingcenark": {
        "fn": "p4colourpreservingcenark.rle",
        "w": 124,
        "h": 97
    },
    "2x2 5-cell still lifes": {
        "fn": "2x25cellstilllifes.rle",
        "w": 22,
        "h": 5
    },
    "68p321 synth": {
        "fn": "68p321_synth.rle",
        "w": 103,
        "h": 79
    },
    "p37 reflections": {
        "fn": "p37reflections.rle",
        "w": 271,
        "h": 47
    },
    "boatclawwithtail": {
        "fn": "boatclawwithtail.rle",
        "w": 6,
        "h": 7
    },
    "catereron36p22 synth": {
        "fn": "catereron36p22_synth.rle",
        "w": 153,
        "h": 70
    },
    "24p2 synth": {
        "fn": "24p2_synth.rle",
        "w": 161,
        "h": 29
    },
    "bargewithverylongtail": {
        "fn": "bargewithverylongtail.rle",
        "w": 8,
        "h": 5
    },
    "carriertiebipole": {
        "fn": "carriertiebipole.rle",
        "w": 9,
        "h": 8
    },
    "Pentadecathlon on thumb 1": {
        "fn": "pentadecathlononthumb1.rle",
        "w": 14,
        "h": 17
    },
    "Block on table synth": {
        "fn": "blockontable_synth.rle",
        "w": 109,
        "h": 51
    },
    "4-engine Cordership v2": {
        "fn": "4enginecordershipb.rle",
        "w": 77,
        "h": 74
    },
    "Bun": {
        "fn": "bun.rle",
        "w": 4,
        "h": 3
    },
    "transrotatedrbee synth": {
        "fn": "transrotatedrbee_synth.rle",
        "w": 27,
        "h": 17
    },
    "37P10.1": {
        "fn": "37p10.1.rle",
        "w": 13,
        "h": 10
    },
    "Bi-loaf 1": {
        "fn": "biloaf1.rle",
        "w": 7,
        "h": 7
    },
    "P36 toad hassler": {
        "fn": "p36toadhassler.rle",
        "w": 20,
        "h": 26
    },
    "Bunnies 11": {
        "fn": "bunnies11.rle",
        "w": 6,
        "h": 4
    },
    "Pi eater": {
        "fn": "pieater.rle",
        "w": 9,
        "h": 12
    },
    "Dart": {
        "fn": "dart.rle",
        "w": 15,
        "h": 10
    },
    "L156": {
        "fn": "l156.rle",
        "w": 29,
        "h": 47
    },
    "long8ship": {
        "fn": "long8ship.rle",
        "w": 11,
        "h": 11
    },
    "30p5h2v0eater2": {
        "fn": "30p5h2v0eater2.rle",
        "w": 16,
        "h": 52
    },
    "Coolout Conjecture": {
        "fn": "cooloutconjecture.rle",
        "w": 6,
        "h": 2
    },
    "Pentoad": {
        "fn": "pentoad.rle",
        "w": 13,
        "h": 12
    },
    "blockersharingblocks": {
        "fn": "blockersharingblocks.rle",
        "w": 18,
        "h": 12
    },
    "Century eater": {
        "fn": "centuryeater.rle",
        "w": 21,
        "h": 21
    },
    "Eater/block frob": {
        "fn": "eaterblockfrob.rle",
        "w": 10,
        "h": 10
    },
    "33p3.1eatingss": {
        "fn": "33p3.1eatingss.rle",
        "w": 34,
        "h": 45
    },
    "Beehive at beehive synth": {
        "fn": "beehiveatbeehive_synth.rle",
        "w": 156,
        "h": 110
    },
    "Maze 9-cell still lifes": {
        "fn": "maze9cellstilllifes.rle",
        "w": 281,
        "h": 169
    },
    "carrierbridgesnake": {
        "fn": "carrierbridgesnake.rle",
        "w": 6,
        "h": 6
    },
    "49P88": {
        "fn": "49p88.rle",
        "w": 27,
        "h": 25
    },
    "Test tube baby synth": {
        "fn": "testtubebaby_synth.rle",
        "w": 140,
        "h": 92
    },
    "cisboatondock synth": {
        "fn": "cisboatondock_synth.rle",
        "w": 249,
        "h": 151
    },
    "Worm": {
        "fn": "worm.rle",
        "w": 6,
        "h": 4
    },
    "Integral with hook synth": {
        "fn": "integralwithhook_synth.rle",
        "w": 145,
        "h": 103
    },
    "Omnibus with tubs": {
        "fn": "omnibuswithtubs.rle",
        "w": 9,
        "h": 11
    },
    "pt38p": {
        "fn": "pt38p.rle",
        "w": 26,
        "h": 8
    },
    "BRx46B": {
        "fn": "brx46b.rle",
        "w": 13,
        "h": 10
    },
    "C/4 wave": {
        "fn": "c4wave.rle",
        "w": 120,
        "h": 45
    },
    "Loaf siamese loaf": {
        "fn": "loafsiameseloaf.rle",
        "w": 5,
        "h": 5
    },
    "Trans-boat and table": {
        "fn": "transboatandtable.rle",
        "w": 6,
        "h": 5
    },
    "boattieeatertail": {
        "fn": "boattieeatertail.rle",
        "w": 5,
        "h": 7
    },
    "long10boat": {
        "fn": "long10boat.rle",
        "w": 13,
        "h": 13
    },
    "bargewithlongtail synth": {
        "fn": "bargewithlongtail_synth.rle",
        "w": 57,
        "h": 16
    },
    "pufferfishspaceship synth": {
        "fn": "pufferfishspaceship_synth.rle",
        "w": 456,
        "h": 499
    },
    "Long long barge synth": {
        "fn": "longlongbarge_synth.rle",
        "w": 71,
        "h": 10
    },
    "Pi orbital": {
        "fn": "piorbital.rle",
        "w": 59,
        "h": 59
    },
    "92P33.1": {
        "fn": "92p331.rle",
        "w": 34,
        "h": 30
    },
    "Circle of fire": {
        "fn": "circleoffire.rle",
        "w": 11,
        "h": 11
    },
    "Pentadecathlon on 37P7.1": {
        "fn": "pentadecathlonon37p7.1.rle",
        "w": 17,
        "h": 16
    },
    "Eve": {
        "fn": "eve.rle",
        "w": 16,
        "h": 10
    },
    "Pi ship 1": {
        "fn": "piship1.rle",
        "w": 99,
        "h": 29
    },
    "OTCA metapixel (off)": {
        "fn": "otcametapixeloff.rle",
        "w": 2058,
        "h": 2058
    },
    "Edge-repair spaceship 1": {
        "fn": "edgerepairspaceship1.rle",
        "w": 16,
        "h": 7
    },
    "Boat-bit": {
        "fn": "boatbit.rle",
        "w": 25,
        "h": 24
    },
    "pt8p": {
        "fn": "pt8p.rle",
        "w": 10,
        "h": 10
    },
    "Up dove on dove": {
        "fn": "updoveondove.rle",
        "w": 9,
        "h": 5
    },
    "Cyclic": {
        "fn": "cyclic.rle",
        "w": 10,
        "h": 10
    },
    "Figure eight on pentadecathlon": {
        "fn": "figureeightonpentadecathlon.rle",
        "w": 14,
        "h": 10
    },
    "Blinker fuse": {
        "fn": "blinkerfuse.rle",
        "w": 25,
        "h": 5
    },
    "duelingbanjostetramer": {
        "fn": "duelingbanjostetramer.rle",
        "w": 39,
        "h": 39
    },
    "Queen bee shuttle synth": {
        "fn": "queenbeeshuttle_synth.rle",
        "w": 157,
        "h": 98
    },
    "ortholoafontable": {
        "fn": "ortholoafontable.rle",
        "w": 7,
        "h": 5
    },
    "x66": {
        "fn": "x66.rle",
        "w": 9,
        "h": 11
    },
    "Trans-loaf with tail synth": {
        "fn": "transloafwithtail_synth.rle",
        "w": 67,
        "h": 39
    },
    "downsnakeontable": {
        "fn": "downsnakeontable.rle",
        "w": 7,
        "h": 4
    },
    "Bi-gun": {
        "fn": "bigun.rle",
        "w": 50,
        "h": 15
    },
    "Jagged lines": {
        "fn": "jaggedlines.rle",
        "w": 125,
        "h": 67
    },
    "Bi-cap": {
        "fn": "bicap.rle",
        "w": 4,
        "h": 7
    },
    "Trans-beacon and dock": {
        "fn": "transbeaconanddock.rle",
        "w": 7,
        "h": 8
    },
    "Bee hat": {
        "fn": "beehat.rle",
        "w": 6,
        "h": 6
    },
    "BFx59Hinjector": {
        "fn": "bfx59hinjector.rle",
        "w": 48,
        "h": 62
    },
    "Diuresis": {
        "fn": "diuresis.rle",
        "w": 29,
        "h": 25
    },
    "77p77": {
        "fn": "77p77.rle",
        "w": 16,
        "h": 21
    },
    "verylongsnake": {
        "fn": "verylongsnake.rle",
        "w": 6,
        "h": 4
    },
    "Loaf tractor beam": {
        "fn": "loaftractorbeam.rle",
        "w": 47,
        "h": 8
    },
    "Elkies' p5": {
        "fn": "elkiesp5.rle",
        "w": 9,
        "h": 8
    },
    "pentoad1h2 synth": {
        "fn": "pentoad1h2_synth.rle",
        "w": 107,
        "h": 63
    },
    "Up wing on wing": {
        "fn": "upwingonwing.rle",
        "w": 9,
        "h": 4
    },
    "Long snake synth": {
        "fn": "longsnake_synth.rle",
        "w": 68,
        "h": 11
    },
    "shortkeys synth": {
        "fn": "shortkeys_synth.rle",
        "w": 37,
        "h": 8
    },
    "Cis-boat with nine": {
        "fn": "cisboatwithnine.rle",
        "w": 6,
        "h": 6
    },
    "Snark": {
        "fn": "snark.rle",
        "w": 17,
        "h": 23
    },
    "Alternate PD on snacker": {
        "fn": "alternatepentadecathlononsnacker.rle",
        "w": 20,
        "h": 15
    },
    "Boojum reflector": {
        "fn": "boojumreflector.rle",
        "w": 44,
        "h": 32
    },
    "Unix synth": {
        "fn": "unix_synth.rle",
        "w": 121,
        "h": 26
    },
    "cishookandrbee synth": {
        "fn": "cishookandrbee_synth.rle",
        "w": 29,
        "h": 12
    },
    "Unidimensional nothing": {
        "fn": "unidimensionalnothing.rle",
        "w": 149,
        "h": 1
    },
    "p69 glider loop": {
        "fn": "p69gliderloop.rle",
        "w": 91,
        "h": 91
    },
    "Moose antlers synth": {
        "fn": "mooseantlers_synth.rle",
        "w": 142,
        "h": 114
    },
    "snarkmaker": {
        "fn": "snarkmaker.rle",
        "w": 65798,
        "h": 65794
    },
    "Integral with tub": {
        "fn": "integralwithtub.rle",
        "w": 6,
        "h": 6
    },
    "rneminus19t84hto2g": {
        "fn": "rneminus19t84hto2g.rle",
        "w": 42,
        "h": 30
    },
    "Canoe synth": {
        "fn": "canoe_synth.rle",
        "w": 73,
        "h": 21
    },
    "Lake 2": {
        "fn": "lake2.rle",
        "w": 10,
        "h": 10
    },
    "diehard1583": {
        "fn": "diehard1583.rle",
        "w": 32,
        "h": 32
    },
    "Candelabra": {
        "fn": "candelabra.rle",
        "w": 16,
        "h": 6
    },
    "Long long barge": {
        "fn": "longlongbarge.rle",
        "w": 6,
        "h": 6
    },
    "Electric fence": {
        "fn": "electricfence.rle",
        "w": 61,
        "h": 15
    },
    "P94S": {
        "fn": "p94s.rle",
        "w": 607,
        "h": 155
    },
    "Sawtooth 1": {
        "fn": "sawtooth1.rle",
        "w": 173,
        "h": 114
    },
    "17c/45 Reaction": {
        "fn": "17c45reaction.rle",
        "w": 16,
        "h": 5
    },
    "l200": {
        "fn": "l200.rle",
        "w": 42,
        "h": 54
    },
    "Gray counter synth": {
        "fn": "graycounter_synth.rle",
        "w": 144,
        "h": 111
    },
    "period90glidergun": {
        "fn": "period90glidergun.rle",
        "w": 64,
        "h": 26
    },
    "rrx56h": {
        "fn": "rrx56h.rle",
        "w": 15,
        "h": 22
    },
    "Eater with cape": {
        "fn": "eaterwithcape.rle",
        "w": 6,
        "h": 4
    },
    "Centinal reflector": {
        "fn": "centinalreflector.rle",
        "w": 52,
        "h": 22
    },
    "Eater 1 with loaf": {
        "fn": "eater1withloaf.rle",
        "w": 9,
        "h": 7
    },
    "Fx119": {
        "fn": "fx119.rle",
        "w": 23,
        "h": 24
    },
    "long4snake synth": {
        "fn": "long4snake synth.rle",
        "w": 70,
        "h": 73
    },
    "Parabolic sawtooth": {
        "fn": "parabolicsawtooth.rle",
        "w": 126,
        "h": 144
    },
    "Barge (spaceship)": {
        "fn": "bargespaceship.rle",
        "w": 61,
        "h": 20
    },
    "2x2 10-cell still lifes": {
        "fn": "2x210cellstilllifes.rle",
        "w": 222,
        "h": 50
    },
    "p18 glider shuttle": {
        "fn": "p18glidershuttle.rle",
        "w": 55,
        "h": 55
    },
    "30clips": {
        "fn": "clips_synth.rle",
        "w": 150,
        "h": 115
    },
    "Bipole": {
        "fn": "bipole.rle",
        "w": 5,
        "h": 5
    },
    "superpond": {
        "fn": "superpond.rle",
        "w": 8,
        "h": 8
    },
    "long3snake synth": {
        "fn": "long3snake_synth.rle",
        "w": 33,
        "h": 18
    },
    "Triple pseudo still life": {
        "fn": "triplepseudostilllife.rle",
        "w": 8,
        "h": 10
    },
    "Wilma": {
        "fn": "wilma.rle",
        "w": 20,
        "h": 20
    },
    "negativespaceships": {
        "fn": "negativespaceships.rle",
        "w": 52,
        "h": 51
    },
    "thunderbird synth": {
        "fn": "thunderbird_synth.rle",
        "w": 24,
        "h": 8
    },
    "Still life tagalong": {
        "fn": "stilllifetagalong.rle",
        "w": 19,
        "h": 17
    },
    "bakersdozen2": {
        "fn": "bakersdozen2.rle",
        "w": 11,
        "h": 21
    },
    "Ed": {
        "fn": "ed.rle",
        "w": 20,
        "h": 20
    },
    "c/3 greyship": {
        "fn": "c3greyship.rle",
        "w": 296,
        "h": 131
    },
    "2x2 glider": {
        "fn": "2x2glider.rle",
        "w": 5,
        "h": 4
    },
    "Tredecapole": {
        "fn": "tredecapole.rle",
        "w": 16,
        "h": 16
    },
    "Herschel grandparent": {
        "fn": "herschelgrandparent.rle",
        "w": 5,
        "h": 4
    },
    "p62 blinker hassler": {
        "fn": "p62blinkerhassler.rle",
        "w": 40,
        "h": 26
    },
    "Stillater": {
        "fn": "stillater.rle",
        "w": 8,
        "h": 8
    },
    "43P18": {
        "fn": "43p18.rle",
        "w": 20,
        "h": 15
    },
    "Snake siamese snake": {
        "fn": "snakesiamesesnake.rle",
        "w": 7,
        "h": 2
    },
    "Bricklayer": {
        "fn": "bricklayer.rle",
        "w": 141,
        "h": 88
    },
    "Sesquihat": {
        "fn": "sesquihat.rle",
        "w": 7,
        "h": 5
    },
    "p4quintisnark": {
        "fn": "p4quintisnark.rle",
        "w": 140,
        "h": 146
    },
    "Ants": {
        "fn": "ants.rle",
        "w": 44,
        "h": 4
    },
    "Toaster": {
        "fn": "toaster.rle",
        "w": 11,
        "h": 12
    },
    "Snacker on 38P7.2": {
        "fn": "snackeron38p7.2.rle",
        "w": 24,
        "h": 20
    },
    "Blinker fuse 3": {
        "fn": "blinkerfuse3.rle",
        "w": 48,
        "h": 7
    },
    "Cis-mirrored worm": {
        "fn": "cismirroredworm.rle",
        "w": 6,
        "h": 9
    },
    "Tetraloaf I": {
        "fn": "tetraloaf1.rle",
        "w": 10,
        "h": 10
    },
    "transcarrierdownontable": {
        "fn": "transcarrierdownontable.rle",
        "w": 7,
        "h": 5
    },
    "p43 glider shuttle": {
        "fn": "p43glidershuttle.rle",
        "w": 65,
        "h": 65
    },
    "p65 glider loop": {
        "fn": "p65gliderloop.rle",
        "w": 87,
        "h": 87
    },
    "Mold on 41P7.2": {
        "fn": "moldon41p7.2.rle",
        "w": 15,
        "h": 14
    },
    "Spark coil": {
        "fn": "sparkcoil.rle",
        "w": 8,
        "h": 5
    },
    "Eater 4": {
        "fn": "eater4.rle",
        "w": 14,
        "h": 14
    },
    "Pentadecathlon synth": {
        "fn": "pentadecathlon_synth.rle",
        "w": 157,
        "h": 101
    },
    "2x2 period 2 oscillators": {
        "fn": "2x2period2oscillators.rle",
        "w": 51,
        "h": 16
    },
    "Shillelagh synth": {
        "fn": "shillelagh_synth.rle",
        "w": 154,
        "h": 118
    },
    "Primer": {
        "fn": "primer.rle",
        "w": 440,
        "h": 294
    },
    "Trans-loaf with tail": {
        "fn": "transloafwithtail.rle",
        "w": 6,
        "h": 6
    },
    "2 fumaroles": {
        "fn": "2fumaroles.rle",
        "w": 15,
        "h": 12
    },
    "Period 9 and 10 oscillators": {
        "fn": "period9and10oscillators.rle",
        "w": 402,
        "h": 331
    },
    "moldon34p14shuttle": {
        "fn": "moldon34p14shuttle.rle",
        "w": 15,
        "h": 19
    },
    "Tub with long tail": {
        "fn": "tubwithlongtail.rle",
        "w": 4,
        "h": 6
    },
    "Orion 2": {
        "fn": "orion2.rle",
        "w": 13,
        "h": 13
    },
    "Figure eight": {
        "fn": "figureeight.rle",
        "w": 6,
        "h": 6
    },
    "Harvester": {
        "fn": "harvester.rle",
        "w": 18,
        "h": 15
    },
    "Smaller newshuttle": {
        "fn": "smallernewshuttle.rle",
        "w": 35,
        "h": 20
    },
    "fireship synth": {
        "fn": "fireship_synth.rle",
        "w": 271,
        "h": 275
    },
    "Fireship": {
        "fn": "fireship.rle",
        "w": 12,
        "h": 21
    },
    "hooks synth": {
        "fn": "hooks_synth.rle",
        "w": 165,
        "h": 72
    },
    "Rabbits": {
        "fn": "rabbits.rle",
        "w": 7,
        "h": 3
    },
    "Crowd": {
        "fn": "crowd.rle",
        "w": 14,
        "h": 14
    },
    "Boat synth": {
        "fn": "boat_synth.rle",
        "w": 152,
        "h": 105
    },
    "R-bee and snake synth": {
        "fn": "rbeeandsnake_synth.rle",
        "w": 28,
        "h": 21
    },
    "twocisgriddleswtwotubs synth": {
        "fn": "twocisgriddleswtwotubs_synth.rle",
        "w": 108,
        "h": 26
    },
    "56p29 synth": {
        "fn": "56p29_synth.rle",
        "w": 383,
        "h": 62
    },
    "Jason's p36": {
        "fn": "jsp36.rle",
        "w": 39,
        "h": 18
    },
    "44p123 synth": {
        "fn": "44p123_synth.rle",
        "w": 515,
        "h": 249
    },
    "LWSS on HWSS": {
        "fn": "lwssonhwss.rle",
        "w": 7,
        "h": 12
    },
    "almostgun2": {
        "fn": "almostgun2.rle",
        "w": 24,
        "h": 24
    },
    "P44 pi-heptomino hassler": {
        "fn": "p44piheptominohassler.rle",
        "w": 31,
        "h": 44
    },
    "37P7.1": {
        "fn": "37p7.1.rle",
        "w": 12,
        "h": 10
    },
    "l112functions": {
        "fn": "l112functions.rle",
        "w": 50,
        "h": 108
    },
    "fx77parallelglider": {
        "fn": "fx77parallelglider.rle",
        "w": 29,
        "h": 23
    },
    "44P7.2": {
        "fn": "44p7.2.rle",
        "w": 16,
        "h": 16
    },
    "53p13": {
        "fn": "53p13.rle",
        "w": 14,
        "h": 12
    },
    "25P3H1V0.1": {
        "fn": "25p3h1v0.1.rle",
        "w": 16,
        "h": 5
    },
    "snarkbreaker": {
        "fn": "snarkbreaker.rle",
        "w": 819,
        "h": 907
    },
    "Airforce": {
        "fn": "airforce.rle",
        "w": 14,
        "h": 15
    },
    "Long cis-shillelagh": {
        "fn": "longcisshillelagh.rle",
        "w": 6,
        "h": 7
    },
    "Barge 2 (extended)": {
        "fn": "barge2extended.rle",
        "w": 105,
        "h": 50
    },
    "loafpull": {
        "fn": "loafpull.rle",
        "w": 17,
        "h": 19
    },
    "60p312 synth": {
        "fn": "60p312_synth.rle",
        "w": 351,
        "h": 204
    },
    "128P10.2": {
        "fn": "128p102.rle",
        "w": 35,
        "h": 35
    },
    "karelsp177": {
        "fn": "karelsp177.rle",
        "w": 46,
        "h": 46
    },
    "Merzenich p18": {
        "fn": "merzenichp18.rle",
        "w": 17,
        "h": 15
    },
    "Jolson synth": {
        "fn": "jolson_synth.rle",
        "w": 100,
        "h": 100
    },
    "$rats synth": {
        "fn": "rats_synth.rle",
        "w": 154,
        "h": 116
    },
    "diehard synth": {
        "fn": "diehard_synth.rle",
        "w": 17,
        "h": 14
    },
    "Boat-tie synth": {
        "fn": "boattie_synth.rle",
        "w": 153,
        "h": 86
    },
    "Glasses synth": {
        "fn": "glasses_synth.rle",
        "w": 132,
        "h": 39
    },
    "teardropwithcape": {
        "fn": "teardropwithcape.rle",
        "w": 6,
        "h": 5
    },
    "Skewed quad": {
        "fn": "skewedquad.rle",
        "w": 7,
        "h": 7
    },
    "Eureka": {
        "fn": "eureka.rle",
        "w": 18,
        "h": 11
    },
    "Griddle and blocks": {
        "fn": "griddleandblocks.rle",
        "w": 6,
        "h": 7
    },
    "Eater tail siamese carrier": {
        "fn": "eatertailsiamesecarrier.rle",
        "w": 4,
        "h": 7
    },
    "Replicator predecessor": {
        "fn": "replicatorpredecessor.rle",
        "w": 4,
        "h": 4
    },
    "Skew R-bees": {
        "fn": "skewrbees.rle",
        "w": 7,
        "h": 6
    },
    "p184gun": {
        "fn": "p184gun.rle",
        "w": 31,
        "h": 31
    },
    "transmangowithtail": {
        "fn": "transmangowithtail.rle",
        "w": 6,
        "h": 7
    },
    "cisorthoparatrans": {
        "fn": "cisorthoparatrans.rle",
        "w": 25,
        "h": 7
    },
    "Table": {
        "fn": "table.rle",
        "w": 4,
        "h": 2
    },
    "Loop": {
        "fn": "loop.rle",
        "w": 5,
        "h": 4
    },
    "long3boat": {
        "fn": "long3boat.rle",
        "w": 6,
        "h": 6
    },
    "Traffic circle": {
        "fn": "trafficcircle.rle",
        "w": 48,
        "h": 48
    },
    "P44 pi-heptomino hassler synth": {
        "fn": "p44piheptominohassler_synth.rle",
        "w": 94,
        "h": 90
    },
    "period156glidergun synth": {
        "fn": "period156glidergun_synth.rle",
        "w": 177,
        "h": 52
    },
    "Bookend synth": {
        "fn": "bookend_synth.rle",
        "w": 118,
        "h": 77
    },
    "Beacon maker": {
        "fn": "beaconmaker.rle",
        "w": 16,
        "h": 15
    },
    "long7boat": {
        "fn": "long7boat.rle",
        "w": 10,
        "h": 10
    },
    "period52gun5": {
        "fn": "period52gun5.rle",
        "w": 126,
        "h": 97
    },
    "Ship on long boat synth": {
        "fn": "shiponlongboat_synth.rle",
        "w": 148,
        "h": 82
    },
    "eatertieboat": {
        "fn": "eatertieboat.rle",
        "w": 7,
        "h": 7
    },
    "V-pentomino": {
        "fn": "vpentomino.rle",
        "w": 3,
        "h": 3
    },
    "T-tetromino": {
        "fn": "ttetromino.rle",
        "w": 3,
        "h": 2
    },
    "Scorpion": {
        "fn": "scorpion.rle",
        "w": 7,
        "h": 6
    },
    "Pentoad 1H2": {
        "fn": "pentoad1h2.rle",
        "w": 15,
        "h": 12
    },
    "carriersiamesedock": {
        "fn": "carriersiamesedock.rle",
        "w": 6,
        "h": 6
    },
    "P37 interaction": {
        "fn": "p37interaction.rle",
        "w": 72,
        "h": 44
    },
    "clockinsertion": {
        "fn": "clockinsertion.rle",
        "w": 59,
        "h": 57
    },
    "p156gun synth": {
        "fn": "p156gun_synth.rle",
        "w": 74,
        "h": 80
    },
    "p52emu": {
        "fn": "p52emu.rle",
        "w": 671,
        "h": 671
    },
    "Champage glass": {
        "fn": "champageglass.rle",
        "w": 15,
        "h": 12
    },
    "Do-see-do": {
        "fn": "doseedo.rle",
        "w": 55,
        "h": 38
    },
    "Spacefiller 2": {
        "fn": "spacefiller2.rle",
        "w": 25,
        "h": 35
    },
    "Cis-boat and table synth": {
        "fn": "cisboatandtable_synth.rle",
        "w": 104,
        "h": 123
    },
    "Mini pressure cooker": {
        "fn": "minipressurecooker.rle",
        "w": 11,
        "h": 10
    },
    "Bi-loaf 1 synth": {
        "fn": "biloaf1_synth.rle",
        "w": 149,
        "h": 91
    },
    "p23gun": {
        "fn": "p23gun.rle",
        "w": 66,
        "h": 34
    },
    "Cis-beacon and cap synth": {
        "fn": "cisbeaconandcap_synth.rle",
        "w": 106,
        "h": 12
    },
    "Mold on pentadecathlon": {
        "fn": "moldonpentadecathlon.rle",
        "w": 10,
        "h": 13
    },
    "Lightweight spaceship": {
        "fn": "lwss.rle",
        "w": 5,
        "h": 4
    },
    "Beehive at loaf synth": {
        "fn": "beehiveatloaf_synth.rle",
        "w": 40,
        "h": 20
    },
    "Cis-boat and long hook eating tub synth": {
        "fn": "cisboatandlonghookeatingtub_synth.rle",
        "w": 147,
        "h": 20
    },
    "(27,1)c/72 Herschel climber": {
        "fn": "271c72climber.rle",
        "w": 115,
        "h": 125
    },
    "Blinker": {
        "fn": "blinker.rle",
        "w": 3,
        "h": 1
    },
    "Weekender": {
        "fn": "weekender.rle",
        "w": 16,
        "h": 11
    },
    "Crown": {
        "fn": "crown.rle",
        "w": 23,
        "h": 14
    },
    "Queen bee turning reaction 1": {
        "fn": "queenbeeturningreaction1.rle",
        "w": 10,
        "h": 11
    },
    "blocksongriddle synth": {
        "fn": "blocksongriddle_synth.rle",
        "w": 29,
        "h": 24
    },
    "38P11.1": {
        "fn": "38p111.rle",
        "w": 12,
        "h": 12
    },
    "Loaf": {
        "fn": "loaf.rle",
        "w": 4,
        "h": 4
    },
    "Original P15 pre-pulsar spaceship": {
        "fn": "originalp15prepulsarspaceship.rle",
        "w": 51,
        "h": 144
    },
    "prepulsarshuttle32": {
        "fn": "prepulsarshuttle32.rle",
        "w": 26,
        "h": 33
    },
    "loafbacktieloaf": {
        "fn": "loafbacktieloaf.rle",
        "w": 6,
        "h": 6
    },
    "Crown synth": {
        "fn": "crown_synth.rle",
        "w": 118,
        "h": 117
    },
    "transboatamphisbaena": {
        "fn": "transboatamphisbaena.rle",
        "w": 6,
        "h": 7
    },
    "p29pentadecathlonhassler": {
        "fn": "p29pentadecathlonhassler.rle",
        "w": 58,
        "h": 22
    },
    "longboattieship": {
        "fn": "longboattieship.rle",
        "w": 7,
        "h": 7
    },
    "Quadpole": {
        "fn": "quadpole.rle",
        "w": 7,
        "h": 7
    },
    "Block-laying switch engine predecessor": {
        "fn": "blocklayingswitchenginepredecessor.rle",
        "w": 16,
        "h": 4
    },
    "Trans-beacon and cap": {
        "fn": "transbeaconandcap.rle",
        "w": 6,
        "h": 8
    },
    "transbeacondownonlonghook": {
        "fn": "transbeacondownonlonghook.rle",
        "w": 7,
        "h": 8
    },
    "P61 Herschel loop 2": {
        "fn": "p61herschelloop2.rle",
        "w": 381,
        "h": 70
    },
    "Jack": {
        "fn": "jack.rle",
        "w": 13,
        "h": 9
    },
    "P54 shuttle synth": {
        "fn": "p54shuttle_synth.rle",
        "w": 149,
        "h": 61
    },
    "Multum in parvo": {
        "fn": "multuminparvo.rle",
        "w": 6,
        "h": 4
    },
    "Cover": {
        "fn": "cover.rle",
        "w": 5,
        "h": 5
    },
    "106P135": {
        "fn": "106p135.rle",
        "w": 54,
        "h": 29
    },
    "Sawtooth 177": {
        "fn": "sawtooth177.rle",
        "w": 68,
        "h": 76
    },
    "flybydeletiontub": {
        "fn": "flybydeletiontub.rle",
        "w": 33,
        "h": 16
    },
    "tubwithlongtail synth": {
        "fn": "tubwithlongtail_synth.rle",
        "w": 26,
        "h": 12
    },
    "339P7H1V0": {
        "fn": "339p7h1v0.rle",
        "w": 17,
        "h": 96
    },
    "60P13.1": {
        "fn": "60p13.1.rle",
        "w": 25,
        "h": 25
    },
    "Squaredance": {
        "fn": "squaredance.rle",
        "w": 40,
        "h": 40
    },
    "Mirrored dock": {
        "fn": "mirroreddock.rle",
        "w": 6,
        "h": 7
    },
    "verylongcanoe": {
        "fn": "verylongcanoe.rle",
        "w": 7,
        "h": 7
    },
    "Glider-producing switch engine": {
        "fn": "gliderproducingswitchengine.rle",
        "w": 67,
        "h": 60
    },
    "Mazing synth": {
        "fn": "mazing_synth.rle",
        "w": 70,
        "h": 22
    },
    "Clock synth": {
        "fn": "clock_synth.rle",
        "w": 145,
        "h": 108
    },
    "Edna 26": {
        "fn": "edna26.rle",
        "w": 43,
        "h": 26
    },
    "caterloopillar31c240": {
        "fn": "caterloopillar31c240.rle",
        "w": 622,
        "h": 647036
    },
    "Block-laying switch engine synth": {
        "fn": "blocklayingswitchengine_synth.rle",
        "w": 138,
        "h": 49
    },
    "Hebdarole": {
        "fn": "hebdarole.rle",
        "w": 24,
        "h": 18
    },
    "Dart synth": {
        "fn": "dart_synth.rle",
        "w": 55,
        "h": 55
    },
    "Herschel parent": {
        "fn": "herschelparent.rle",
        "w": 5,
        "h": 4
    },
    "42100M": {
        "fn": "42100m.rle",
        "w": 16,
        "h": 16
    },
    "OTCA metapixel": {
        "fn": "otcametapixel.rle",
        "w": 2058,
        "h": 2058
    },
    "26p40 synth": {
        "fn": "26p40_synth.rle",
        "w": 113,
        "h": 30
    },
    "Beacon": {
        "fn": "beacon.rle",
        "w": 4,
        "h": 4
    },
    "l112periodquadrupler": {
        "fn": "l112periodquadrupler.rle",
        "w": 43,
        "h": 49
    },
    "fumaroleonp18biblockhassler": {
        "fn": "fumaroleonp18biblockhassler.rle",
        "w": 23,
        "h": 18
    },
    "Ship on bipole synth": {
        "fn": "shiponbipole_synth.rle",
        "w": 159,
        "h": 134
    },
    "Jaws": {
        "fn": "jaws.rle",
        "w": 654,
        "h": 2881
    },
    "Heavyweight emulator synth": {
        "fn": "heavyweightemulator_synth.rle",
        "w": 128,
        "h": 64
    },
    "Fumarole on 34P13": {
        "fn": "fumaroleon34p13.rle",
        "w": 21,
        "h": 18
    },
    "Bx125": {
        "fn": "bx125.rle",
        "w": 38,
        "h": 27
    },
    "achimsp8 synth": {
        "fn": "achimsp8_synth.rle",
        "w": 63,
        "h": 58
    },
    "54P3H1V0": {
        "fn": "54p3h1v0.rle",
        "w": 13,
        "h": 13
    },
    "beehive and table synth": {
        "fn": "beehiveandtable_synth.rle",
        "w": 106,
        "h": 49
    },
    "Cross 2": {
        "fn": "cross2.rle",
        "w": 15,
        "h": 15
    },
    "p60bheptominohassler synth": {
        "fn": "p60bheptominohassler_synth.rle",
        "w": 351,
        "h": 57
    },
    "Cis-rotated R-bee": {
        "fn": "cisrotatedrbee.rle",
        "w": 7,
        "h": 5
    },
    "Baker's dozen synth": {
        "fn": "bakersdozen_synth.rle",
        "w": 149,
        "h": 75
    },
    "Alternate wickstretcher 1": {
        "fn": "alternatewickstretcher1.rle",
        "w": 38,
        "h": 20
    },
    "doubleblockeating": {
        "fn": "doubleblockeating.rle",
        "w": 16,
        "h": 7
    },
    "diehard1192": {
        "fn": "diehard1192.rle",
        "w": 16,
        "h": 16
    },
    "Bumper": {
        "fn": "bumper.rle",
        "w": 21,
        "h": 9
    },
    "27vaseha": {
        "fn": "vasesiamesehat_synth.rle",
        "w": 111,
        "h": 32
    },
    "R64": {
        "fn": "r64.rle",
        "w": 23,
        "h": 26
    },
    "egyptianwalk": {
        "fn": "egyptianwalk.rle",
        "w": 7,
        "h": 4
    },
    "Montana": {
        "fn": "montana.rle",
        "w": 12,
        "h": 10
    },
    "beacon and long hook synth": {
        "fn": "cisbeaconupandlonghook_synth.rle",
        "w": 27,
        "h": 22
    },
    "Jellyfish": {
        "fn": "jellyfish.rle",
        "w": 4,
        "h": 4
    },
    "long3barge": {
        "fn": "long3barge.rle",
        "w": 7,
        "h": 7
    },
    "Block": {
        "fn": "block.rle",
        "w": 2,
        "h": 2
    },
    "Phoenix 1": {
        "fn": "phoenix1.rle",
        "w": 8,
        "h": 8
    },
    "Pufferfish rake": {
        "fn": "pufferfishrake.rle",
        "w": 58,
        "h": 79
    },
    "(34,7)c/156 Herschel climber": {
        "fn": "347c156climber.rle",
        "w": 303,
        "h": 60
    },
    "Backward space rake": {
        "fn": "backwardspacerake.rle",
        "w": 23,
        "h": 20
    },
    "30P4H2V0.4": {
        "fn": "30p4h2v04.rle",
        "w": 14,
        "h": 9
    },
    "bananaspark": {
        "fn": "bananaspark.rle",
        "w": 7,
        "h": 5
    },
    "122p80.1": {
        "fn": "122p801.rle",
        "w": 45,
        "h": 41
    },
    "Canada goose": {
        "fn": "canadagoose.rle",
        "w": 13,
        "h": 12
    },
    "Coe ship": {
        "fn": "coeship.rle",
        "w": 10,
        "h": 9
    },
    "beluchenkosp51 synth": {
        "fn": "beluchenkosp51_synth.rle",
        "w": 187,
        "h": 187
    },
    "Dock": {
        "fn": "dock.rle",
        "w": 6,
        "h": 3
    },
    "Achim's p4 synth": {
        "fn": "achimsp4_synth.rle",
        "w": 116,
        "h": 95
    },
    "htomwssrep72": {
        "fn": "htomwssrep72.rle",
        "w": 179,
        "h": 160
    },
    "torus": {
        "fn": "torus.rle",
        "w": 100,
        "h": 100
    },
    "31-1": {
        "fn": "ariesbetwixttwoblocks_synth.rle",
        "w": 111,
        "h": 40
    },
    "123P27.1": {
        "fn": "123p271.rle",
        "w": 20,
        "h": 22
    },
    "Long hook": {
        "fn": "longhook.rle",
        "w": 5,
        "h": 3
    },
    "Loaf siamese barge": {
        "fn": "loafsiamesebarge.rle",
        "w": 5,
        "h": 5
    },
    "Pentadecathlon on 38P7.2": {
        "fn": "pentadecathlonon38p7.2.rle",
        "w": 20,
        "h": 12
    },
    "Sawtooth 260": {
        "fn": "sawtooth260.rle",
        "w": 154,
        "h": 100
    },
    "44P12.2": {
        "fn": "44p122.rle",
        "w": 14,
        "h": 14
    },
    "Tub with cis-tail": {
        "fn": "tubwithcistail.rle",
        "w": 7,
        "h": 5
    },
    "Tri-block": {
        "fn": "triblock.rle",
        "w": 8,
        "h": 2
    },
    "p61 glider loop": {
        "fn": "p61gliderloop.rle",
        "w": 83,
        "h": 83
    },
    "Fumarole on 43P18": {
        "fn": "fumaroleon43p18.rle",
        "w": 23,
        "h": 18
    },
    "26p2 synth": {
        "fn": "26p2_synth.rle",
        "w": 162,
        "h": 83
    },
    "Glider": {
        "fn": "glider.rle",
        "w": 3,
        "h": 3
    },
    "introvert": {
        "fn": "introvert.rle",
        "w": 17,
        "h": 20
    },
    "herscheldescendant": {
        "fn": "herscheldescendant.rle",
        "w": 4,
        "h": 5
    },
    "Blocked p4 t-nose": {
        "fn": "blockedp4tnose.rle",
        "w": 25,
        "h": 13
    },
    "Cis-beacon and table": {
        "fn": "cisbeaconandtable.rle",
        "w": 4,
        "h": 7
    },
    "Glasses": {
        "fn": "glasses.rle",
        "w": 18,
        "h": 11
    },
    "Long fuse with two tails": {
        "fn": "longfusewithtwotails.rle",
        "w": 7,
        "h": 5
    },
    "c/4 orthogonal spaceships": {
        "fn": "c4orthogonalspaceships.rle",
        "w": 311,
        "h": 267
    },
    "Boat-tie": {
        "fn": "boattie.rle",
        "w": 6,
        "h": 6
    },
    "p60glidershuttle": {
        "fn": "p60glidershuttle.rle",
        "w": 35,
        "h": 7
    },
    "Glider-producing switch engine synth": {
        "fn": "gliderproducingswitchengine_synth.rle",
        "w": 168,
        "h": 54
    },
    "54P17.1": {
        "fn": "54p171.rle",
        "w": 15,
        "h": 13
    },
    "Cis-mirrored worm siamese cis-mirrored worm": {
        "fn": "cismirroredwormsiamesecismirroredworm.rle",
        "w": 11,
        "h": 9
    },
    "wavestretcher": {
        "fn": "wavestretcher.rle",
        "w": 105,
        "h": 75
    },
    "HighLife 13-cell still lifes": {
        "fn": "highlife13cellstilllifes.rle",
        "w": 288,
        "h": 150
    },
    "transblockonlonghook": {
        "fn": "transblockonlonghook.rle",
        "w": 5,
        "h": 6
    },
    "Fleet synth": {
        "fn": "fleet_synth.rle",
        "w": 149,
        "h": 111
    },
    "verylongprodigal": {
        "fn": "verylongprodigal.rle",
        "w": 8,
        "h": 4
    },
    "Honeybit": {
        "fn": "honeybit.rle",
        "w": 34,
        "h": 14
    },
    "pipsquirter1 synth": {
        "fn": "pipsquirter1_synth.rle",
        "w": 912,
        "h": 1530
    },
    "Orthogonal on-off": {
        "fn": "domino.rle",
        "w": 2,
        "h": 1
    },
    "spacenonfiller": {
        "fn": "spacenonfiller.rle",
        "w": 37,
        "h": 31
    },
    "30P4H2V0.4 glider synthesis": {
        "fn": "30p4h2v04_synth.rle",
        "w": 757,
        "h": 100
    },
    "14p2.1": {
        "fn": "14p2.1.rle",
        "w": 7,
        "h": 5
    },
    "60P5H2V0 eater": {
        "fn": "60p5h2v0eater.rle",
        "w": 33,
        "h": 26
    },
    "Blocker": {
        "fn": "blocker.rle",
        "w": 10,
        "h": 5
    },
    "f171": {
        "fn": "f171.rle",
        "w": 33,
        "h": 35
    },
    "memorycell": {
        "fn": "memorycell.rle",
        "w": 43,
        "h": 18
    },
    "Non-monotonic spaceship 1": {
        "fn": "nonmonotonicspaceship1.rle",
        "w": 21,
        "h": 11
    },
    "Blinker fuse synth": {
        "fn": "blinkerfuse_synth.rle",
        "w": 39,
        "h": 11
    },
    "Symmetric scorpion": {
        "fn": "symmetricscorpion.rle",
        "w": 7,
        "h": 5
    },
    "switchengine synth": {
        "fn": "switchengine_synth.rle",
        "w": 14,
        "h": 8
    },
    "verylongshillelagh synth": {
        "fn": "verylongshillelagh_synth.rle",
        "w": 32,
        "h": 12
    },
    "Queen bee": {
        "fn": "queenbee.rle",
        "w": 7,
        "h": 5
    },
    "loop synth": {
        "fn": "loop_synth.rle",
        "w": 28,
        "h": 13
    },
    "bargesiameseloaf synth": {
        "fn": "bargesiameseloaf_synth.rle",
        "w": 148,
        "h": 104
    },
    "p3bumper": {
        "fn": "p3bumper.rle",
        "w": 59,
        "h": 52
    },
    "101": {
        "fn": "101.rle",
        "w": 18,
        "h": 12
    },
    "L112": {
        "fn": "l112.rle",
        "w": 24,
        "h": 42
    },
    "Orion": {
        "fn": "orion.rle",
        "w": 14,
        "h": 15
    },
    "Octagon 4 synth": {
        "fn": "octagon4_synth.rle",
        "w": 142,
        "h": 118
    },
    "AK-94": {
        "fn": "ak94.rle",
        "w": 38,
        "h": 25
    },
    "14p2.3": {
        "fn": "14p2.3.rle",
        "w": 7,
        "h": 7
    },
    "Pre-pulsar shuttle 29 v3": {
        "fn": "prepulsarshuttle29v3.rle",
        "w": 39,
        "h": 39
    },
    "elkiesp5 synth": {
        "fn": "elkiesp5_synth.rle",
        "w": 29,
        "h": 30
    },
    "long6snake": {
        "fn": "long6snake.rle",
        "w": 8,
        "h": 10
    },
    "p40 glider gun": {
        "fn": "period40glidergun.rle",
        "w": 226,
        "h": 220
    },
    "buckaroo180": {
        "fn": "buckaroo180.rle",
        "w": 23,
        "h": 27
    },
    "Cis-loaf with tail synth": {
        "fn": "cisloafwithtail_synth.rle",
        "w": 66,
        "h": 66
    },
    "duelingbanjos synth": {
        "fn": "duelingbanjos_synth.rle",
        "w": 170,
        "h": 197
    },
    "Bi-block synth": {
        "fn": "biblock_synth.rle",
        "w": 105,
        "h": 92
    },
    "104P177": {
        "fn": "104p177.rle",
        "w": 46,
        "h": 46
    },
    "Monogram": {
        "fn": "monogram.rle",
        "w": 7,
        "h": 5
    },
    "Fumarole on Achim's p11": {
        "fn": "fumaroleonachimsp11.rle",
        "w": 29,
        "h": 22
    },
    "Dinner table": {
        "fn": "dinnertable.rle",
        "w": 13,
        "h": 13
    },
    "Revolver": {
        "fn": "revolver.rle",
        "w": 14,
        "h": 8
    },
    "heavyweightvolcanosuhajda": {
        "fn": "heavyweightvolcanosuhajda.rle",
        "w": 28,
        "h": 17
    },
    "translongbargewithtail": {
        "fn": "translongbargewithtail.rle",
        "w": 7,
        "h": 7
    },
    "pdpairreflector": {
        "fn": "pdpairreflector.rle",
        "w": 45,
        "h": 30
    },
    "p72quasishuttle": {
        "fn": "p72quasishuttle.rle",
        "w": 37,
        "h": 23
    },
    "13loop2": {
        "fn": "13loop2.rle",
        "w": 6,
        "h": 6
    },
    "modelf": {
        "fn": "modelf.rle",
        "w": 689,
        "h": 715
    },
    "Pentant": {
        "fn": "pentant.rle",
        "w": 10,
        "h": 12
    },
    "55p9h3v0": {
        "fn": "55p9h3v0.rle",
        "w": 21,
        "h": 13
    },
    "Fumarole": {
        "fn": "fumarole.rle",
        "w": 8,
        "h": 7
    },
    "2c/5 puffer 1": {
        "fn": "2c5puffer1.rle",
        "w": 112,
        "h": 75
    },
    "Brain": {
        "fn": "brain.rle",
        "w": 17,
        "h": 11
    },
    "2x12 infinite growth": {
        "fn": "2x12infinitegrowth.rle",
        "w": 12,
        "h": 2
    },
    "stopandrestart": {
        "fn": "stopandrestart.rle",
        "w": 58,
        "h": 59
    },
    "zebrastripes": {
        "fn": "zebrastripes.rle",
        "w": 27,
        "h": 21
    },
    "wing synth": {
        "fn": "wing_synth.rle",
        "w": 10,
        "h": 8
    },
    "Shillelagh": {
        "fn": "shillelagh.rle",
        "w": 5,
        "h": 3
    },
    "prepulsarshuttle64": {
        "fn": "prepulsarshuttle64.rle",
        "w": 33,
        "h": 51
    },
    "Griddle and block": {
        "fn": "griddleandblock.rle",
        "w": 6,
        "h": 7
    },
    "Trans-rotated R-bee": {
        "fn": "transrotatedrbee.rle",
        "w": 7,
        "h": 7
    },
    "pinwheel synth": {
        "fn": "pinwheel_synth.rle",
        "w": 1406,
        "h": 529
    },
    "F166": {
        "fn": "f166.rle",
        "w": 56,
        "h": 31
    },
    "42P10.1": {
        "fn": "42p101.rle",
        "w": 13,
        "h": 13
    },
    "7\u00d79 eater": {
        "fn": "7by9eater.rle",
        "w": 7,
        "h": 9
    },
    "Fx153": {
        "fn": "fx153.rle",
        "w": 38,
        "h": 27
    },
    "pentant synth": {
        "fn": "pentant_synth.rle",
        "w": 252,
        "h": 39
    },
    "Queen bee shuttle pi predecessor": {
        "fn": "queenbeeshuttlepipredecessor.rle",
        "w": 35,
        "h": 7
    },
    "Honey farm": {
        "fn": "honeyfarm.rle",
        "w": 13,
        "h": 13
    },
    "B29": {
        "fn": "b29.rle",
        "w": 8,
        "h": 16
    },
    "Quadruple burloaferimeter 2": {
        "fn": "quadrupleburloaferimeter2.rle",
        "w": 16,
        "h": 16
    },
    "233P3H1V0": {
        "fn": "233p3h1v0.rle",
        "w": 31,
        "h": 36
    },
    "26P2": {
        "fn": "26p2.rle",
        "w": 8,
        "h": 7
    },
    "long5boat": {
        "fn": "long5boat.rle",
        "w": 8,
        "h": 8
    },
    "p5pipsquirter": {
        "fn": "p5pipsquirter.rle",
        "w": 15,
        "h": 13
    },
    "Fourteener synth": {
        "fn": "fourteener_synth.rle",
        "w": 76,
        "h": 25
    },
    "Two-glider syntheses": {
        "fn": "twoglidersyntheses.rle",
        "w": 379,
        "h": 369
    },
    "Bi-boat": {
        "fn": "biboat.rle",
        "w": 7,
        "h": 3
    },
    "Pulsar synth": {
        "fn": "pulsar_synth.rle",
        "w": 152,
        "h": 118
    },
    "cisboatwithnine synth": {
        "fn": "cisboatwithnine_synth.rle",
        "w": 103,
        "h": 15
    },
    "Long long snake synth": {
        "fn": "longlongsnake_synth.rle",
        "w": 114,
        "h": 26
    },
    "Buckaroo": {
        "fn": "buckaroo.rle",
        "w": 23,
        "h": 9
    },
    "burloaferimeter synth": {
        "fn": "burloaferimeter_synth.rle",
        "w": 221,
        "h": 28
    },
    "Twin bees shuttle v2": {
        "fn": "twinbeesshuttlev2.rle",
        "w": 27,
        "h": 17
    },
    "Duoplet": {
        "fn": "duoplet.rle",
        "w": 2,
        "h": 2
    },
    "A for all": {
        "fn": "aforall.rle",
        "w": 10,
        "h": 10
    },
    "nw31transparentlanes": {
        "fn": "nw31transparentlanes.rle",
        "w": 50,
        "h": 60
    },
    "209p8": {
        "fn": "209p8.rle",
        "w": 34,
        "h": 23
    },
    "CP semi-Snark": {
        "fn": "cpsemisnark.rle",
        "w": 23,
        "h": 23
    },
    "Beehive with nine synth": {
        "fn": "beehivewithnine_synth.rle",
        "w": 67,
        "h": 40
    },
    "p9bouncer": {
        "fn": "p9bouncer.rle",
        "w": 63,
        "h": 63
    },
    "eaterwithtailfeather": {
        "fn": "eaterwithtailfeather.rle",
        "w": 7,
        "h": 5
    },
    "Almosymmetric": {
        "fn": "almosymmetric.rle",
        "w": 9,
        "h": 8
    },
    "Confused eaters synth": {
        "fn": "confusedeaters_synth.rle",
        "w": 131,
        "h": 104
    },
    "60p72": {
        "fn": "60p72.rle",
        "w": 26,
        "h": 20
    },
    "Quad": {
        "fn": "quad.rle",
        "w": 6,
        "h": 6
    },
    "Carrier siamese carrier synth": {
        "fn": "carriersiamesecarrier_synth.rle",
        "w": 89,
        "h": 83
    },
    "Long long canoe": {
        "fn": "longlongcanoe.rle",
        "w": 7,
        "h": 7
    },
    "Pipsquirter 2": {
        "fn": "pipsquirter2.rle",
        "w": 13,
        "h": 22
    },
    "Z-pentomino": {
        "fn": "zpentomino.rle",
        "w": 3,
        "h": 3
    },
    "Trans-queen bee shuttle": {
        "fn": "transqueenbeeshuttle.rle",
        "w": 22,
        "h": 7
    },
    "Metacatacryst": {
        "fn": "metacatacryst.rle",
        "w": 59739,
        "h": 14663
    },
    "diagonalonoff": {
        "fn": "diagonalonoff.rle",
        "w": 2,
        "h": 2
    },
    "HighLife 4-cell still lifes": {
        "fn": "highlife4cellstilllifes.rle",
        "w": 9,
        "h": 3
    },
    "longboatwithlongtail": {
        "fn": "longboatwithlongtail.rle",
        "w": 7,
        "h": 5
    },
    "double28p7.3": {
        "fn": "double28p7.3.rle",
        "w": 11,
        "h": 18
    },
    "cismirroredprofessor synth": {
        "fn": "cismirroredprofessor_synth.rle",
        "w": 131,
        "h": 53
    },
    "p8bouncer": {
        "fn": "p8bouncer.rle",
        "w": 61,
        "h": 54
    },
    "Rabbits synth": {
        "fn": "rabbits_synth.rle",
        "w": 48,
        "h": 13
    },
    "Trans-hook and R-bee": {
        "fn": "transhookandrbee.rle",
        "w": 5,
        "h": 7
    },
    "487tickreflector": {
        "fn": "487tickreflector.rle",
        "w": 270,
        "h": 174
    },
    "87p26": {
        "fn": "87p26.rle",
        "w": 17,
        "h": 16
    },
    "Achim's p16 synth": {
        "fn": "achimsp16_synth.rle",
        "w": 203,
        "h": 67
    },
    "transcarriertie": {
        "fn": "transcarriertie.rle",
        "w": 7,
        "h": 7
    },
    "jasonsp156": {
        "fn": "jasonsp156.rle",
        "w": 42,
        "h": 42
    },
    "Windmill": {
        "fn": "windmill.rle",
        "w": 18,
        "h": 18
    },
    "Unicycle synth": {
        "fn": "unicycle_synth.rle",
        "w": 146,
        "h": 91
    },
    "tubwithlong3tail": {
        "fn": "tubwithlong3tail.rle",
        "w": 4,
        "h": 8
    },
    "2enginecordershipseed": {
        "fn": "2enginecordershipseed.rle",
        "w": 55,
        "h": 48
    },
    "Edge-repair spaceship 2": {
        "fn": "edgerepairspaceship2.rle",
        "w": 22,
        "h": 11
    },
    "prepulsarshuttle90": {
        "fn": "prepulsarshuttle90.rle",
        "w": 24,
        "h": 31
    },
    "Quadfuse": {
        "fn": "quadfuse.rle",
        "w": 2742,
        "h": 13
    },
    "lx84": {
        "fn": "lx84.rle",
        "w": 25,
        "h": 53
    },
    "oddkeys synth": {
        "fn": "oddkeys_synth.rle",
        "w": 91,
        "h": 90
    },
    "44p14": {
        "fn": "44p14.rle",
        "w": 13,
        "h": 10
    },
    "Gosper glider gun": {
        "fn": "gosperglidergun.rle",
        "w": 36,
        "h": 9
    },
    "56P27": {
        "fn": "56p27.rle",
        "w": 30,
        "h": 30
    },
    "lei": {
        "fn": "lei.rle",
        "w": 7,
        "h": 6
    },
    "caterer synth": {
        "fn": "caterer_synth.rle",
        "w": 19,
        "h": 20
    },
    "Caber tosser 1": {
        "fn": "cabertosser1.rle",
        "w": 145,
        "h": 114
    },
    "hertzoscillator synth": {
        "fn": "hertzoscillator_synth.rle",
        "w": 571,
        "h": 243
    },
    "House on house siamese table-on-table weld hat-siamese-hat": {
        "fn": "houseonhousesiamesetableontableweldhatsiamesehat.rle",
        "w": 7,
        "h": 13
    },
    "p57gun": {
        "fn": "p57gun.rle",
        "w": 582,
        "h": 104
    },
    "Twin prime calculator": {
        "fn": "twinprimecalculator.rle",
        "w": 440,
        "h": 294
    },
    "cross synth": {
        "fn": "cross_synth.rle",
        "w": 46,
        "h": 32
    },
    "Caterer on 42P7.1": {
        "fn": "catereron42p7.1.rle",
        "w": 17,
        "h": 14
    },
    "Heavyweight spaceship": {
        "fn": "hwss.rle",
        "w": 7,
        "h": 5
    },
    "Cuphook": {
        "fn": "cuphook.rle",
        "w": 9,
        "h": 8
    },
    "Octagon 2 synth": {
        "fn": "octagon2_synth.rle",
        "w": 153,
        "h": 79
    },
    "Block and glider": {
        "fn": "blockandglider.rle",
        "w": 4,
        "h": 3
    },
    "28p6 synth": {
        "fn": "28p6_synth.rle",
        "w": 65,
        "h": 35
    },
    "31.4": {
        "fn": "31.4.rle",
        "w": 13,
        "h": 8
    },
    "Sesquihat synth": {
        "fn": "sesquihat_synth.rle",
        "w": 111,
        "h": 103
    },
    "Snake synth": {
        "fn": "snake_synth.rle",
        "w": 98,
        "h": 74
    },
    "skewedquad synth": {
        "fn": "skewedquad_synth.rle",
        "w": 30,
        "h": 15
    },
    "24P10": {
        "fn": "24p10.rle",
        "w": 13,
        "h": 9
    },
    "symmetricalsynapse": {
        "fn": "symmetricalsynapse.rle",
        "w": 9,
        "h": 9
    },
    "134P25": {
        "fn": "134p25.rle",
        "w": 35,
        "h": 23
    },
    "longboattieship synth": {
        "fn": "longboattieship_synth.rle",
        "w": 39,
        "h": 16
    },
    "long3shillelagh": {
        "fn": "long3shillelagh.rle",
        "w": 5,
        "h": 8
    },
    "long8boat": {
        "fn": "long8boat.rle",
        "w": 11,
        "h": 11
    },
    "Spaghetti monster": {
        "fn": "702p7h3v0.rle",
        "w": 27,
        "h": 137
    },
    "246p3": {
        "fn": "246p3.rle",
        "w": 28,
        "h": 24
    },
    "Two pulsar quadrants": {
        "fn": "twopulsarquadrants.rle",
        "w": 9,
        "h": 9
    },
    "Blom": {
        "fn": "blom.rle",
        "w": 12,
        "h": 5
    },
    "Alternate eater 5": {
        "fn": "alternateeater5.rle",
        "w": 10,
        "h": 6
    },
    "c/3 ladders": {
        "fn": "c3ladders.rle",
        "w": 14,
        "h": 213
    },
    "monogram synth": {
        "fn": "monogram_synth.rle",
        "w": 64,
        "h": 61
    },
    "Mango test tube baby": {
        "fn": "mangotesttubebaby.rle",
        "w": 14,
        "h": 7
    },
    "bunnies10a": {
        "fn": "bunnies10a.rle",
        "w": 7,
        "h": 5
    },
    "2c/5 ladders": {
        "fn": "2c5ladders.rle",
        "w": 13,
        "h": 274
    },
    "gliderto2blocks": {
        "fn": "gliderto2blocks.rle",
        "w": 34,
        "h": 27
    },
    "Trans-fuse with two tails synth": {
        "fn": "transfusewithtwotails_synth.rle",
        "w": 68,
        "h": 34
    },
    "Fx158": {
        "fn": "fx158.rle",
        "w": 30,
        "h": 30
    },
    "Tub with tail": {
        "fn": "tubwithtail.rle",
        "w": 5,
        "h": 5
    },
    "p59 glider loop": {
        "fn": "p59gliderloop.rle",
        "w": 81,
        "h": 81
    },
    "p24lwss": {
        "fn": "p24lwss.rle",
        "w": 84,
        "h": 89
    },
    "Spacefiller 1": {
        "fn": "spacefiller1.rle",
        "w": 29,
        "h": 43
    },
    "Tubber": {
        "fn": "tubber.rle",
        "w": 13,
        "h": 13
    },
    "Lightweight emulator": {
        "fn": "lightweightemulator.rle",
        "w": 14,
        "h": 6
    },
    "Pre-pulsar shuttle 29": {
        "fn": "prepulsarshuttle29.rle",
        "w": 28,
        "h": 28
    },
    "35P52": {
        "fn": "35p52.rle",
        "w": 17,
        "h": 17
    },
    "Eleven loop": {
        "fn": "elevenloop.rle",
        "w": 5,
        "h": 5
    },
    "Paperclip synth": {
        "fn": "paperclip_synth.rle",
        "w": 149,
        "h": 57
    },
    "p5760 unit Life cell": {
        "fn": "p5760unitlifecell.rle",
        "w": 499,
        "h": 499
    },
    "244p3": {
        "fn": "244p3.rle",
        "w": 28,
        "h": 24
    },
    "Odd test tube baby": {
        "fn": "oddtesttubebaby.rle",
        "w": 9,
        "h": 6
    },
    "Ship-tie": {
        "fn": "shiptie.rle",
        "w": 6,
        "h": 6
    },
    "Period-60 glider gun": {
        "fn": "period60glidergun.rle",
        "w": 39,
        "h": 27
    },
    "Long canoe synth": {
        "fn": "longcanoe_synth.rle",
        "w": 74,
        "h": 16
    },
    "Mold on fumarole synth": {
        "fn": "moldonfumarole_synth.rle",
        "w": 53,
        "h": 30
    },
    "asymmetricamphisbaena": {
        "fn": "asymmetricamphisbaena.rle",
        "w": 5,
        "h": 8
    },
    "Monoclaw test tube baby": {
        "fn": "monoclawtesttubebaby.rle",
        "w": 10,
        "h": 6
    },
    "Trice tongs": {
        "fn": "tricetongs.rle",
        "w": 7,
        "h": 7
    },
    "Inline inverter": {
        "fn": "inlineinverter.rle",
        "w": 36,
        "h": 20
    },
    "145P20": {
        "fn": "145p20.rle",
        "w": 28,
        "h": 21
    },
    "carriersiamesehookwithtailtail": {
        "fn": "carriersiamesehookwithtailtail.rle",
        "w": 7,
        "h": 5
    },
    "Rumbling river 1": {
        "fn": "rumblingriver1.rle",
        "w": 49,
        "h": 12
    },
    "coesp8 synth": {
        "fn": "coesp8_synth.rle",
        "w": 143,
        "h": 25
    },
    "Overweight emulator": {
        "fn": "overweightemulator.rle",
        "w": 17,
        "h": 9
    },
    "mold on 36P22 synth": {
        "fn": "moldon36p22_synth.rle",
        "w": 88,
        "h": 39
    },
    "c/5 diagonal tubstretcher": {
        "fn": "c5diagonaltubstretcher.rle",
        "w": 55,
        "h": 55
    },
    "123 synth": {
        "fn": "123_synth.rle",
        "w": 611,
        "h": 273
    },
    "p30gunexample": {
        "fn": "p30gunexample.rle",
        "w": 37,
        "h": 27
    },
    "Ecologist": {
        "fn": "ecologist.rle",
        "w": 27,
        "h": 18
    },
    "fumaroleon34p14shuttle": {
        "fn": "fumaroleon34p14shuttle.rle",
        "w": 15,
        "h": 21
    },
    "long5snake synth": {
        "fn": "long5snake_synth.rle",
        "w": 70,
        "h": 73
    },
    "halfbakery synth": {
        "fn": "halfbakery_synth.rle",
        "w": 149,
        "h": 91
    },
    "unixonrichsp16": {
        "fn": "unixonrichsp16.rle",
        "w": 23,
        "h": 17
    },
    "prepulsarshuttle36": {
        "fn": "prepulsarshuttle36.rle",
        "w": 36,
        "h": 75
    },
    "Period 5 oscillators": {
        "fn": "period5oscillators.rle",
        "w": 311,
        "h": 154
    },
    "Chemist": {
        "fn": "chemist.rle",
        "w": 15,
        "h": 13
    },
    "carriertieboat synth": {
        "fn": "carriertieboat_synth.rle",
        "w": 44,
        "h": 45
    },
    "Coe ship synth": {
        "fn": "coeship_synth.rle",
        "w": 109,
        "h": 154
    },
    "Killer candlefrobras eating a HWSS": {
        "fn": "killercandlefrobraseatinghwss.rle",
        "w": 21,
        "h": 14
    },
    "88p25 synth": {
        "fn": "88p25_synth.rle",
        "w": 235,
        "h": 38
    },
    "Gabriel's p138 synth": {
        "fn": "gabrielsp138_synth.rle",
        "w": 63,
        "h": 27
    },
    "R-pentomino": {
        "fn": "rpentomino.rle",
        "w": 3,
        "h": 3
    },
    "Venetian blinds": {
        "fn": "venetianblinds.rle",
        "w": 38,
        "h": 45
    },
    "p49bumperloop": {
        "fn": "p49bumperloop.rle",
        "w": 40,
        "h": 40
    },
    "Tablecloth agar": {
        "fn": "tablecloth.rle",
        "w": 4,
        "h": 4
    },
    "Wickstretcher 1": {
        "fn": "wickstretcher1.rle",
        "w": 49,
        "h": 16
    },
    "pressurecooker synth": {
        "fn": "pressurecooker_synth.rle",
        "w": 1175,
        "h": 644
    },
    "Traffic light": {
        "fn": "trafficlight.rle",
        "w": 7,
        "h": 7
    },
    "10-cell infinite growth": {
        "fn": "10cellinfinitegrowth.rle",
        "w": 8,
        "h": 6
    },
    "Maze 6-cell still lifes": {
        "fn": "maze6cellstilllifes.rle",
        "w": 58,
        "h": 13
    },
    "c/4 diagonal spaceships": {
        "fn": "c4diagonalspaceships.rle",
        "w": 392,
        "h": 267
    },
    "(p, p+8) prime calculator": {
        "fn": "pp8primecalculator.rle",
        "w": 1022,
        "h": 618
    },
    "Maze 3-cell still lifes": {
        "fn": "maze3cellstilllifes.rle",
        "w": 3,
        "h": 3
    },
    "lx65": {
        "fn": "lx65.rle",
        "w": 75,
        "h": 67
    },
    "Glider synth": {
        "fn": "glider_synth.rle",
        "w": 110,
        "h": 43
    },
    "Gardens of Eden": {
        "fn": "gardensofeden.rle",
        "w": 276,
        "h": 141
    },
    "Gliders by the dozen": {
        "fn": "glidersbythedozen.rle",
        "w": 5,
        "h": 3
    },
    "boatamphisbaena": {
        "fn": "boatamphisbaena.rle",
        "w": 6,
        "h": 7
    },
    "Beehive bend tail": {
        "fn": "beehivebendtail.rle",
        "w": 5,
        "h": 6
    },
    "37P10.2": {
        "fn": "37p10.2.rle",
        "w": 13,
        "h": 13
    },
    "5c9wire": {
        "fn": "5c9wire.rle",
        "w": 91,
        "h": 86
    },
    "Middleweight volcano": {
        "fn": "middleweightvolcano.rle",
        "w": 13,
        "h": 11
    },
    "shillelaghsiamesesnake": {
        "fn": "shillelaghsiamesesnake.rle",
        "w": 3,
        "h": 8
    },
    "Hook with tail": {
        "fn": "hookwithtail.rle",
        "w": 5,
        "h": 4
    },
    "p48lwss": {
        "fn": "p48lwss.rle",
        "w": 71,
        "h": 67
    },
    "biblockfuse": {
        "fn": "biblockfuse.rle",
        "w": 47,
        "h": 5
    },
    "Universal turing machine": {
        "fn": "universalturingmachine.rle",
        "w": 12699,
        "h": 12652
    },
    "Jolson (period 9)": {
        "fn": "jolsonperiod9.rle",
        "w": 22,
        "h": 24
    },
    "Lidka": {
        "fn": "lidka.rle",
        "w": 9,
        "h": 15
    },
    "pf35w": {
        "fn": "pf35w.rle",
        "w": 15,
        "h": 27
    },
    "p4bouncer": {
        "fn": "p4bouncer.rle",
        "w": 47,
        "h": 60
    },
    "Octagon 2 on 36P22": {
        "fn": "octagon2on36p22.rle",
        "w": 32,
        "h": 16
    },
    "P50 traffic jam": {
        "fn": "p50trafficjam.rle",
        "w": 48,
        "h": 16
    },
    "Bookends siamese tables": {
        "fn": "bookendssiamesetables.rle",
        "w": 7,
        "h": 7
    },
    "oddtesttubebaby synth": {
        "fn": "oddtesttubebaby_synth.rle",
        "w": 45,
        "h": 22
    },
    "biblockpuffer": {
        "fn": "biblockpuffer.rle",
        "w": 59,
        "h": 29
    },
    "Long long shillelagh": {
        "fn": "longlongshillelagh.rle",
        "w": 7,
        "h": 4
    },
    "28P7.2": {
        "fn": "28p7.2.rle",
        "w": 12,
        "h": 11
    },
    "twoblockershasslingrpentomino": {
        "fn": "twoblockershasslingrpentomino.rle",
        "w": 30,
        "h": 14
    },
    "halfbakery": {
        "fn": "halfbakery.rle",
        "w": 7,
        "h": 7
    },
    "Line-puffer": {
        "fn": "linepuffer.rle",
        "w": 156,
        "h": 32
    },
    "verylongboat synth": {
        "fn": "verylongboat_synth.rle",
        "w": 151,
        "h": 101
    },
    "Sidewalk synth": {
        "fn": "sidewalk_synth.rle",
        "w": 72,
        "h": 8
    },
    "jormungantsgtoh": {
        "fn": "jormungantsgtoh.rle",
        "w": 27,
        "h": 57
    },
    "38P7.3": {
        "fn": "38p7.3.rle",
        "w": 13,
        "h": 13
    },
    "23P2": {
        "fn": "23p2.rle",
        "w": 7,
        "h": 8
    },
    "R-loaf": {
        "fn": "rloaf.rle",
        "w": 4,
        "h": 4
    },
    "carrierbridgecarrier": {
        "fn": "carrierbridgecarrier.rle",
        "w": 6,
        "h": 6
    },
    "62p3.1": {
        "fn": "62p3.1.rle",
        "w": 10,
        "h": 18
    },
    "Hustler II synth": {
        "fn": "hustlerii_synth.rle",
        "w": 155,
        "h": 119
    },
    "p50 glider gun variant by David Bell": {
        "fn": "period50glidergunbell.rle",
        "w": 120,
        "h": 95
    },
    "44P5H2V0": {
        "fn": "44p5h2v0tagalongs.rle",
        "w": 66,
        "h": 35
    },
    "extendedp6shuttle": {
        "fn": "extendedp6shuttle.rle",
        "w": 25,
        "h": 24
    },
    "simkinglidergun synth": {
        "fn": "simkinglidergun_synth.rle",
        "w": 39,
        "h": 33
    },
    "Diamond ring": {
        "fn": "diamondring.rle",
        "w": 13,
        "h": 13
    },
    "Hat": {
        "fn": "hat.rle",
        "w": 5,
        "h": 4
    },
    "p7bumper": {
        "fn": "p7bumper.rle",
        "w": 65,
        "h": 57
    },
    "HighLife 7-cell still lifes": {
        "fn": "highlife7cellstilllifes.rle",
        "w": 26,
        "h": 4
    },
    "Partial queen bee loop": {
        "fn": "partialqueenbeeloop.rle",
        "w": 24,
        "h": 24
    },
    "Ship in a bottle": {
        "fn": "shipinabottle.rle",
        "w": 18,
        "h": 18
    },
    "Light bulb synth": {
        "fn": "lightbulb_synth.rle",
        "w": 71,
        "h": 30
    },
    "Tremi-Snark": {
        "fn": "tremisnark.rle",
        "w": 18,
        "h": 25
    },
    "Time bomb": {
        "fn": "timebomb.rle",
        "w": 15,
        "h": 6
    },
    "twogun": {
        "fn": "twogun.rle",
        "w": 39,
        "h": 27
    },
    "long3hookwithtail": {
        "fn": "long3hookwithtail.rle",
        "w": 5,
        "h": 8
    },
    "19-4199": {
        "fn": "longhorn_synth.rle",
        "w": 77,
        "h": 66
    },
    "Baker's dozen": {
        "fn": "bakersdozen.rle",
        "w": 23,
        "h": 11
    },
    "P35 honey farm hassler": {
        "fn": "p35honeyfarmhassler.rle",
        "w": 24,
        "h": 17
    },
    "extended68p9": {
        "fn": "extended68p9.rle",
        "w": 40,
        "h": 17
    },
    "duelingbanjosp24gun": {
        "fn": "duelingbanjosp24gun.rle",
        "w": 40,
        "h": 31
    },
    "Herschel great-grandparent": {
        "fn": "herschelgreatgrandparent.rle",
        "w": 7,
        "h": 5
    },
    "cisaircraftontable": {
        "fn": "cisaircraftontable.rle",
        "w": 6,
        "h": 5
    },
    "Worker bee": {
        "fn": "workerbee.rle",
        "w": 16,
        "h": 11
    },
    "p6bumper": {
        "fn": "p6bumper.rle",
        "w": 63,
        "h": 53
    },
    "22P36 synth": {
        "fn": "22p36_synth.rle",
        "w": 134,
        "h": 28
    },
    "p8 swan tagalong": {
        "fn": "p8swantagalong.rle",
        "w": 28,
        "h": 28
    },
    "43P18 synth": {
        "fn": "43p18_synth.rle",
        "w": 120,
        "h": 81
    },
    "Hive test tube baby": {
        "fn": "hivetesttubebaby.rle",
        "w": 12,
        "h": 6
    },
    "Sixty-nine": {
        "fn": "sixtynine.rle",
        "w": 21,
        "h": 21
    },
    "Maze 5-cell still lifes": {
        "fn": "maze5cellstilllifes.rle",
        "w": 38,
        "h": 5
    },
    "c/5 greyship": {
        "fn": "c5greyship.rle",
        "w": 286,
        "h": 199
    },
    "3c/10 pi wave": {
        "fn": "3c10piwave.rle",
        "w": 45,
        "h": 29
    },
    "eureka synth": {
        "fn": "eureka_synth.rle",
        "w": 64,
        "h": 117
    },
    "Light speed oscillator 1": {
        "fn": "lightspeedoscillator1.rle",
        "w": 69,
        "h": 19
    },
    "centuryeater2": {
        "fn": "centuryeater2.rle",
        "w": 52,
        "h": 42
    },
    "bipoletiesnake": {
        "fn": "bipoletiesnake.rle",
        "w": 9,
        "h": 7
    },
    "longintegral synth": {
        "fn": "longintegral_synth.rle",
        "w": 32,
        "h": 15
    },
    "Transparent block reaction": {
        "fn": "transparentblockreaction.rle",
        "w": 14,
        "h": 4
    },
    "Block-laying switch engine": {
        "fn": "blocklayingswitchengine.rle",
        "w": 29,
        "h": 28
    },
    "P5 reflector with gliders": {
        "fn": "p5reflectorwithgliders.rle",
        "w": 29,
        "h": 25
    },
    "ciscarriertie": {
        "fn": "ciscarriertie.rle",
        "w": 6,
        "h": 8
    },
    "Lightweight spaceship synth": {
        "fn": "lwss_synth.rle",
        "w": 149,
        "h": 103
    },
    "22P2": {
        "fn": "22p2.rle",
        "w": 7,
        "h": 8
    },
    "Sidecar synth": {
        "fn": "sidecar_synth.rle",
        "w": 65,
        "h": 14
    },
    "p71 glider loop": {
        "fn": "p71gliderloop.rle",
        "w": 93,
        "h": 93
    },
    "74P8H2V0": {
        "fn": "74p8h2v0.rle",
        "w": 15,
        "h": 21
    },
    "Mold on pentadecathlon synth": {
        "fn": "moldonpentadecathlon_synth.rle",
        "w": 37,
        "h": 19
    },
    "Unidimensional tumbler": {
        "fn": "unidimensionaltumbler.rle",
        "w": 36,
        "h": 1
    },
    "bargesiameseloaf": {
        "fn": "bargesiameseloaf.rle",
        "w": 5,
        "h": 5
    },
    "Mosquito 3": {
        "fn": "mosquito3.rle",
        "w": 2754,
        "h": 650
    },
    "verylonghookwithtail": {
        "fn": "verylonghookwithtail.rle",
        "w": 7,
        "h": 4
    },
    "tnosedp6 synth": {
        "fn": "tnosedp6_synth.rle",
        "w": 287,
        "h": 125
    },
    "verylongbarge synth": {
        "fn": "verylongbarge_synth.rle",
        "w": 71,
        "h": 10
    },
    "lwssgliderinjection": {
        "fn": "lwssgliderinjection.rle",
        "w": 382,
        "h": 374
    },
    "boatlinetub": {
        "fn": "boatlinetub.rle",
        "w": 7,
        "h": 7
    },
    "32-1": {
        "fn": "inflected30greatsym_synth.rle",
        "w": 128,
        "h": 46
    },
    "cisboatandcap": {
        "fn": "cisboatandcap.rle",
        "w": 7,
        "h": 4
    },
    "bouncerr1": {
        "fn": "bouncerr1.rle",
        "w": 27,
        "h": 46
    },
    "cisboatontable": {
        "fn": "cisboatontable.rle",
        "w": 6,
        "h": 4
    },
    "heavyweightvolcanoeppstein": {
        "fn": "heavyweightvolcanoeppstein.rle",
        "w": 25,
        "h": 22
    },
    "Pi-heptomino synth": {
        "fn": "piheptomino_synth.rle",
        "w": 147,
        "h": 68
    },
    "Voldiag": {
        "fn": "voldiag.rle",
        "w": 26,
        "h": 26
    },
    "Prime quadruplet calculator": {
        "fn": "primequadrupletcalculator.rle",
        "w": 460,
        "h": 294
    },
    "Bakery synth": {
        "fn": "bakery_synth.rle",
        "w": 151,
        "h": 104
    },
    "Interchange": {
        "fn": "interchange.rle",
        "w": 14,
        "h": 7
    },
    "doubleclaw": {
        "fn": "doubleclaw.rle",
        "w": 6,
        "h": 6
    },
    "3-engine Cordership eater": {
        "fn": "3enginecordershipeater.rle",
        "w": 89,
        "h": 97
    },
    "112P51": {
        "fn": "112p51.rle",
        "w": 37,
        "h": 37
    },
    "camelship synth": {
        "fn": "camelship_synth.rle",
        "w": 3994323,
        "h": 3677052
    },
    "Maze 2-cell still lifes": {
        "fn": "maze2cellstilllifes.rle",
        "w": 5,
        "h": 2
    },
    "presidewalk": {
        "fn": "presidewalk.rle",
        "w": 7,
        "h": 4
    },
    "pentoad2 synth": {
        "fn": "pentoad2_synth.rle",
        "w": 62,
        "h": 39
    },
    "eaterheadsiameseeatertail": {
        "fn": "eaterheadsiameseeatertail.rle",
        "w": 7,
        "h": 4
    },
    "Moving sawtooth": {
        "fn": "movingsawtooth.rle",
        "w": 128,
        "h": 173
    },
    "p66 glider shuttle": {
        "fn": "p66glidershuttle.rle",
        "w": 25,
        "h": 21
    },
    "HWSS glider collision sample": {
        "fn": "hwssglidercollisionsample.rle",
        "w": 90,
        "h": 38
    },
    "Octagon 2": {
        "fn": "octagon2.rle",
        "w": 8,
        "h": 8
    },
    "94P27.1": {
        "fn": "94p271.rle",
        "w": 32,
        "h": 18
    },
    "Switch engine ping-pong": {
        "fn": "switchenginepingpong.rle",
        "w": 210515,
        "h": 183739
    },
    "tnp4v": {
        "fn": "tnp4v.rle",
        "w": 17,
        "h": 12
    },
    "Unix": {
        "fn": "unix.rle",
        "w": 8,
        "h": 8
    },
    "Integral with long hook": {
        "fn": "integralwithlonghook.rle",
        "w": 7,
        "h": 5
    },
    "34p14 shuttle": {
        "fn": "34p14_shuttle.rle",
        "w": 15,
        "h": 11
    },
    "2x2 3-cell still lifes": {
        "fn": "2x23cellstilllifes.rle",
        "w": 3,
        "h": 3
    },
    "Cross": {
        "fn": "cross.rle",
        "w": 8,
        "h": 8
    },
    "Achim's other p16": {
        "fn": "achimsotherp16.rle",
        "w": 35,
        "h": 35
    },
    "Eleven loop synth": {
        "fn": "elevenloop_synth.rle",
        "w": 73,
        "h": 123
    },
    "Hivenudger": {
        "fn": "hivenudger.rle",
        "w": 13,
        "h": 13
    },
    "Eater 1 with pre-beehive": {
        "fn": "eater1withprebeehive.rle",
        "w": 7,
        "h": 7
    },
    "15bentpaperclip": {
        "fn": "15bentpaperclip.rle",
        "w": 7,
        "h": 5
    },
    "58P5H1V1": {
        "fn": "58p5h1v1.rle",
        "w": 23,
        "h": 23
    },
    "original gliders by the dozen": {
        "fn": "originalglidersbythedozen.rle",
        "w": 9,
        "h": 5
    },
    "Mersenne prime calculator": {
        "fn": "mersenneprimecalculator.rle",
        "w": 893,
        "h": 590
    },
    "Boat on quadpole synth": {
        "fn": "boatonquadpole_synth.rle",
        "w": 121,
        "h": 109
    },
    "p8bumper": {
        "fn": "p8bumper.rle",
        "w": 63,
        "h": 57
    },
    "Y-pentomino": {
        "fn": "ypentomino.rle",
        "w": 4,
        "h": 2
    },
    "beluchenkosp51": {
        "fn": "beluchenkosp51.rle",
        "w": 31,
        "h": 31
    },
    "Pentoad 2": {
        "fn": "pentoad2.rle",
        "w": 12,
        "h": 17
    },
    "Wing": {
        "fn": "wing.rle",
        "w": 4,
        "h": 4
    },
    "p5bouncer": {
        "fn": "p5bouncer.rle",
        "w": 46,
        "h": 48
    },
    "p52overunity": {
        "fn": "p52overunity.rle",
        "w": 414,
        "h": 400
    },
    "p68 glider loop": {
        "fn": "p68gliderloop.rle",
        "w": 56,
        "h": 56
    },
    "cisbargewithnine": {
        "fn": "cisbargewithnine.rle",
        "w": 6,
        "h": 7
    },
    "Eater on boat synth": {
        "fn": "eateronboat_synth.rle",
        "w": 143,
        "h": 100
    },
    "eatertailsiameselongsnake": {
        "fn": "eatertailsiameselongsnake.rle",
        "w": 4,
        "h": 8
    },
    "Blocked p4-3": {
        "fn": "blockedp43.rle",
        "w": 21,
        "h": 9
    },
    "Pipsquirter 1 as reflector": {
        "fn": "pipsquirter1asreflector.rle",
        "w": 22,
        "h": 17
    },
    "P-pentomino": {
        "fn": "ppentomino.rle",
        "w": 2,
        "h": 3
    },
    "Beehive": {
        "fn": "beehive.rle",
        "w": 4,
        "h": 3
    },
    "Block and cap synth": {
        "fn": "blockandcap_synth.rle",
        "w": 107,
        "h": 113
    },
    "griddleandblock synth": {
        "fn": "griddleandblock_synth.rle",
        "w": 20,
        "h": 24
    },
    "p57 glider loop": {
        "fn": "p57gliderloop.rle",
        "w": 79,
        "h": 79
    },
    "114p84": {
        "fn": "114p84.rle",
        "w": 23,
        "h": 26
    },
    "sawtooth181": {
        "fn": "sawtooth181.rle",
        "w": 78,
        "h": 87
    },
    "Acorn": {
        "fn": "acorn.rle",
        "w": 7,
        "h": 3
    },
    "extraglider": {
        "fn": "extraglider.rle",
        "w": 84,
        "h": 160
    },
    "Pressure cooker": {
        "fn": "pressurecooker.rle",
        "w": 11,
        "h": 12
    },
    "Period-184 glider gun synthesis": {
        "fn": "p184gun_synth.rle",
        "w": 268,
        "h": 38
    },
    "p60 hassler": {
        "fn": "p60hassler.rle",
        "w": 34,
        "h": 23
    },
    "prepulsarshuttle60": {
        "fn": "prepulsarshuttle60.rle",
        "w": 21,
        "h": 29
    },
    "65P13.1": {
        "fn": "65p131.rle",
        "w": 25,
        "h": 15
    },
    "Frothing puffer 2": {
        "fn": "frothingpuffer2.rle",
        "w": 249,
        "h": 371
    },
    "clock2 synth": {
        "fn": "clock2_synth.rle",
        "w": 1456,
        "h": 578
    },
    "New five": {
        "fn": "newfive.rle",
        "w": 9,
        "h": 9
    },
    "48P22": {
        "fn": "48p22.rle",
        "w": 16,
        "h": 16
    },
    "Unidimensional six gliders": {
        "fn": "unidimensionalsixgliders.rle",
        "w": 15,
        "h": 1
    },
    "long4ship": {
        "fn": "long4ship.rle",
        "w": 7,
        "h": 7
    },
    "p46gunexample": {
        "fn": "p46gunexample.rle",
        "w": 35,
        "h": 38
    },
    "Bent keys synth": {
        "fn": "bentkeys_synth.rle",
        "w": 57,
        "h": 27
    },
    "Unidimensional pentadecathlon": {
        "fn": "unidimensionalpentadecathlon.rle",
        "w": 10,
        "h": 1
    },
    "Twin bees shuttles hassling blinker": {
        "fn": "twinbeesshuttleshasslingblinker.rle",
        "w": 68,
        "h": 11
    },
    "29P9 synth": {
        "fn": "29p9_synth.rle",
        "w": 292,
        "h": 42
    },
    "2x2 6-cell still lifes": {
        "fn": "2x26cellstilllifes.rle",
        "w": 58,
        "h": 6
    },
    "Swan": {
        "fn": "swan.rle",
        "w": 24,
        "h": 12
    },
    "26P40": {
        "fn": "26p40.rle",
        "w": 17,
        "h": 14
    },
    "p6 diagonal wickstretcher 1": {
        "fn": "p6diagonalwickstretcher1.rle",
        "w": 61,
        "h": 61
    },
    "48P20": {
        "fn": "48p20.rle",
        "w": 15,
        "h": 15
    },
    "Puffer 1": {
        "fn": "puffer1.rle",
        "w": 27,
        "h": 7
    },
    "Gliders by the dozen synth": {
        "fn": "glidersbythedozen_synth.rle",
        "w": 110,
        "h": 15
    },
    "loafbacktieloaf synth": {
        "fn": "loafbacktieloaf_synth.rle",
        "w": 10,
        "h": 10
    },
    "p60trafficlighthassler": {
        "fn": "p60trafficlighthassler.rle",
        "w": 36,
        "h": 40
    },
    "Pi-heptomino": {
        "fn": "piheptomino.rle",
        "w": 3,
        "h": 3
    },
    "Wing (spaceship)": {
        "fn": "wingspaceship.rle",
        "w": 23,
        "h": 23
    },
    "Cis-boat and table": {
        "fn": "cisboatandtable.rle",
        "w": 4,
        "h": 6
    },
    "carrierwithfeather synth": {
        "fn": "carrierwithfeather_synth.rle",
        "w": 52,
        "h": 12
    },
    "Original P56 B-heptomino shuttle": {
        "fn": "originalp56bheptominoshuttle.rle",
        "w": 40,
        "h": 21
    },
    "65p131 synth": {
        "fn": "65p131_synth.rle",
        "w": 1634,
        "h": 294
    },
    "multipleroteightors": {
        "fn": "multipleroteightors.rle",
        "w": 32,
        "h": 32
    },
    "30P5H2V0 Synthesis": {
        "fn": "30p5h2v0_synth.rle",
        "w": 1386,
        "h": 338
    },
    "Sidecar": {
        "fn": "sidecar.rle",
        "w": 8,
        "h": 10
    },
    "fx77extraglider": {
        "fn": "fx77extraglider.rle",
        "w": 194,
        "h": 87
    },
    "Unidimensional four gliders": {
        "fn": "unidimensionalfourgliders.rle",
        "w": 56,
        "h": 1
    },
    "P48 toad hassler": {
        "fn": "p48toadhassler.rle",
        "w": 36,
        "h": 54
    },
    "Beacon on 38P11.1": {
        "fn": "beaconon38p11.1.rle",
        "w": 12,
        "h": 14
    },
    "Beehive with nine": {
        "fn": "beehivewithnine.rle",
        "w": 7,
        "h": 6
    },
    "Brain perturbing glider": {
        "fn": "brainperturbingglider.rle",
        "w": 22,
        "h": 14
    },
    "168P22.1": {
        "fn": "168p22.1.rle",
        "w": 33,
        "h": 23
    },
    "prepulsarshuttle30": {
        "fn": "prepulsarshuttle30.rle",
        "w": 18,
        "h": 11
    },
    "Small lake synth": {
        "fn": "smalllake_synth.rle",
        "w": 84,
        "h": 54
    },
    "Bookends synth": {
        "fn": "bookends_synth.rle",
        "w": 126,
        "h": 91
    },
    "6-engine Cordership v2": {
        "fn": "6enginecordershipv2.rle",
        "w": 77,
        "h": 74
    },
    "Eater plug": {
        "fn": "eaterplug.rle",
        "w": 8,
        "h": 8
    },
    "againstthegrain": {
        "fn": "againstthegrain.rle",
        "w": 35,
        "h": 52
    },
    "extensible28p7.3": {
        "fn": "extensible28p7.3.rle",
        "w": 15,
        "h": 98
    },
    "Bakery": {
        "fn": "bakery.rle",
        "w": 10,
        "h": 10
    },
    "AK47 reaction": {
        "fn": "ak47reaction.rle",
        "w": 10,
        "h": 12
    },
    "HighLife 12-cell still lifes": {
        "fn": "highlife12cellstilllifes.rle",
        "w": 259,
        "h": 71
    },
    "Blinkers bit pole": {
        "fn": "blinkersbitpole.rle",
        "w": 7,
        "h": 6
    },
    "Duodecapole": {
        "fn": "duodecapole.rle",
        "w": 15,
        "h": 15
    },
    "Long shillelagh": {
        "fn": "longshillelagh.rle",
        "w": 6,
        "h": 3
    },
    "Scrubber": {
        "fn": "scrubber.rle",
        "w": 11,
        "h": 11
    },
    "Butterfly synth": {
        "fn": "butterfly_synth.rle",
        "w": 151,
        "h": 75
    },
    "pseudop6": {
        "fn": "pseudop6.rle",
        "w": 27,
        "h": 18
    },
    "Light speed oscillator 3": {
        "fn": "lightspeedoscillator3.rle",
        "w": 73,
        "h": 73
    },
    "cpsemicenark": {
        "fn": "cpsemicenark.rle",
        "w": 33,
        "h": 32
    },
    "Lobster (spaceship)": {
        "fn": "83p7h1v1.rle",
        "w": 26,
        "h": 26
    },
    "almostgun1": {
        "fn": "almostgun1.rle",
        "w": 87,
        "h": 13
    },
    "vsparkblocktoglider": {
        "fn": "vsparkblocktoglider.rle",
        "w": 5,
        "h": 4
    },
    "55p10": {
        "fn": "55p10.rle",
        "w": 21,
        "h": 9
    },
    "heavyweightvolcanohickerson": {
        "fn": "heavyweightvolcanohickerson.rle",
        "w": 36,
        "h": 19
    },
    "Quattuordecapole": {
        "fn": "quattuordecapole.rle",
        "w": 17,
        "h": 17
    },
    "Achim's p11": {
        "fn": "achimsp11.rle",
        "w": 22,
        "h": 22
    },
    "Original p44 pi-heptomino hassler": {
        "fn": "originalp44piheptominohassler.rle",
        "w": 31,
        "h": 44
    },
    "lightspeedbubble1": {
        "fn": "lightspeedbubble1.rle",
        "w": 62,
        "h": 37
    },
    "BF20H": {
        "fn": "bf20h.rle",
        "w": 51,
        "h": 60
    },
    "Mosquito 4": {
        "fn": "mosquito4.rle",
        "w": 2754,
        "h": 650
    },
    "Boat with long tail": {
        "fn": "boatwithlongtail.rle",
        "w": 6,
        "h": 4
    },
    "htog0stamp": {
        "fn": "htog0stamp.rle",
        "w": 208,
        "h": 96
    },
    "6-engine Cordership synth": {
        "fn": "6enginecordership_synth.rle",
        "w": 301,
        "h": 293
    },
    "telegraphfuse": {
        "fn": "telegraphfuse.rle",
        "w": 1290,
        "h": 10
    },
    "Maze 4-cell still lifes": {
        "fn": "maze4cellstilllifes.rle",
        "w": 23,
        "h": 4
    },
    "Maze 7-cell still lifes": {
        "fn": "maze7cellstilllifes.rle",
        "w": 134,
        "h": 15
    },
    "Blinker ship 1": {
        "fn": "blinkership1.rle",
        "w": 27,
        "h": 15
    },
    "sixls synth": {
        "fn": "sixls_synth.rle",
        "w": 126,
        "h": 61
    },
    "p337gun": {
        "fn": "p337gun.rle",
        "w": 80,
        "h": 97
    },
    "Boat-ship-tie": {
        "fn": "boatshiptie.rle",
        "w": 6,
        "h": 6
    },
    "long4shillelagh": {
        "fn": "long4shillelagh.rle",
        "w": 6,
        "h": 9
    },
    "Long snake": {
        "fn": "longsnake.rle",
        "w": 5,
        "h": 3
    },
    "hookwithtailhooksiamesesnake": {
        "fn": "hookwithtailhooksiamesesnake.rle",
        "w": 4,
        "h": 8
    },
    "One per generation": {
        "fn": "onepergeneration.rle",
        "w": 17,
        "h": 15
    },
    "Sawtooth 1163": {
        "fn": "sawtooth1163.rle",
        "w": 150,
        "h": 219
    },
    "Log(t)^2 growth": {
        "fn": "logt2growth.rle",
        "w": 290,
        "h": 218
    },
    "112P51 extended": {
        "fn": "112p51extended.rle",
        "w": 64,
        "h": 64
    },
    "Biting off more than they can chew synth": {
        "fn": "bitingoffmorethantheycanchew_synth.rle",
        "w": 122,
        "h": 108
    },
    "Extra extra long snake synth": {
        "fn": "extraextralongsnake_synth.rle",
        "w": 70,
        "h": 73
    },
    "tlog(t) growth": {
        "fn": "tlogtgrowth.rle",
        "w": 635,
        "h": 377
    },
    "Tripole": {
        "fn": "tripole.rle",
        "w": 6,
        "h": 6
    },
    "Beehive with tail synth": {
        "fn": "beehivewithtail_synth.rle",
        "w": 77,
        "h": 76
    },
    "Killer candlefrobras": {
        "fn": "killercandlefrobras.rle",
        "w": 21,
        "h": 5
    },
    "26-cell quadratic growth": {
        "fn": "26cellquadraticgrowth.rle",
        "w": 16193,
        "h": 15089
    },
    "60p33 synth": {
        "fn": "60p33_synth.rle",
        "w": 156,
        "h": 36
    },
    "P40 B-heptomino shuttle": {
        "fn": "p40bheptominoshuttle.rle",
        "w": 26,
        "h": 23
    },
    "Trans-block and long hook eating tub": {
        "fn": "transblockandlonghookeatingtub.rle",
        "w": 10,
        "h": 5
    },
    "p64tbh": {
        "fn": "p64tbh.rle",
        "w": 15,
        "h": 27
    },
    "Hook": {
        "fn": "hook.rle",
        "w": 3,
        "h": 2
    },
    "Pond on pond synth": {
        "fn": "pondonpond_synth.rle",
        "w": 105,
        "h": 70
    },
    "40514m": {
        "fn": "40514m.rle",
        "w": 78,
        "h": 54
    },
    "Turtle with tagalong": {
        "fn": "turtlewithtagalong.rle",
        "w": 70,
        "h": 11
    },
    "togglecircuit": {
        "fn": "togglecircuit.rle",
        "w": 73,
        "h": 23
    },
    "p50 glider gun variant by Dean Hickerson": {
        "fn": "period50glidergun.rle",
        "w": 144,
        "h": 46
    },
    "multuminparvo synth": {
        "fn": "multuminparvo_synth.rle",
        "w": 10,
        "h": 7
    },
    "Eater 1 with glider": {
        "fn": "eater1withglider.rle",
        "w": 8,
        "h": 7
    },
    "Coe's p8": {
        "fn": "coesp8.rle",
        "w": 12,
        "h": 6
    },
    "pseudobarberpole synth": {
        "fn": "pseudobarberpole_synth.rle",
        "w": 106,
        "h": 27
    },
    "Blockstacker": {
        "fn": "blockstacker.rle",
        "w": 531,
        "h": 915
    },
    "Block on boat synth": {
        "fn": "blockonboat_synth.rle",
        "w": 144,
        "h": 94
    },
    "Trans-beacon and dock synth": {
        "fn": "transbeaconanddock_synth.rle",
        "w": 120,
        "h": 53
    },
    "longsnakesiameselongsnake": {
        "fn": "longsnakesiameselongsnake.rle",
        "w": 4,
        "h": 9
    },
    "branchingspaceship": {
        "fn": "branchingspaceship.rle",
        "w": 62,
        "h": 52
    },
    "beluchenkosp37": {
        "fn": "beluchenkosp37.rle",
        "w": 37,
        "h": 37
    },
    "Boat on snake": {
        "fn": "boatonsnake.rle",
        "w": 7,
        "h": 5
    },
    "Twin bees shuttle": {
        "fn": "twinbeesshuttle.rle",
        "w": 29,
        "h": 11
    },
    "88p25": {
        "fn": "88p25.rle",
        "w": 32,
        "h": 23
    },
    "8-engine Cordership": {
        "fn": "8enginecordership.rle",
        "w": 83,
        "h": 83
    },
    "demultiplexer": {
        "fn": "demultiplexer.rle",
        "w": 49,
        "h": 43
    },
    "126P3H1V0": {
        "fn": "126p3h1v0.rle",
        "w": 17,
        "h": 33
    },
    "Fire-spitting": {
        "fn": "firespitting.rle",
        "w": 8,
        "h": 10
    },
    "tubwithtwodowntranstails": {
        "fn": "tubwithtwodowntranstails.rle",
        "w": 7,
        "h": 7
    },
    "Cousin prime calculator": {
        "fn": "cousinprimecalculator.rle",
        "w": 439,
        "h": 296
    },
    "Bipole synth": {
        "fn": "bipole_synth.rle",
        "w": 157,
        "h": 49
    },
    "B-52 bomber": {
        "fn": "b52bomber.rle",
        "w": 39,
        "h": 21
    },
    "Cis-shillelagh synth": {
        "fn": "cisshillelagh_synth.rle",
        "w": 95,
        "h": 133
    },
    "Hook test tube baby": {
        "fn": "hooktesttubebaby.rle",
        "w": 10,
        "h": 5
    },
    "Fast forward force field": {
        "fn": "fastforwardforcefield.rle",
        "w": 17,
        "h": 7
    },
    "35201M": {
        "fn": "35201m.rle",
        "w": 20,
        "h": 20
    },
    "loafer synth": {
        "fn": "loafer_synth.rle",
        "w": 34,
        "h": 31
    },
    "112P15": {
        "fn": "112p15.rle",
        "w": 25,
        "h": 25
    },
    "loafer": {
        "fn": "loafer.rle",
        "w": 9,
        "h": 9
    },
    "V gun": {
        "fn": "vgun.rle",
        "w": 2325,
        "h": 1257
    },
    "92p156 synth": {
        "fn": "92p156_synth.rle",
        "w": 271,
        "h": 266
    },
    "minipressurecooker synth": {
        "fn": "minipressurecooker_synth.rle",
        "w": 448,
        "h": 150
    },
    "twotransgriddleswithtwotubs synth": {
        "fn": "twotransgriddleswithtwotubs_synth.rle",
        "w": 63,
        "h": 30
    },
    "Barge synth": {
        "fn": "barge_synth.rle",
        "w": 147,
        "h": 86
    },
    "p52emu4": {
        "fn": "p52emu4.rle",
        "w": 394,
        "h": 394
    },
    "Halfmax v3": {
        "fn": "halfmaxv3.rle",
        "w": 111,
        "h": 84
    },
    "Boat on spark coil synth": {
        "fn": "boatonsparkcoil_synth.rle",
        "w": 73,
        "h": 87
    },
    "487-tick reflector": {
        "fn": "slr487.rle",
        "w": 134,
        "h": 95
    },
    "Dragon tagalongs": {
        "fn": "dragontagalongs.rle",
        "w": 70,
        "h": 36
    },
    "Caterer": {
        "fn": "caterer.rle",
        "w": 8,
        "h": 6
    },
    "Blocked p4-5": {
        "fn": "blockedp45.rle",
        "w": 18,
        "h": 12
    },
    "Karel's p15 synth": {
        "fn": "karelsp15_synth.rle",
        "w": 64,
        "h": 33
    },
    "Loaf tie eater-with-tail": {
        "fn": "loaftieeaterwithtail.rle",
        "w": 8,
        "h": 7
    },
    "Bi-clock": {
        "fn": "biclock.rle",
        "w": 7,
        "h": 7
    },
    "104P177 reactions": {
        "fn": "104p177reactions.rle",
        "w": 377,
        "h": 173
    },
    "1-2-3-4": {
        "fn": "1234.rle",
        "w": 11,
        "h": 11
    },
    "P200 traffic jam": {
        "fn": "p200trafficjam.rle",
        "w": 77,
        "h": 77
    },
    "p12cloverleafhassler": {
        "fn": "p12cloverleafhassler.rle",
        "w": 49,
        "h": 49
    },
    "pipsquirter3": {
        "fn": "pipsquirter3.rle",
        "w": 15,
        "h": 13
    },
    "Dead spark coil": {
        "fn": "deadsparkcoil.rle",
        "w": 7,
        "h": 5
    },
    "Mango synth": {
        "fn": "mango_synth.rle",
        "w": 157,
        "h": 106
    },
    "p52pseudo": {
        "fn": "p52pseudo.rle",
        "w": 69,
        "h": 68
    },
    "integralwithtubandtail": {
        "fn": "integralwithtubandtail.rle",
        "w": 6,
        "h": 7
    },
    "Two trans griddles with two tubs": {
        "fn": "twotransgriddleswtwotubs.rle",
        "w": 8,
        "h": 7
    },
    "long4boat": {
        "fn": "long4boat.rle",
        "w": 7,
        "h": 7
    },
    "5x5 infinite growth": {
        "fn": "5x5infinitegrowth.rle",
        "w": 5,
        "h": 5
    },
    "Trans-barge with tail": {
        "fn": "transbargewithtail.rle",
        "w": 6,
        "h": 6
    },
    "p63 glider loop": {
        "fn": "p63gliderloop.rle",
        "w": 85,
        "h": 85
    },
    "p53 glider loop": {
        "fn": "p53gliderloop.rle",
        "w": 75,
        "h": 75
    },
    "Shuriken": {
        "fn": "shuriken.rle",
        "w": 7,
        "h": 7
    },
    "Pulsar quadrant synth": {
        "fn": "pulsarquadrant_synth.rle",
        "w": 78,
        "h": 25
    },
    "Cis-mirrored R-bee": {
        "fn": "cismirroredrbee.rle",
        "w": 7,
        "h": 4
    },
    "herschelstopper": {
        "fn": "herschelstopper.rle",
        "w": 94,
        "h": 58
    },
    "briansbrainp3": {
        "fn": "briansbrainp3.rle",
        "w": 4,
        "h": 4
    },
    "glidertoblock": {
        "fn": "glidertoblock.rle",
        "w": 24,
        "h": 31
    },
    "P26 glider shuttle": {
        "fn": "p26glidershuttle.rle",
        "w": 53,
        "h": 53
    },
    "lightweightemulator synth": {
        "fn": "lightweightemulator_synth.rle",
        "w": 356,
        "h": 180
    },
    "Pseudo-barberpole on 36p22": {
        "fn": "pseudobarberpoleon36p22.rle",
        "w": 35,
        "h": 14
    },
    "Two cis griddles with two tubs": {
        "fn": "twocisgriddleswtwotubs.rle",
        "w": 8,
        "h": 7
    },
    "88p28 synth": {
        "fn": "88p28_synth.rle",
        "w": 114,
        "h": 114
    },
    "p11 double-length signal injector": {
        "fn": "p11doublelengthsignalinjector.rle",
        "w": 51,
        "h": 46
    },
    "longclawwithtail synth": {
        "fn": "longclawwithtail_synth.rle",
        "w": 31,
        "h": 13
    },
    "fusewithtwolongtails": {
        "fn": "fusewithtwolongtails.rle",
        "w": 4,
        "h": 8
    },
    "knightshippartial": {
        "fn": "knightshippartial.rle",
        "w": 26,
        "h": 31
    },
    "2x2 7-cell still lifes": {
        "fn": "2x27cellstilllifes.rle",
        "w": 36,
        "h": 15
    },
    "ciscarriertiesnake": {
        "fn": "ciscarriertiesnake.rle",
        "w": 8,
        "h": 5
    },
    "Onion rings": {
        "fn": "onionrings.rle",
        "w": 96,
        "h": 96
    },
    "Turning toads": {
        "fn": "turningtoads.rle",
        "w": 37,
        "h": 8
    },
    "sw1t43": {
        "fn": "sw1t43.rle",
        "w": 17,
        "h": 18
    },
    "x66 synth": {
        "fn": "x66_synth.rle",
        "w": 27,
        "h": 79
    },
    "Tritoad": {
        "fn": "tritoad.rle",
        "w": 18,
        "h": 14
    },
    "eater2onetimeswitch": {
        "fn": "eater2onetimeswitch.rle",
        "w": 81,
        "h": 62
    },
    "B29 with gliders": {
        "fn": "b29withgliders.rle",
        "w": 54,
        "h": 16
    },
    "Hustler synth": {
        "fn": "hustler_synth.rle",
        "w": 182,
        "h": 142
    },
    "Moose antlers": {
        "fn": "mooseantlers.rle",
        "w": 9,
        "h": 5
    },
    "Bee hat synth": {
        "fn": "beehat_synth.rle",
        "w": 31,
        "h": 12
    },
    "rotatedtable": {
        "fn": "rotatedtable.rle",
        "w": 6,
        "h": 5
    },
    "24-cell quadratic growth": {
        "fn": "24cellquadraticgrowth.rle",
        "w": 39786,
        "h": 143
    },
    "Python siamese snake": {
        "fn": "pythonsiamesesnake.rle",
        "w": 3,
        "h": 8
    },
    "Longhorn": {
        "fn": "longhorn.rle",
        "w": 9,
        "h": 6
    },
    "Alternate pi orbital": {
        "fn": "alternatepiorbital.rle",
        "w": 63,
        "h": 61
    },
    "440P49.1": {
        "fn": "440p49.1.rle",
        "w": 65,
        "h": 65
    },
    "Period 7 and 8 oscillators": {
        "fn": "period7and8oscillators.rle",
        "w": 491,
        "h": 161
    },
    "Swine": {
        "fn": "swine.rle",
        "w": 37,
        "h": 10
    },
    "Eater plug synth": {
        "fn": "eaterplug_synth.rle",
        "w": 118,
        "h": 67
    },
    "Lx200": {
        "fn": "lx200.rle",
        "w": 36,
        "h": 61
    },
    "68p9": {
        "fn": "68p9.rle",
        "w": 16,
        "h": 17
    },
    "Ship on long boat": {
        "fn": "shiponlongboat.rle",
        "w": 7,
        "h": 7
    },
    "componentexample": {
        "fn": "componentexample.rle",
        "w": 48,
        "h": 18
    },
    "Long barge": {
        "fn": "longbarge.rle",
        "w": 5,
        "h": 5
    },
    "28p7.1 synth": {
        "fn": "28p7.1_synth.rle",
        "w": 711,
        "h": 238
    },
    "Tail": {
        "fn": "tail.rle",
        "w": 2,
        "h": 3
    },
    "Two eaters": {
        "fn": "twoeaters.rle",
        "w": 9,
        "h": 8
    },
    "Bookend siamese table": {
        "fn": "bookendsiamesetable.rle",
        "w": 7,
        "h": 3
    },
    "Skewed pre-pulsar": {
        "fn": "skewedprepulsar.rle",
        "w": 9,
        "h": 5
    },
    "Block and two tails synth": {
        "fn": "blockandtwotails_synth.rle",
        "w": 107,
        "h": 81
    },
    "Spider": {
        "fn": "spider.rle",
        "w": 27,
        "h": 8
    },
    "verylongship": {
        "fn": "verylongship.rle",
        "w": 5,
        "h": 5
    },
    "Pufferfish breeder": {
        "fn": "pufferfishbreeder.rle",
        "w": 495,
        "h": 428
    },
    "Mango": {
        "fn": "mango.rle",
        "w": 5,
        "h": 4
    },
    "Period 6 oscillators": {
        "fn": "period6oscillators.rle",
        "w": 375,
        "h": 218
    },
    "bumperreactions": {
        "fn": "bumperreactions.rle",
        "w": 20,
        "h": 44
    },
    "Teardrop synth": {
        "fn": "teardrop_synth.rle",
        "w": 108,
        "h": 49
    },
    "Gosper glider gun synth": {
        "fn": "gosperglidergun_synth.rle",
        "w": 47,
        "h": 14
    },
    "R2D2": {
        "fn": "r2d2.rle",
        "w": 11,
        "h": 10
    },
    "hatsiamesevase": {
        "fn": "hatsiamesevase.rle",
        "w": 9,
        "h": 9
    },
    "integralwithcishook": {
        "fn": "integralwithcishook.rle",
        "w": 7,
        "h": 5
    },
    "c/2 wickstretcher": {
        "fn": "c2wickstretcher.rle",
        "w": 43,
        "h": 61
    },
    "New gun 2": {
        "fn": "newgun2.rle",
        "w": 51,
        "h": 24
    },
    "twinbeesshuttlepairp46gun": {
        "fn": "twinbeesshuttlepairp46gun.rle",
        "w": 49,
        "h": 14
    },
    "gardenofeden11": {
        "fn": "gardenofeden11.rle",
        "w": 11,
        "h": 9
    },
    "1beacon synth": {
        "fn": "1beacon_synth.rle",
        "w": 72,
        "h": 17
    },
    "Racetrack and ortho-beacon": {
        "fn": "racetrackandorthobeacon.rle",
        "w": 7,
        "h": 9
    },
    "thumb1 synth": {
        "fn": "thumb1_synth.rle",
        "w": 631,
        "h": 49
    },
    "Trans-boat with tail": {
        "fn": "transboatwithtail.rle",
        "w": 5,
        "h": 5
    },
    "Claw with tail": {
        "fn": "clawwithtail.rle",
        "w": 6,
        "h": 5
    },
    "Maximum volatility gun": {
        "fn": "maximumvolatilitygun.rle",
        "w": 1087,
        "h": 1
    },
    "prepreblockmakesmwss": {
        "fn": "prepreblockmakesmwss.rle",
        "w": 22,
        "h": 18
    },
    "p60trafficlighthassler synth": {
        "fn": "p60trafficlighthassler_synth.rle",
        "w": 837,
        "h": 48
    },
    "Stairstep hexomino": {
        "fn": "stairstephexomino.rle",
        "w": 4,
        "h": 3
    },
    "Still lifes": {
        "fn": "stilllifes.rle",
        "w": 108,
        "h": 76
    },
    "Tlog(log(t)) growth": {
        "fn": "tloglogtgrowth.rle",
        "w": 1581,
        "h": 373
    },
    "Snacker synth": {
        "fn": "snacker_synth.rle",
        "w": 150,
        "h": 137
    },
    "p435gun": {
        "fn": "p435gun.rle",
        "w": 112,
        "h": 68
    },
    "p32 blinker hassler 2": {
        "fn": "p32blinkerhassler2.rle",
        "w": 30,
        "h": 24
    },
    "Quadruple burloaferimeter": {
        "fn": "quadrupleburloaferimeter.rle",
        "w": 16,
        "h": 16
    },
    "Fx119 inserter": {
        "fn": "fx119inserter.rle",
        "w": 30,
        "h": 24
    },
    "longboatonboat": {
        "fn": "longboatonboat.rle",
        "w": 7,
        "h": 7
    },
    "P110 traffic jam": {
        "fn": "p110trafficjam.rle",
        "w": 111,
        "h": 72
    },
    "p56bheptominoshuttle synth": {
        "fn": "p56bheptominoshuttle_synth.rle",
        "w": 104,
        "h": 36
    },
    "p49bumperloopnew": {
        "fn": "p49bumperloopnew.rle",
        "w": 46,
        "h": 46
    },
    "Middleweight emulator synth": {
        "fn": "middleweightemulator_synth.rle",
        "w": 127,
        "h": 54
    },
    "15gsynthp46": {
        "fn": "15gsynthp46.rle",
        "w": 125,
        "h": 89
    },
    "14p2.3 synth": {
        "fn": "14p2.3_synth.rle",
        "w": 116,
        "h": 20
    },
    "twinbeesshuttle synth": {
        "fn": "twinbeesshuttle_synth.rle",
        "w": 111,
        "h": 79
    },
    "56P6H1V0": {
        "fn": "56p6h1v0.rle",
        "w": 26,
        "h": 12
    },
    "T-nosed p4 on 48P31": {
        "fn": "tnosedp4on48p31.rle",
        "w": 24,
        "h": 30
    },
    "Anvil": {
        "fn": "anvil.rle",
        "w": 7,
        "h": 4
    },
    "Die hard": {
        "fn": "diehard.rle",
        "w": 8,
        "h": 3
    },
    "Cis-beacon and dock synth": {
        "fn": "cisbeaconanddock_synth.rle",
        "w": 108,
        "h": 58
    },
    "Pinwheel": {
        "fn": "pinwheel.rle",
        "w": 12,
        "h": 12
    },
    "4-degree MWSS-to-G": {
        "fn": "45degreemwsstog.rle",
        "w": 28,
        "h": 26
    },
    "hookwithtailtailsiamesesnake": {
        "fn": "hookwithtailtailsiamesesnake.rle",
        "w": 7,
        "h": 5
    },
    "Fuse with tail and long tail": {
        "fn": "fusewithtailandlongtail.rle",
        "w": 7,
        "h": 5
    },
    "lightspeedcrawler": {
        "fn": "lightspeedcrawler.rle",
        "w": 120,
        "h": 6
    },
    "foreandback synth": {
        "fn": "foreandback_synth.rle",
        "w": 30,
        "h": 18
    },
    "centurytogliderconverter1": {
        "fn": "centurytogliderconverter1.rle",
        "w": 17,
        "h": 31
    },
    "Slide gun": {
        "fn": "slidegun.rle",
        "w": 109,
        "h": 59
    },
    "3-engine Cordership rake": {
        "fn": "3enginecordershiprake.rle",
        "w": 461,
        "h": 424
    },
    "Maze period 2": {
        "fn": "mazeperiod2.rle",
        "w": 28,
        "h": 5
    },
    "Snake pit 2": {
        "fn": "snakepit2.rle",
        "w": 11,
        "h": 11
    },
    "verylonghookwithtail synth": {
        "fn": "verylonghookwithtail_synth.rle",
        "w": 108,
        "h": 91
    },
    "Acorn synth": {
        "fn": "acorn_synth.rle",
        "w": 42,
        "h": 7
    },
    "Heavyweight volcano": {
        "fn": "heavyweightvolcano.rle",
        "w": 24,
        "h": 17
    },
    "36P22": {
        "fn": "36p22.rle",
        "w": 27,
        "h": 10
    },
    "cisboatondock": {
        "fn": "cisboatondock.rle",
        "w": 7,
        "h": 6
    },
    "Buckaroo synth": {
        "fn": "buckaroo_synth.rle",
        "w": 107,
        "h": 45
    },
    "newf176": {
        "fn": "newf176.rle",
        "w": 50,
        "h": 32
    },
    "Gunstar 216": {
        "fn": "gunstar216.rle",
        "w": 149,
        "h": 149
    },
    "Trans-block and long hook eating tub synth": {
        "fn": "transblockandlonghookeatingtub_synth.rle",
        "w": 119,
        "h": 45
    },
    "blinkerpull": {
        "fn": "blinkerpull.rle",
        "w": 14,
        "h": 17
    },
    "6-engine Cordership gun": {
        "fn": "6enginecordershipgun.rle",
        "w": 1285,
        "h": 1065
    },
    "3-engine Cordership LWSS reflection": {
        "fn": "3enginecordershiplwssreflection.rle",
        "w": 63,
        "h": 68
    },
    "p5pentaboojum": {
        "fn": "p5pentaboojum.rle",
        "w": 136,
        "h": 149
    },
    "c/3 ladder": {
        "fn": "c3ladder.rle",
        "w": 5,
        "h": 8
    },
    "longtubclawwithtail": {
        "fn": "longtubclawwithtail.rle",
        "w": 8,
        "h": 5
    },
    "88P28": {
        "fn": "88p28.rle",
        "w": 36,
        "h": 36
    },
    "Eater 1": {
        "fn": "eater1.rle",
        "w": 4,
        "h": 4
    },
    "p156gun": {
        "fn": "p156gun.rle",
        "w": 42,
        "h": 42
    },
    "prepulsarshuttle55": {
        "fn": "prepulsarshuttle55.rle",
        "w": 25,
        "h": 43
    },
    "92P156": {
        "fn": "92p156.rle",
        "w": 42,
        "h": 42
    },
    "c/5 diagonal puffer 1": {
        "fn": "c5diagonalpuffer1.rle",
        "w": 85,
        "h": 71
    },
    "simkinsp60": {
        "fn": "simkinsp60.rle",
        "w": 33,
        "h": 31
    },
    "snakeandtable": {
        "fn": "snakeandtable.rle",
        "w": 7,
        "h": 4
    },
    "Welded geese": {
        "fn": "weldedgeese.rle",
        "w": 23,
        "h": 22
    },
    "Tub with extra long tail": {
        "fn": "tubwithextralongtail.rle",
        "w": 4,
        "h": 8
    },
    "twinbeesshuttlesparklwssturn": {
        "fn": "twinbeesshuttlesparklwssturn.rle",
        "w": 13,
        "h": 13
    },
    "28p73 synth": {
        "fn": "28p73_synth.rle",
        "w": 194,
        "h": 50
    },
    "prepulsarshuttle25": {
        "fn": "prepulsarshuttle25.rle",
        "w": 28,
        "h": 27
    },
    "carnivalshuttle2": {
        "fn": "carnivalshuttle2.rle",
        "w": 80,
        "h": 34
    },
    "Mold and long hook eating tub synth": {
        "fn": "moldandlonghookeatingtub_synth.rle",
        "w": 111,
        "h": 52
    },
    "hectic synth": {
        "fn": "hectic_synth.rle",
        "w": 169,
        "h": 73
    },
    "Blinker puffer 1": {
        "fn": "blinkerpuffer1.rle",
        "w": 9,
        "h": 18
    },
    "pulshuttlev": {
        "fn": "pulshuttlev.rle",
        "w": 42,
        "h": 29
    },
    "Pulsar quadrant": {
        "fn": "pulsarquadrant.rle",
        "w": 8,
        "h": 8
    },
    "boatonaircraft synth": {
        "fn": "boatonaircraft_synth.rle",
        "w": 44,
        "h": 45
    },
    "60P5H2V0": {
        "fn": "60p5h2v0.rle",
        "w": 19,
        "h": 11
    },
    "Tanner's p46": {
        "fn": "tannersp46.rle",
        "w": 13,
        "h": 26
    },
    "Glider loop": {
        "fn": "gliderloop.rle",
        "w": 73,
        "h": 150
    },
    "48p31 synth": {
        "fn": "48p31_synth.rle",
        "w": 142,
        "h": 39
    },
    "Pond": {
        "fn": "pond.rle",
        "w": 4,
        "h": 4
    },
    "13loop": {
        "fn": "13loop.rle",
        "w": 6,
        "h": 7
    },
    "Garden of Eden 4": {
        "fn": "gardenofeden4.rle",
        "w": 12,
        "h": 11
    },
    "Bunnies 10": {
        "fn": "bunnies10.rle",
        "w": 6,
        "h": 6
    },
    "Double ewe": {
        "fn": "doubleewe.rle",
        "w": 20,
        "h": 20
    },
    "Jason's p6": {
        "fn": "jasonsp6.rle",
        "w": 10,
        "h": 10
    },
    "BTS": {
        "fn": "bts.rle",
        "w": 7,
        "h": 8
    },
    "Asymmetric pre-pulsar spaceship": {
        "fn": "apps.rle",
        "w": 69,
        "h": 15
    },
    "Beehive synth": {
        "fn": "beehive_synth.rle",
        "w": 156,
        "h": 60
    },
    "phisparkswithgliders": {
        "fn": "phisparkswithgliders.rle",
        "w": 45,
        "h": 18
    },
    "P132 Hans Leo hassler": {
        "fn": "p132hansleohassler.rle",
        "w": 40,
        "h": 39
    },
    "Thunderbird": {
        "fn": "thunderbird.rle",
        "w": 3,
        "h": 5
    },
    "cyclic synth": {
        "fn": "cyclic_synth.rle",
        "w": 60,
        "h": 42
    },
    "Slow puffer 2": {
        "fn": "slowpuffer2.rle",
        "w": 22,
        "h": 21
    },
    "Extremely impressive": {
        "fn": "extremelyimpressive.rle",
        "w": 12,
        "h": 10
    },
    "Snorkel loop": {
        "fn": "snorkelloop.rle",
        "w": 6,
        "h": 5
    },
    "Blocked p4-1": {
        "fn": "blockedp41.rle",
        "w": 23,
        "h": 14
    },
    "buckinghaminjector": {
        "fn": "buckinghaminjector.rle",
        "w": 23,
        "h": 21
    },
    "amphisbaena synth": {
        "fn": "amphisbaena_synth.rle",
        "w": 64,
        "h": 17
    },
    "cottonmouthextended": {
        "fn": "cottonmouthextended.rle",
        "w": 30,
        "h": 85
    },
    "minstrels": {
        "fn": "minstrels.rle",
        "w": 146,
        "h": 104
    },
    "shuriken synth": {
        "fn": "shuriken_synth.rle",
        "w": 75,
        "h": 82
    },
    "houndstooth": {
        "fn": "houndstooth.rle",
        "w": 32,
        "h": 32
    },
    "5-engine Cordership synth": {
        "fn": "5enginecordership_synth.rle",
        "w": 162,
        "h": 158
    },
    "p15bumper": {
        "fn": "p15bumper.rle",
        "w": 72,
        "h": 64
    },
    "117P9H3V0": {
        "fn": "117p9h3v0.rle",
        "w": 17,
        "h": 29
    },
    "crystal": {
        "fn": "crystal.rle",
        "w": 664,
        "h": 659
    },
    "queenbeeshuttlepairgun": {
        "fn": "queenbeeshuttlepairgun.rle",
        "w": 150,
        "h": 131
    },
    "long7ship": {
        "fn": "long7ship.rle",
        "w": 10,
        "h": 10
    },
    "158P3 on Achim's p11": {
        "fn": "258p3onachimsp11.rle",
        "w": 49,
        "h": 31
    },
    "Block on table": {
        "fn": "blockontable.rle",
        "w": 4,
        "h": 5
    },
    "Harbor": {
        "fn": "harbor.rle",
        "w": 17,
        "h": 17
    },
    "Barge": {
        "fn": "barge.rle",
        "w": 4,
        "h": 4
    },
    "tubwithlongcistail": {
        "fn": "tubwithlongcistail.rle",
        "w": 6,
        "h": 8
    },
    "beacononlonghookisomers": {
        "fn": "beacononlonghookisomers.rle",
        "w": 16,
        "h": 19
    },
    "Fermat prime calculator": {
        "fn": "fermatprimecalculator.rle",
        "w": 838,
        "h": 736
    },
    "pentapole synth": {
        "fn": "pentapole_synth.rle",
        "w": 67,
        "h": 22
    },
    "6-in-a-row Cordership": {
        "fn": "6inarowcordership.rle",
        "w": 95,
        "h": 99
    },
    "Trans-long boat with tail": {
        "fn": "translongboatwithtail.rle",
        "w": 6,
        "h": 6
    },
    "7468M": {
        "fn": "7468m.rle",
        "w": 6,
        "h": 4
    },
    "unixon34p14shuttle": {
        "fn": "unixon34p14shuttle.rle",
        "w": 18,
        "h": 22
    },
    "metamorphosisii": {
        "fn": "metamorphosisii.rle",
        "w": 53,
        "h": 51
    },
    "Merzenich's P11": {
        "fn": "mmp11.rle",
        "w": 21,
        "h": 21
    },
    "Bent keys": {
        "fn": "bentkeys.rle",
        "w": 12,
        "h": 5
    },
    "Ellison p4 HW emulator hybrid": {
        "fn": "ellisonp4hwemulatorhybrid.rle",
        "w": 20,
        "h": 9
    },
    "vspark": {
        "fn": "vspark.rle",
        "w": 3,
        "h": 2
    },
    "25p3h1v0.1 synth": {
        "fn": "25p3h1v0.1_synth.rle",
        "w": 374,
        "h": 494
    },
    "barbershopreaction": {
        "fn": "barbershopreaction.rle",
        "w": 20,
        "h": 16
    },
    "transsnaketie": {
        "fn": "transsnaketie.rle",
        "w": 6,
        "h": 6
    },
    "p6bouncer": {
        "fn": "p6bouncer.rle",
        "w": 61,
        "h": 55
    },
    "Very long house": {
        "fn": "verylonghouse.rle",
        "w": 7,
        "h": 3
    },
    "Figure eight on rattlesnake": {
        "fn": "figureeightonrattlesnake.rle",
        "w": 20,
        "h": 15
    },
    "long3boat synth": {
        "fn": "long3boat_synth.rle",
        "w": 32,
        "h": 20
    },
    "F117": {
        "fn": "f117.rle",
        "w": 45,
        "h": 22
    },
    "Decapole": {
        "fn": "decapole.rle",
        "w": 13,
        "h": 13
    },
    "Pentoad synth": {
        "fn": "pentoad_synth.rle",
        "w": 123,
        "h": 25
    },
    "pennylane synth": {
        "fn": "pennylane_synth.rle",
        "w": 379,
        "h": 32
    },
    "Popover": {
        "fn": "popover.rle",
        "w": 32,
        "h": 32
    },
    "Laputa": {
        "fn": "laputa.rle",
        "w": 12,
        "h": 7
    },
    "ccsemicenark": {
        "fn": "ccsemicenark.rle",
        "w": 35,
        "h": 35
    },
    "Table on table": {
        "fn": "tableontable.rle",
        "w": 4,
        "h": 5
    },
    "fx77": {
        "fn": "fx77.rle",
        "w": 30,
        "h": 61
    },
    "B29 synth": {
        "fn": "b29_synth.rle",
        "w": 221,
        "h": 29
    },
    "Long hook with tail synth": {
        "fn": "longhookwithtail_synth.rle",
        "w": 73,
        "h": 51
    },
    "36P22 synth": {
        "fn": "36p22_synth.rle",
        "w": 68,
        "h": 32
    },
    "bistableswitchgun": {
        "fn": "bistableswitchgun.rle",
        "w": 123,
        "h": 93
    },
    "Cis-loaf with tail": {
        "fn": "cisloafwithtail.rle",
        "w": 6,
        "h": 5
    },
    "carriersiamesecanoe": {
        "fn": "carriersiamesecanoe.rle",
        "w": 6,
        "h": 8
    },
    "33p3.1reactions": {
        "fn": "33p3.1reactions.rle",
        "w": 19,
        "h": 16
    },
    "Trans-beacon and table synth": {
        "fn": "transbeaconandtable_synth.rle",
        "w": 109,
        "h": 49
    },
    "Worker bee synth": {
        "fn": "workerbee_synth.rle",
        "w": 156,
        "h": 111
    },
    "T-nosed p6": {
        "fn": "tnosedp6.rle",
        "w": 15,
        "h": 11
    },
    "brokensnake synth": {
        "fn": "brokensnake_synth.rle",
        "w": 38,
        "h": 18
    },
    "7-in-a-row Cordership synth": {
        "fn": "7inarowcordership_synth.rle",
        "w": 213,
        "h": 221
    },
    "aVerage": {
        "fn": "average.rle",
        "w": 13,
        "h": 11
    },
    "Eater 2": {
        "fn": "eater2.rle",
        "w": 7,
        "h": 7
    },
    "Beacon on table tie eater": {
        "fn": "beaconontabletieeater.rle",
        "w": 8,
        "h": 8
    },
    "47575m": {
        "fn": "47575m.rle",
        "w": 16,
        "h": 16
    },
    "Pre-pulsar": {
        "fn": "prepulsar.rle",
        "w": 9,
        "h": 3
    },
    "Kok's galaxy": {
        "fn": "koksgalaxy.rle",
        "w": 9,
        "h": 9
    },
    "6-in-a-row Cordership synth": {
        "fn": "6inarowcordership_synth.rle",
        "w": 171,
        "h": 175
    },
    "Slow puffer 1": {
        "fn": "slowpuffer1.rle",
        "w": 82,
        "h": 73
    },
    "P11 pinwheel": {
        "fn": "p11pinwheel.rle",
        "w": 23,
        "h": 23
    },
    "Garden of Eden 1": {
        "fn": "gardenofeden1.rle",
        "w": 33,
        "h": 9
    },
    "60P3H1V0.3 reactions": {
        "fn": "60p3h1v0.3reactions.rle",
        "w": 185,
        "h": 43
    },
    "Bunnies synth": {
        "fn": "bunnies_synth.rle",
        "w": 48,
        "h": 13
    },
    "b60": {
        "fn": "b60.rle",
        "w": 21,
        "h": 13
    },
    "sparkingeaters": {
        "fn": "sparkingeaters.rle",
        "w": 56,
        "h": 16
    },
    "p3bumperalt": {
        "fn": "p3bumperalt.rle",
        "w": 59,
        "h": 54
    },
    "Jam": {
        "fn": "jam.rle",
        "w": 6,
        "h": 7
    },
    "Eater head siamese carrier": {
        "fn": "eaterheadsiamesecarrier.rle",
        "w": 7,
        "h": 5
    },
    "P44 traffic light hassler": {
        "fn": "p44trafficlighthassler.rle",
        "w": 36,
        "h": 53
    },
    "oquadloaf": {
        "fn": "oquadloaf.rle",
        "w": 10,
        "h": 10
    },
    "P5 reflector": {
        "fn": "p5reflector.rle",
        "w": 29,
        "h": 24
    },
    "Great on off synth": {
        "fn": "greatonoff_synth.rle",
        "w": 80,
        "h": 98
    },
    "rake based on Slow Puffer 2": {
        "fn": "slowpuffer2rake.rle",
        "w": 183,
        "h": 79
    },
    "Zweiback": {
        "fn": "zweiback.rle",
        "w": 79,
        "h": 21
    },
    "tnosedp4 synth": {
        "fn": "tnosedp4_synth.rle",
        "w": 295,
        "h": 59
    },
    "clawsiamesetubwithcape": {
        "fn": "clawsiamesetubwithcape.rle",
        "w": 6,
        "h": 5
    },
    "Long long snake": {
        "fn": "longlongsnake.rle",
        "w": 6,
        "h": 4
    },
    "Queen bee loop": {
        "fn": "queenbeeloop.rle",
        "w": 24,
        "h": 24
    },
    "Roteightor": {
        "fn": "roteightor.rle",
        "w": 14,
        "h": 14
    },
    "Eater on boat": {
        "fn": "eateronboat.rle",
        "w": 5,
        "h": 7
    },
    "figureeightonbeluchenkosp13": {
        "fn": "figureeightonbeluchenkosp13.rle",
        "w": 23,
        "h": 19
    },
    "151P3H1V0": {
        "fn": "151p3h1v0.rle",
        "w": 33,
        "h": 21
    },
    "period690glidergun": {
        "fn": "period690glidergun.rle",
        "w": 52,
        "h": 25
    },
    "Fleet": {
        "fn": "fleet.rle",
        "w": 10,
        "h": 10
    },
    "186P24": {
        "fn": "186p24.rle",
        "w": 58,
        "h": 26
    },
    "Barge 2 (spaceship)": {
        "fn": "barge2spaceship.rle",
        "w": 35,
        "h": 15
    },
    "S-pentomino": {
        "fn": "spentomino.rle",
        "w": 4,
        "h": 2
    },
    "Hertz oscillator": {
        "fn": "hertzoscillator.rle",
        "w": 11,
        "h": 12
    },
    "carriersiameseverylongsnake": {
        "fn": "carriersiameseverylongsnake.rle",
        "w": 5,
        "h": 9
    },
    "loadingdock synth": {
        "fn": "loadingdock_synth.rle",
        "w": 83,
        "h": 30
    },
    "Merzenich's p64": {
        "fn": "merzenichsp64.rle",
        "w": 21,
        "h": 21
    },
    "verylongclawwithtail": {
        "fn": "verylongclawwithtail.rle",
        "w": 8,
        "h": 4
    },
    "Griddle and dock": {
        "fn": "griddleanddock.rle",
        "w": 7,
        "h": 8
    },
    "Boat with very long tail": {
        "fn": "boatwithverylongtail.rle",
        "w": 7,
        "h": 4
    },
    "Hacksaw": {
        "fn": "hacksaw.rle",
        "w": 199,
        "h": 102
    },
    "fheptomino": {
        "fn": "fheptomino.rle",
        "w": 4,
        "h": 4
    },
    "29-bit still life #1": {
        "fn": "29bitstilllifeno1.rle",
        "w": 8,
        "h": 10
    },
    "Rectifier": {
        "fn": "rectifier.rle",
        "w": 41,
        "h": 33
    },
    "l156reactions": {
        "fn": "l156reactions.rle",
        "w": 133,
        "h": 144
    },
    "cottonmouth": {
        "fn": "cottonmouth.rle",
        "w": 10,
        "h": 34
    },
    "Jolson": {
        "fn": "jolson.rle",
        "w": 13,
        "h": 19
    },
    "Total aperiodic": {
        "fn": "totalaperiodic.rle",
        "w": 59,
        "h": 57
    },
    "Boat on spark coil": {
        "fn": "boatonsparkcoil.rle",
        "w": 8,
        "h": 11
    },
    "stillater synth": {
        "fn": "stillater_synth.rle",
        "w": 104,
        "h": 34
    },
    "Heptapole": {
        "fn": "heptapole.rle",
        "w": 10,
        "h": 10
    },
    "Cthulhu": {
        "fn": "cthulhu.rle",
        "w": 11,
        "h": 13
    },
    "Jaydot": {
        "fn": "jaydot.rle",
        "w": 3,
        "h": 6
    },
    "Period-36 glider gun": {
        "fn": "period36glidergun.rle",
        "w": 50,
        "h": 49
    },
    "E-heptomino": {
        "fn": "eheptomino.rle",
        "w": 4,
        "h": 3
    },
    "Riley's breeder": {
        "fn": "rileysbreeder.rle",
        "w": 135,
        "h": 41
    },
    "Herschel": {
        "fn": "herschel.rle",
        "w": 3,
        "h": 4
    },
    "p7reflectoralt": {
        "fn": "p7reflectoralt.rle",
        "w": 76,
        "h": 56
    },
    "35P52 synth": {
        "fn": "35p52_synth.rle",
        "w": 94,
        "h": 79
    },
    "tableandtable synth": {
        "fn": "tableandtable_synth.rle",
        "w": 118,
        "h": 103
    },
    "p144gun": {
        "fn": "p144gun.rle",
        "w": 35,
        "h": 33
    },
    "10gm": {
        "fn": "twoglidermess.rle",
        "w": 87,
        "h": 8
    },
    "Block and cap": {
        "fn": "blockandcap.rle",
        "w": 4,
        "h": 6
    },
    "spartangtomwss": {
        "fn": "spartangtomwss.rle",
        "w": 131,
        "h": 137
    },
    "beehiveontable synth": {
        "fn": "beehiveontable_synth.rle",
        "w": 106,
        "h": 49
    },
    "T-nosed p4": {
        "fn": "tnosedp4.rle",
        "w": 11,
        "h": 10
    },
    "Period 11 - 14 oscillators": {
        "fn": "period1114oscillators.rle",
        "w": 544,
        "h": 263
    },
    "Tripole synth": {
        "fn": "tripole_synth.rle",
        "w": 22,
        "h": 17
    },
    "Turing machine": {
        "fn": "turingmachine.rle",
        "w": 1714,
        "h": 1647
    },
    "Blocked p4-2": {
        "fn": "blockedp42.rle",
        "w": 22,
        "h": 8
    },
    "Griddle and boat synth": {
        "fn": "griddleandboat_synth.rle",
        "w": 159,
        "h": 71
    },
    "5blink": {
        "fn": "5blink.rle",
        "w": 13,
        "h": 9
    },
    "beehiveondock": {
        "fn": "beehiveondock.rle",
        "w": 7,
        "h": 6
    },
    "Cis-hook with tail": {
        "fn": "cishookwithtail.rle",
        "w": 5,
        "h": 6
    },
    "canoesiamesesnake": {
        "fn": "canoesiamesesnake.rle",
        "w": 8,
        "h": 5
    },
    "Muttering moat 1": {
        "fn": "mutteringmoat1.rle",
        "w": 7,
        "h": 7
    },
    "newgun": {
        "fn": "newgun.rle",
        "w": 36,
        "h": 34
    },
    "Long canoe": {
        "fn": "longcanoe.rle",
        "w": 6,
        "h": 6
    },
    "Lake 2 synth": {
        "fn": "lake2_synth.rle",
        "w": 71,
        "h": 18
    },
    "Eureka v2": {
        "fn": "eurekav2.rle",
        "w": 20,
        "h": 11
    },
    "B-heptomino": {
        "fn": "bheptomino.rle",
        "w": 4,
        "h": 3
    },
    "Extremely impressive synth": {
        "fn": "extremelyimpressive_synth.rle",
        "w": 152,
        "h": 91
    },
    "Pentoad with two hexominoes": {
        "fn": "pentoadwithtwohexominoes.rle",
        "w": 17,
        "h": 16
    },
    "Fore and back": {
        "fn": "foreandback.rle",
        "w": 7,
        "h": 7
    },
    "blockondock": {
        "fn": "blockondock.rle",
        "w": 6,
        "h": 6
    },
    "Mold and long hook eating tub": {
        "fn": "moldandlonghookeatingtub.rle",
        "w": 6,
        "h": 14
    },
    "132P37": {
        "fn": "132p37.rle",
        "w": 47,
        "h": 47
    },
    "p6shuttle": {
        "fn": "p6shuttle.rle",
        "w": 14,
        "h": 14
    },
    "18P2.471": {
        "fn": "18p2.471.rle",
        "w": 7,
        "h": 7
    },
    "House synth": {
        "fn": "house_synth.rle",
        "w": 147,
        "h": 68
    },
    "Wing extended": {
        "fn": "wingextended.rle",
        "w": 47,
        "h": 47
    },
    "Newshuttle": {
        "fn": "newshuttle.rle",
        "w": 49,
        "h": 49
    },
    "wainwrightstagalong": {
        "fn": "wainwrightstagalong.rle",
        "w": 16,
        "h": 12
    },
    "spartangtoh": {
        "fn": "spartangtoh.rle",
        "w": 95,
        "h": 159
    },
    "dove synth": {
        "fn": "dove_synth.rle",
        "w": 8,
        "h": 11
    },
    "204p47 synth": {
        "fn": "204p47_synth.rle",
        "w": 1516,
        "h": 88
    },
    "70P2H1V0.1": {
        "fn": "70p2h1v01.rle",
        "w": 12,
        "h": 21
    },
    "quasarwick": {
        "fn": "quasarwick.rle",
        "w": 98,
        "h": 47
    },
    "56P18": {
        "fn": "56p18.rle",
        "w": 29,
        "h": 29
    },
    "Noah's ark": {
        "fn": "noahsark.rle",
        "w": 15,
        "h": 15
    },
    "period256glidergun": {
        "fn": "period256glidergun.rle",
        "w": 49,
        "h": 49
    },
    "gliderstopper": {
        "fn": "gliderstopper.rle",
        "w": 67,
        "h": 57
    },
    "c/3 puffer #1": {
        "fn": "c3puffer1.rle",
        "w": 87,
        "h": 38
    },
    "Eater stamp collection": {
        "fn": "eaterstampcollection.rle",
        "w": 241,
        "h": 350
    },
    "Tetrominoes": {
        "fn": "tetrominoes.rle",
        "w": 39,
        "h": 2
    },
    "Cis-beacon and dock": {
        "fn": "cisbeaconanddock.rle",
        "w": 6,
        "h": 8
    },
    "25P3H1V0.2": {
        "fn": "25p3h1v0.2.rle",
        "w": 16,
        "h": 8
    },
    "mwssgun1": {
        "fn": "mwssgun1.rle",
        "w": 40,
        "h": 29
    },
    "boatbitmwss": {
        "fn": "boatbitmwss.rle",
        "w": 21,
        "h": 23
    },
    "Beacon and two tails": {
        "fn": "beaconandtwotails.rle",
        "w": 7,
        "h": 7
    },
    "verylongboat": {
        "fn": "verylongboat.rle",
        "w": 5,
        "h": 5
    },
    "grinreagent": {
        "fn": "grinreagent.rle",
        "w": 13,
        "h": 11
    },
    "Cis-long boat with tail": {
        "fn": "cislongboatwithtail.rle",
        "w": 6,
        "h": 5
    },
    "Dove": {
        "fn": "dove.rle",
        "w": 5,
        "h": 4
    },
    "86P5H1V1": {
        "fn": "86p5h1v1.rle",
        "w": 23,
        "h": 23
    },
    "Pony express": {
        "fn": "ponyexpress.rle",
        "w": 74,
        "h": 124
    },
    "Titanic toroidal traveler": {
        "fn": "titanictoroidaltraveler.rle",
        "w": 78,
        "h": 2
    },
    "beehiveatclaw": {
        "fn": "beehiveatclaw.rle",
        "w": 6,
        "h": 6
    },
    "Cousins": {
        "fn": "cousins.rle",
        "w": 13,
        "h": 7
    },
    "64P2H1V0": {
        "fn": "64p2h1v0.rle",
        "w": 31,
        "h": 8
    },
    "cisshipontable": {
        "fn": "cisshipontable.rle",
        "w": 6,
        "h": 4
    },
    "Space rake": {
        "fn": "spacerake.rle",
        "w": 22,
        "h": 19
    },
    "Eater 1 reactions": {
        "fn": "eater1reactions.rle",
        "w": 101,
        "h": 15
    },
    "Bi-block": {
        "fn": "biblock.rle",
        "w": 5,
        "h": 2
    },
    "p60bheptominoshuttle": {
        "fn": "p60bheptominoshuttle.rle",
        "w": 27,
        "h": 47
    },
    "syringe": {
        "fn": "syringe.rle",
        "w": 41,
        "h": 36
    },
    "beehivestopper": {
        "fn": "beehivestopper.rle",
        "w": 48,
        "h": 48
    },
    "p36gun": {
        "fn": "p36gun.rle",
        "w": 52,
        "h": 49
    },
    "Trans-fuse with two tails": {
        "fn": "transfusewithtwotails.rle",
        "w": 6,
        "h": 6
    },
    "Integral sign synth": {
        "fn": "integralsign_synth.rle",
        "w": 141,
        "h": 101
    },
    "Aircraft carrier": {
        "fn": "aircraftcarrier.rle",
        "w": 4,
        "h": 3
    },
    "transcarrierupontable": {
        "fn": "transcarrierupontable.rle",
        "w": 7,
        "h": 4
    },
    "Cis-boat and long hook eating tub": {
        "fn": "cisboatandlonghookeatingtub.rle",
        "w": 11,
        "h": 5
    },
    "Caterer on figure eight": {
        "fn": "catereronfigureeight.rle",
        "w": 18,
        "h": 6
    },
    "Short keys": {
        "fn": "shortkeys.rle",
        "w": 12,
        "h": 4
    },
    "Bi-gun synth": {
        "fn": "bigun_synth.rle",
        "w": 125,
        "h": 23
    },
    "cisblockonlonghook": {
        "fn": "cisblockonlonghook.rle",
        "w": 5,
        "h": 6
    },
    "Pipsquirter 1": {
        "fn": "pipsquirter1.rle",
        "w": 15,
        "h": 11
    },
    "P44 traffic light hassler (original)": {
        "fn": "p44trafficlighthassleroriginal.rle",
        "w": 36,
        "h": 86
    },
    "p4colourchangingcenark": {
        "fn": "p4colourchangingcenark.rle",
        "w": 121,
        "h": 98
    },
    "131c31climber": {
        "fn": "131c31climber.rle",
        "w": 37,
        "h": 108
    },
    "Achim's p16": {
        "fn": "achimsp16.rle",
        "w": 13,
        "h": 13
    },
    "Unicycle": {
        "fn": "unicycle.rle",
        "w": 14,
        "h": 14
    },
    "18-7007": {
        "fn": "loaftieeaterwithtail_synth.rle",
        "w": 122,
        "h": 108
    },
    "22-13005": {
        "fn": "bookendssiamesetables_synth.rle",
        "w": 79,
        "h": 16
    },
    "46P4H1V0": {
        "fn": "46p4h1v0.rle",
        "w": 19,
        "h": 10
    },
    "HWSS blinker fuse": {
        "fn": "hwssblinkerfuse.rle",
        "w": 28,
        "h": 9
    },
    "16cell47487m": {
        "fn": "16cell47487m.rle",
        "w": 22,
        "h": 17
    },
    "14p2.4 synth": {
        "fn": "14p2.4_synth.rle",
        "w": 421,
        "h": 54
    },
    "112P15 synth": {
        "fn": "112p15_synth.rle",
        "w": 258,
        "h": 81
    },
    "Fred": {
        "fn": "fred.rle",
        "w": 20,
        "h": 20
    },
    "p28glidershuttle": {
        "fn": "p28glidershuttle.rle",
        "w": 42,
        "h": 42
    },
    "Long barge synth": {
        "fn": "longbarge_synth.rle",
        "w": 146,
        "h": 46
    },
    "inflectedlongsnake": {
        "fn": "inflectedlongsnake.rle",
        "w": 4,
        "h": 8
    },
    "cisboatontable synth": {
        "fn": "cisboatontable_synth.rle",
        "w": 104,
        "h": 123
    },
    "Dragon": {
        "fn": "dragon.rle",
        "w": 29,
        "h": 18
    },
    "Big S synth": {
        "fn": "bigs_synth.rle",
        "w": 152,
        "h": 77
    },
    "70P5H2V0": {
        "fn": "70p5h2v0.rle",
        "w": 18,
        "h": 16
    },
    "iheptomino": {
        "fn": "iheptomino.rle",
        "w": 4,
        "h": 4
    },
    "halfbakeryreaction": {
        "fn": "halfbakeryreaction.rle",
        "w": 68,
        "h": 71
    },
    "Pentadecathlon on snacker synth": {
        "fn": "pentadecathlononsnacker_synth.rle",
        "w": 150,
        "h": 127
    },
    "nonominoswitchenginepredecessor": {
        "fn": "nonominoswitchenginepredecessor.rle",
        "w": 6,
        "h": 3
    },
    "Small lake variants": {
        "fn": "smalllakevariants.rle",
        "w": 21,
        "h": 9
    },
    "Hooks": {
        "fn": "hooks.rle",
        "w": 11,
        "h": 10
    },
    "Loaf siamese barge synth": {
        "fn": "loafsiamesebarge_synth.rle",
        "w": 148,
        "h": 104
    },
    "Cauldron": {
        "fn": "cauldron.rle",
        "w": 11,
        "h": 13
    },
    "Heart": {
        "fn": "heart.rle",
        "w": 11,
        "h": 11
    },
    "31c/240 reaction": {
        "fn": "31c240reaction.rle",
        "w": 10,
        "h": 26
    },
    "Snake bridge snake synth": {
        "fn": "snakebridgesnake_synth.rle",
        "w": 109,
        "h": 45
    },
    "400P49": {
        "fn": "400p49.rle",
        "w": 65,
        "h": 65
    },
    "eatingc4ss": {
        "fn": "eatingc4ss.rle",
        "w": 98,
        "h": 60
    },
    "bobsled": {
        "fn": "bobsled.rle",
        "w": 224,
        "h": 239
    },
    "lwsslwssbounce": {
        "fn": "lwsslwssbounce.rle",
        "w": 55,
        "h": 4
    },
    "minimalsnarktromboneslide": {
        "fn": "minimalsnarktromboneslide.rle",
        "w": 49,
        "h": 64
    },
    "Snail": {
        "fn": "snail.rle",
        "w": 38,
        "h": 21
    },
    "Period-45 glider gun": {
        "fn": "period45glidergun.rle",
        "w": 48,
        "h": 40
    },
    "64P13.2": {
        "fn": "64p13.2.rle",
        "w": 19,
        "h": 31
    },
    "114P6H1V0": {
        "fn": "114p6h1v0.rle",
        "w": 16,
        "h": 32
    },
    "pole3rotor": {
        "fn": "pole3rotor.rle",
        "w": 3,
        "h": 1
    },
    "Bullet heptomino": {
        "fn": "bulletheptomino.rle",
        "w": 3,
        "h": 3
    },
    "3-engine Cordership synth": {
        "fn": "3enginecordership_synth.rle",
        "w": 94,
        "h": 102
    },
    "Sawtooth 362": {
        "fn": "sawtooth362.rle",
        "w": 168,
        "h": 306
    },
    "64P13.1": {
        "fn": "64p13.1.rle",
        "w": 17,
        "h": 31
    },
    "boattieeatertail synth": {
        "fn": "boattieeatertail_synth.rle",
        "w": 143,
        "h": 100
    },
    "tannersp46 gun": {
        "fn": "tannersp46_gun.rle",
        "w": 31,
        "h": 44
    },
    "Six Ls": {
        "fn": "sixls.rle",
        "w": 7,
        "h": 8
    },
    "duelingbanjos": {
        "fn": "duelingbanjos.rle",
        "w": 20,
        "h": 25
    },
    "twopulsarquadrants synth": {
        "fn": "twopulsarquadrants_synth.rle",
        "w": 28,
        "h": 38
    },
    "Silver's p5": {
        "fn": "silversp5.rle",
        "w": 11,
        "h": 7
    },
    "Cap and dock": {
        "fn": "capanddock.rle",
        "w": 7,
        "h": 6
    },
    "4gto5greaction": {
        "fn": "4gto5greaction.rle",
        "w": 65,
        "h": 49
    },
    "Queen bee shuttle": {
        "fn": "queenbeeshuttle.rle",
        "w": 22,
        "h": 7
    },
    "harbor synth": {
        "fn": "harbor_synth.rle",
        "w": 124,
        "h": 67
    },
    "22P36": {
        "fn": "22p36.rle",
        "w": 14,
        "h": 18
    },
    "Loaflipflop": {
        "fn": "loaflipflop.rle",
        "w": 34,
        "h": 34
    },
    "Karel's p15": {
        "fn": "karelsp15.rle",
        "w": 10,
        "h": 11
    },
    "bronco": {
        "fn": "bronco.rle",
        "w": 55,
        "h": 48
    },
    "P61 Herschel loop 1": {
        "fn": "p61herschelloop1.rle",
        "w": 404,
        "h": 256
    },
    "Radial pseudo-barberpole": {
        "fn": "radialpseudobarberpole.rle",
        "w": 13,
        "h": 13
    },
    "Eater 1 synth": {
        "fn": "eater1_synth.rle",
        "w": 150,
        "h": 105
    },
    "Bunnies 9": {
        "fn": "bunnies9.rle",
        "w": 8,
        "h": 7
    },
    "Catacryst": {
        "fn": "catacryst.rle",
        "w": 2555,
        "h": 1772
    },
    "47P72 synth": {
        "fn": "47p72_synth.rle",
        "w": 150,
        "h": 90
    },
    "p18biblockhassler": {
        "fn": "p18biblockhassler.rle",
        "w": 15,
        "h": 20
    },
    "Blinker fuse (6c/13)": {
        "fn": "blinkerfuse6c13.rle",
        "w": 62,
        "h": 3
    },
    "rephaser": {
        "fn": "rephaser.rle",
        "w": 20,
        "h": 13
    },
    "Snake pit": {
        "fn": "snakepit.rle",
        "w": 7,
        "h": 7
    },
    "78P70": {
        "fn": "78p70.rle",
        "w": 31,
        "h": 25
    },
    "20P2": {
        "fn": "20p2.rle",
        "w": 8,
        "h": 11
    },
    "Mirage": {
        "fn": "mirage.rle",
        "w": 68,
        "h": 46
    },
    "c5diagonalrake": {
        "fn": "c5diagonalrake.rle",
        "w": 570,
        "h": 884
    },
    "(23,5)c/79 Herschel climber": {
        "fn": "235c79climber.rle",
        "w": 59,
        "h": 98
    },
    "Sawtooth 269": {
        "fn": "sawtooth269.rle",
        "w": 151,
        "h": 107
    },
    "Boat-ship-tie synth": {
        "fn": "boatshiptie_synth.rle",
        "w": 143,
        "h": 112
    },
    "p36shuttle": {
        "fn": "p36shuttle.rle",
        "w": 30,
        "h": 30
    },
    "Infinite glider hotel": {
        "fn": "infinitegliderhotel.rle",
        "w": 566,
        "h": 572
    },
    "Snacker": {
        "fn": "snacker.rle",
        "w": 20,
        "h": 11
    },
    "P156 Hans Leo hassler": {
        "fn": "p156hansleohassler.rle",
        "w": 33,
        "h": 40
    },
    "Trans-beacon and table": {
        "fn": "transbeaconandtable.rle",
        "w": 6,
        "h": 7
    },
    "tee": {
        "fn": "tee.rle",
        "w": 37,
        "h": 29
    },
    "Undecapole": {
        "fn": "undecapole.rle",
        "w": 14,
        "h": 14
    },
    "negativespaceship": {
        "fn": "negativespaceship.rle",
        "w": 55,
        "h": 31
    },
    "stairstephexomino synth": {
        "fn": "stairstephexomino_synth.rle",
        "w": 7,
        "h": 11
    },
    "twelveloop": {
        "fn": "twelveloop.rle",
        "w": 6,
        "h": 5
    },
    "Schick engine synth": {
        "fn": "schickengine_synth.rle",
        "w": 126,
        "h": 76
    },
    "104P177 synth": {
        "fn": "104p177_synth.rle",
        "w": 167,
        "h": 88
    },
    "brokenelevener": {
        "fn": "brokenelevener.rle",
        "w": 6,
        "h": 7
    },
    "Halfmax v2": {
        "fn": "halfmaxv2.rle",
        "w": 117,
        "h": 112
    },
    "Sparky": {
        "fn": "sparky.rle",
        "w": 30,
        "h": 11
    },
    "Sawtooth 633": {
        "fn": "sawtooth633.rle",
        "w": 272,
        "h": 69
    },
    "p24 shuttle synthesis": {
        "fn": "p24shuttle_synth.rle",
        "w": 184,
        "h": 119
    },
    "7-engine Cordership reflections": {
        "fn": "7enginecordershipreflections.rle",
        "w": 78,
        "h": 86
    },
    "1-2-3-4 synth": {
        "fn": "1234_synth.rle",
        "w": 158,
        "h": 120
    },
    "Garden of Eden 2": {
        "fn": "gardenofeden2.rle",
        "w": 14,
        "h": 14
    },
    "35p12": {
        "fn": "35p12.rle",
        "w": 14,
        "h": 14
    },
    "hfx58b": {
        "fn": "hfx58b.rle",
        "w": 32,
        "h": 92
    },
    "Toad flipper": {
        "fn": "toadflipper.rle",
        "w": 18,
        "h": 10
    },
    "Tetraplets": {
        "fn": "tetraplets.rle",
        "w": 54,
        "h": 31
    },
    "eaterheadsiameseeatertail synth": {
        "fn": "eaterheadsiameseeatertail_synth.rle",
        "w": 147,
        "h": 138
    },
    "Gourmet": {
        "fn": "gourmet.rle",
        "w": 20,
        "h": 20
    },
    "honeybarge": {
        "fn": "honeybarge.rle",
        "w": 6,
        "h": 5
    },
    "Big S": {
        "fn": "bigs.rle",
        "w": 7,
        "h": 6
    },
    "pyrotechnecium": {
        "fn": "pyrotechnecium.rle",
        "w": 16,
        "h": 10
    },
    "cisrotatedhook synth": {
        "fn": "cisrotatedhook_synth.rle",
        "w": 33,
        "h": 15
    },
    "LWSS on MWSS": {
        "fn": "lwssonmwss.rle",
        "w": 6,
        "h": 12
    },
    "144P24": {
        "fn": "144p24.rle",
        "w": 25,
        "h": 35
    },
    "P30 beehive hassler": {
        "fn": "p30beehivehassler.rle",
        "w": 79,
        "h": 24
    },
    "p20 glider gun": {
        "fn": "period20glidergun.rle",
        "w": 78,
        "h": 42
    },
    "roteightor synth": {
        "fn": "roteightor_synth.rle",
        "w": 60,
        "h": 62
    },
    "tubwithtailsiamesesnake": {
        "fn": "tubwithtailsiamesesnake.rle",
        "w": 8,
        "h": 5
    },
    "38p72 synth": {
        "fn": "38p72_synth.rle",
        "w": 113,
        "h": 39
    },
    "Boat test tube baby": {
        "fn": "boattesttubebaby.rle",
        "w": 10,
        "h": 6
    },
    "eater bridge eater": {
        "fn": "eaterbridgeeater.rle",
        "w": 8,
        "h": 5
    },
    "Great on-off": {
        "fn": "greatonoff.rle",
        "w": 8,
        "h": 8
    },
    "Wavefront": {
        "fn": "wavefront.rle",
        "w": 13,
        "h": 13
    },
    "14-172": {
        "fn": "eaterbridgeeater_synth.rle",
        "w": 107,
        "h": 98
    },
    "Bookend": {
        "fn": "bookend.rle",
        "w": 4,
        "h": 3
    },
    "Clock 2": {
        "fn": "clock2.rle",
        "w": 12,
        "h": 12
    },
    "Traffic jam": {
        "fn": "trafficjam.rle",
        "w": 16,
        "h": 8
    },
    "shipontripole": {
        "fn": "shipontripole.rle",
        "w": 9,
        "h": 9
    },
    "Sidecar eater": {
        "fn": "sidecareater.rle",
        "w": 18,
        "h": 14
    },
    "Long long ship synth": {
        "fn": "longlongship_synth.rle",
        "w": 154,
        "h": 120
    },
    "Triple caterer": {
        "fn": "triplecaterer.rle",
        "w": 16,
        "h": 11
    },
    "Breeder 1": {
        "fn": "breeder1.rle",
        "w": 749,
        "h": 338
    },
    "76p8": {
        "fn": "76p8.rle",
        "w": 14,
        "h": 15
    },
    "hexominoes": {
        "fn": "hexominoes.rle",
        "w": 82,
        "h": 43
    },
    "p60trafficlighthasslerpd synth": {
        "fn": "p60trafficlighthasslerpd_synth.rle",
        "w": 184,
        "h": 64
    },
    "Table on table synth": {
        "fn": "tableontable_synth.rle",
        "w": 118,
        "h": 103
    },
    "Big glider": {
        "fn": "bigglider.rle",
        "w": 18,
        "h": 18
    },
    "42p10.3": {
        "fn": "42p10.3.rle",
        "w": 13,
        "h": 13
    },
    "Scorpion synth": {
        "fn": "scorpion_synth.rle",
        "w": 35,
        "h": 47
    },
    "Infinite glider hotel 4": {
        "fn": "infinitegliderhotel4.rle",
        "w": 198,
        "h": 165
    },
    "tubwithlong4tail": {
        "fn": "tubwithlong4tail.rle",
        "w": 5,
        "h": 9
    },
    "Eater 5": {
        "fn": "eater5.rle",
        "w": 9,
        "h": 6
    },
    "Long long ship": {
        "fn": "longlongship.rle",
        "w": 5,
        "h": 5
    },
    "spiralgrowth": {
        "fn": "spiralgrowth.rle",
        "w": 1966,
        "h": 2007
    },
    "Pond synth": {
        "fn": "pond_synth.rle",
        "w": 105,
        "h": 36
    },
    "Maze wickstretcher": {
        "fn": "mazewickstretcher.rle",
        "w": 6,
        "h": 8
    },
    "28p7.3eatingss": {
        "fn": "28p7.3eatingss.rle",
        "w": 41,
        "h": 25
    },
    "Carrier siamese snake synth": {
        "fn": "carriersiamesesnake_synth.rle",
        "w": 151,
        "h": 92
    },
    "P15 pre-pulsar spaceship": {
        "fn": "p15prepulsarspaceship.rle",
        "w": 61,
        "h": 43
    },
    "c12rpentominocrawler": {
        "fn": "c12rpentominocrawler.rle",
        "w": 23,
        "h": 27
    },
    "1-2-3": {
        "fn": "123.rle",
        "w": 10,
        "h": 9
    },
    "Figure eight synth": {
        "fn": "figureeight_synth.rle",
        "w": 82,
        "h": 98
    },
    "124P21": {
        "fn": "124p21.rle",
        "w": 36,
        "h": 25
    },
    "l156variants": {
        "fn": "l156variants.rle",
        "w": 59,
        "h": 171
    },
    "tetradecathlon": {
        "fn": "tetradecathlon.rle",
        "w": 40,
        "h": 15
    },
    "Heavyweight spaceship synth": {
        "fn": "hwss_synth.rle",
        "w": 151,
        "h": 105
    },
    "gliderturner": {
        "fn": "gliderturner.rle",
        "w": 20,
        "h": 21
    },
    "p58toadflipper": {
        "fn": "p58toadflipper.rle",
        "w": 64,
        "h": 60
    },
    "180degreekickback": {
        "fn": "180degreekickback.rle",
        "w": 13,
        "h": 18
    },
    "sphere": {
        "fn": "sphere.rle",
        "w": 100,
        "h": 100
    },
    "prepulsarshuttle47 synth": {
        "fn": "prepulsarshuttle47_synth.rle",
        "w": 278,
        "h": 87
    },
    "Tub with tail synth": {
        "fn": "tubwithtail_synth.rle",
        "w": 107,
        "h": 104
    },
    "Tub": {
        "fn": "tub.rle",
        "w": 3,
        "h": 3
    },
    "Barge with long tail": {
        "fn": "bargewithlongtail.rle",
        "w": 7,
        "h": 5
    },
    "Honeycomb": {
        "fn": "honeycomb.rle",
        "w": 6,
        "h": 5
    },
    "prepulsarshuttle42": {
        "fn": "prepulsarshuttle42.rle",
        "w": 36,
        "h": 37
    },
    "P690 60P5H2V0 gun": {
        "fn": "p69060p5h2v0gun.rle",
        "w": 871,
        "h": 854
    },
    "p18biblockhassler synth": {
        "fn": "p18biblockhassler_synth.rle",
        "w": 120,
        "h": 81
    },
    "Rx202": {
        "fn": "rx202.rle",
        "w": 31,
        "h": 55
    },
    "Blockade": {
        "fn": "blockade.rle",
        "w": 23,
        "h": 10
    },
    "snark synth": {
        "fn": "snark_synth.rle",
        "w": 133,
        "h": 130
    },
    "stopandgo": {
        "fn": "stopandgo.rle",
        "w": 42,
        "h": 11
    },
    "Star synth": {
        "fn": "star_synth.rle",
        "w": 85,
        "h": 67
    },
    "tubbendlinetub": {
        "fn": "tubbendlinetub.rle",
        "w": 6,
        "h": 8
    },
    "114P6H1V0 pushalong": {
        "fn": "114p6h1v0pushalong.rle",
        "w": 30,
        "h": 43
    },
    "period52gun": {
        "fn": "period52gun.rle",
        "w": 309,
        "h": 298
    },
    "Trans-boat with tail synth": {
        "fn": "transboatwithtail_synth.rle",
        "w": 148,
        "h": 73
    },
    "Chris Cain": {
        "fn": "pseudoperiod14lglidergun4.rle",
        "w": 335,
        "h": 230
    },
    "Overweight spaceship": {
        "fn": "owss.rle",
        "w": 8,
        "h": 5
    },
    "Carrier with feather": {
        "fn": "carrierwithfeather.rle",
        "w": 7,
        "h": 4
    },
    "p48 pi hassler": {
        "fn": "period48pihassler.rle",
        "w": 33,
        "h": 26
    },
    "Up dove on dove synthesis": {
        "fn": "updoveondove_synth.rle",
        "w": 89,
        "h": 90
    },
    "49P88 synth": {
        "fn": "49p88_synth.rle",
        "w": 117,
        "h": 81
    },
    "quadraticsawtooth": {
        "fn": "quadraticsawtooth.rle",
        "w": 785,
        "h": 785
    },
    "Triplets": {
        "fn": "triplets.rle",
        "w": 38,
        "h": 3
    },
    "boatontripole": {
        "fn": "boatontripole.rle",
        "w": 9,
        "h": 9
    },
    "Canoe": {
        "fn": "canoe.rle",
        "w": 5,
        "h": 5
    },
    "runnynose": {
        "fn": "runnynose.rle",
        "w": 8,
        "h": 10
    },
    "Garden of Eden 5": {
        "fn": "gardenofeden5.rle",
        "w": 11,
        "h": 11
    },
    "loops43to54": {
        "fn": "loops43to54.rle",
        "w": 295,
        "h": 222
    },
    "catereronbeluchenkosp13": {
        "fn": "catereronbeluchenkosp13.rle",
        "w": 21,
        "h": 16
    },
    "centuryeaterreaction": {
        "fn": "centuryeaterreaction.rle",
        "w": 30,
        "h": 43
    },
    "Pre-block synth": {
        "fn": "preblock_synth.rle",
        "w": 67,
        "h": 34
    },
    "Blinker synth": {
        "fn": "blinker_synth.rle",
        "w": 150,
        "h": 75
    },
    "Integral with tub and hook": {
        "fn": "integralwithtubandhook.rle",
        "w": 6,
        "h": 7
    },
    "Siesta": {
        "fn": "siesta.rle",
        "w": 16,
        "h": 12
    },
    "Integral with hook": {
        "fn": "integralwithhook.rle",
        "w": 6,
        "h": 5
    },
    "Eater head siamese snake": {
        "fn": "eaterheadsiamesesnake.rle",
        "w": 7,
        "h": 4
    },
    "Tub test tube baby": {
        "fn": "tubtesttubebaby.rle",
        "w": 10,
        "h": 6
    },
    "period44mwssgun": {
        "fn": "period44mwssgun.rle",
        "w": 50,
        "h": 40
    },
    "Pre-pulsar predecessor": {
        "fn": "prepulsarpredecessor.rle",
        "w": 9,
        "h": 2
    },
    "Champagne glass": {
        "fn": "champagneglass.rle",
        "w": 15,
        "h": 12
    },
    "Teardrop": {
        "fn": "teardrop.rle",
        "w": 4,
        "h": 4
    },
    "Hectic": {
        "fn": "hectic.rle",
        "w": 39,
        "h": 39
    },
    "Caterer on rattlesnake": {
        "fn": "catereronrattlesnake.rle",
        "w": 13,
        "h": 20
    },
    "Pond on pond": {
        "fn": "pondonpond.rle",
        "w": 9,
        "h": 4
    },
    "Fx176": {
        "fn": "fx176.rle",
        "w": 50,
        "h": 38
    },
    "fourmoldshasslingeightblocks": {
        "fn": "fourmoldshasslingeightblocks.rle",
        "w": 26,
        "h": 26
    },
    "Boatstretcher 1": {
        "fn": "boatstretcher1.rle",
        "w": 21,
        "h": 21
    },
    "Piston": {
        "fn": "piston.rle",
        "w": 11,
        "h": 5
    },
    "Germ": {
        "fn": "germ.rle",
        "w": 10,
        "h": 10
    },
    "Caterer on 34P13": {
        "fn": "catereron34p13.rle",
        "w": 23,
        "h": 16
    },
    "Paperclip": {
        "fn": "paperclip.rle",
        "w": 5,
        "h": 6
    },
    "Vacuum (gun) pulling": {
        "fn": "vacuumgunpulling.rle",
        "w": 90,
        "h": 97
    },
    "86P9H3V0": {
        "fn": "86p9h3v0.rle",
        "w": 37,
        "h": 13
    },
    "Double X": {
        "fn": "doublex.rle",
        "w": 131,
        "h": 63
    },
    "p104bshuttle": {
        "fn": "p104bshuttle.rle",
        "w": 39,
        "h": 21
    },
    "c/4 diagonal puffer 1": {
        "fn": "c4diagonalpuffer1.rle",
        "w": 84,
        "h": 60
    },
    "snakewithfeather synth": {
        "fn": "snakewithfeather_synth.rle",
        "w": 30,
        "h": 30
    },
    "p4hwvolcano": {
        "fn": "p4hwvolcano.rle",
        "w": 20,
        "h": 18
    },
    "Mickey Mouse": {
        "fn": "mickeymouse.rle",
        "w": 10,
        "h": 7
    },
    "Fumarole synth": {
        "fn": "fumarole_synth.rle",
        "w": 191,
        "h": 52
    },
    "Technician": {
        "fn": "technician.rle",
        "w": 11,
        "h": 12
    },
    "41P7.2": {
        "fn": "41p7.2.rle",
        "w": 13,
        "h": 14
    },
    "Pre-pulsar shuttle 29 v2": {
        "fn": "prepulsarshuttle29v2.rle",
        "w": 27,
        "h": 18
    },
    "Cis-boat with tail": {
        "fn": "cisboatwithtail.rle",
        "w": 5,
        "h": 5
    },
    "Centinal": {
        "fn": "centinal.rle",
        "w": 52,
        "h": 17
    },
    "Tub with long long tail synth": {
        "fn": "tubwithlonglongtail_synth.rle",
        "w": 64,
        "h": 53
    },
    "Tub with long long tail": {
        "fn": "tubwithlonglongtail.rle",
        "w": 4,
        "h": 7
    },
    "Sawtooth 262": {
        "fn": "sawtooth262.rle",
        "w": 223,
        "h": 161
    },
    "Long boat synth": {
        "fn": "longboat_synth.rle",
        "w": 157,
        "h": 112
    },
    "Seal tagalong": {
        "fn": "sealtagalong.rle",
        "w": 55,
        "h": 55
    },
    "Glider symmetric PPS": {
        "fn": "glidersymmetricpps.rle",
        "w": 69,
        "h": 13
    },
    "Two eaters synth": {
        "fn": "twoeaters_synth.rle",
        "w": 155,
        "h": 28
    }
}

export default rle_index;